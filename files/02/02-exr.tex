\subsection{Exercises}

\begin{exr}[]
Let $f,g: \R \to \R$ be continuous functions.
Show that their \emph{equalizer} $A = \{x \in \R \big\vert f(x) = g(x)\} \subseteq \R$ is closed.

\textsf{Hint (backwards): {}.\hyperlink{prop:t2-criterion}{noiretirc $T_2$} eht esU.}
\end{exr}
\begin{sol}
  Since $\R$ is Hausdorff, the diagonal $\Delta = \{(x,x) \big\vert x \in \R\} \subseteq \R \times \R$ is closed.
  Then, consider the map
  \begin{align*}
    d: \R \to \R \times \R, \quad x \mapsto  (f(x),g(x))
  \end{align*}
  since $A = d^{-1}(\Delta)$, it follows from continuity of $d$ that $A$ is also closed.

  Note that the proof extends to any pair of parallel continuous functions $f,g: X \to Y$, where $Y$ is Hausdorff.
\end{sol}

\begin{exr}[]
  \begin{enumerate}
    \item Show that the quotient topology does indeed define a topology on $\faktor{X}{\sim}$.
    \item Show that the quotient topology is the \hyperlink{dfn:finer-topology}{finest} topology on the set $\faktor{X}{\sim}$ such that the quotient map $\pi$ is continuous.
  \end{enumerate}
\end{exr}



\begin{exr}[The torus]
  The $2$-torus is defined as $\IT^{2} := \IS^{1} \times \IS^{1}$ with the product topology.
  Show that it is homeomorphic to the quotient space $\faktor{X}{\sim}$, where
  \begin{align*}
    X = [0,1] \times [0,1], \quad (s,0) \sim (s,1) \text{\ and } (0,t) \sim (1,t), \quad \text{for all $s,t \in [0,1]$}
  \end{align*}

\end{exr}

\begin{exr}[Quotient of the product]
  Let $X,Y$ be topological spaces and consider the following equivalence relation on $X \times Y$ 
  \begin{align*}
    (x,y) \sim_Y (x',y') \iff x = x'
  \end{align*}
  Is it true that $\faktor{X \times Y}{\sim_Y} \iso X$?
\end{exr}
\begin{sol}
  If $Y$ is empty, this is false because $X \times \emptyset \iso \emptyset$.
  However for $Y$ non-empty this is true. 

  Let $y_0 \in Y$. We define the bijective map
  \begin{align*}
    \Phi: X \to \faktor{X \times Y}{\sim} =: Z
    ,\quad
    x \mapsto [(x,y_0)]
  \end{align*}
 % Note that $\Phi = \pi \circ \iota_{y_0}$ is the composition of continuous\footnote{Continuity of $\iota_{y_0}$ was proven in the previous exercise.} functions
 % \begin{align*}
 %   \xymatrix{
 %     & X \times Y
 %     \ar[d]^{\pi}
 %     \\
 %     X \ar[ur]^{\iota_{y_0}}
 %     \ar[r]_(0.4){\Phi}
 %     &
 %     \faktor{X \times Y}{\sim}
 %   }
 % \end{align*}
 % so it is continuous. 
  Its inverse function
  \begin{align*}
    \Psi: \faktor{X \times Y}{\sim} \to X
    ,\quad
    [(x,y)] \mapsto x
  \end{align*}
  is also well-defined and they do indeed satisfy $\Phi \circ \Psi = \id_{Z}$ and $\Psi \circ \Phi = \id_X$ (check it yourself).
  To show that $\Phi$ and $\Psi$ are continuous, we show that for all $A \subseteq X$, $A$ is open if and only if $B := \Phi(A) \subseteq Z$ is open.

  \textbf{$A$ open $\bm{\implies}$ $B$ open:} By definition of the quotient topology, $B$ is open if 
  \begin{align*}
    \pi^{-1}(B)
    = \{(x,y) \in X \times Y \big\vert [(x,y)] \in B\}
  \end{align*}
  is open.
\end{sol}

\begin{exr}[]
  Show that the converse statements of the previous remark are false. Find a topological space $X$ with equivalence relation $\sim$ such that:
  $\faktor{X}{\sim}$ is compact/connected/path-connected, but $X$ is not.
\end{exr}
\begin{sol}
Let $X$ be any non-empty space that is not compact/connected/path-connected space and take the equivalence relation $x \sim y$ for all $x,y \in X$.
Then trivially $\faktor{X}{\sim}$ has exactly one point and is compact, connected and path-connected.

If you think these counter examples are cheating, here are some ``real-word examples'':
\begin{itemize}
  \item $X = \R$ is not compact, but for $x \sim x + 1 \forall x \in \R$, the quotient space $\faktor{X}{\sim} \iso \IS^{1}$, is compact.
\end{itemize}
\end{sol}


\begin{exr}[Path connectedness of Lie Groups]
  For the following topological groups show the following.
  \begin{enumerate}
    \item $\text{O}(n) = \{A \in \GL(n,\R) \big\vert A^{T}A = 1\}$ is not path-connected.
    \item $\text{U}(n) = \{A \in \GL(n,\C) \big\vert A^{\dagger} A = 1\}$ is path-connected. \quad ($A^{\dagger}$ is the conjugate transpose of $A$)
    \item $\text{SU}(n) = \{U \in \text{U}(n) \big\vert \det U = 1\}$ is path-connected.
    \item $\GL(n,\C)$ is path-connected.
    %\item $\text{O}(n) = \{A \in \GL(n,\R) \big\vert A^{T}A = 1\}$
    %\item $\GL(n,\R)$
  \end{enumerate}
  You may use the following results from Linear Algebra/Numerical Analysis.
  \begin{itemize}
    \item For all $A \in \text{U}(n)$, there exists $U \in \text{U}(n)$ and $D = \text{diag}(e^{i \phi_1}, \ldots, e^{i \phi_n})$ with $\phi_i \in \R$ such that $A = UDU^{-1}$.
    \item For $A \in \GL(n,\C)$, there exist $Q$ unitary and $R$ an upper triangle matrix with non-negative diagonal entries.
  \end{itemize}
\end{exr}
\begin{sol}
\begin{enumerate}
  \item First note that $A \in \text{O}(n)$ implies that $1 = \det 1 = \det (A^{T}A) = {(\det A)}^{2}$. 
    Since the determinant is polynomial in the coefficients of the matrix, it is continuous, so $\text{O}(n)$ can't be path-connected because the image of
    \begin{align*}
      \det: \text{O}(n) \to \{\pm 1\}
    \end{align*}
    is not path-connected.
  \item Let $A = UDU^{-1} \in \text{U}(n)$ as in the hint. Define the path
    \begin{align*}
      \gamma: [0,1] \to \text{U}(n), \quad
      t \mapsto U D_t U^{-1}
      \quad \text{where} \quad 
      D_t = \text{diag}(e^{i t \phi_1},\ldots,e^{it \phi_n})
    \end{align*}
    Multiplication with $U$ and $U^{-1}$ are continuous and the map $t \mapsto  e^{i t \phi}$ is also continuous, so $\gamma$ is continuous. Moreover
    \begin{align*}
      \gamma(0) = UD_0U^{-1} = UU^{-1} = 1
      \quad \text{and} \quad 
      \gamma(1) = UD_1U^{-1} = A
    \end{align*}
    so every $A \in \text{U}(n)$ is path-connected to the identity $1 \in \text{U}(n)$. It follows that $\text{U}(n)$ is path-conncted.
  \item Let $A = UDU^{-1} \in \text{SU}(n)$. We want to show that the path $\gamma(t)$ always stays inside $\text{SU}(n)$, i.e.\ that $\det(\gamma(t)) = 1$ for all $t$.
    Indeed, we have
    \begin{align*}
      1 = \det A = \det D = \prod_{k=1}^{n} e^{i \phi_k} = e^{i\sum_{k=1}^{n} \phi_k}
    \end{align*}
    which implies that $\sum_{k=1}^{n} \phi_k$ is an integer-multiple of $2 \pi$. 
    We can always substitute the numbers $\phi_i$ with $\phi_i \pm 2 \pi$, so we can assume WLOG $\sum_{k=1}^{n} \phi_k = 0$.
    So
    \begin{align*}
      \det \gamma(t) = \det D_t = e^{it \sum_{k=1}^{n} \phi_k} = e^{it \cdot 0} = 1
    \end{align*}
  \item Since we know that $\text{U}(n)$ is path-connected, it suffices to find a path from $A \in \GL(n,\C)$ to some $U \in \text{U}(n)$ to show that $\GL(n,\C)$ is path-connected.
    Let $A = QR$ be the decomposition with $Q \in \text{U}(n)$ and $R$ an upper-triangle matrix with entries $r_{ij}$.
    Since $R$ is invertible, $\det R = \prod_{k=1}^{n}r_{ii} \neq 0$.
    By adjusting $Q$, we can also assume that $r_{ii} > 0$.
    Then define the path
    \begin{align*}
      \gamma: [0,1] \to \GL(n,\C), \quad
      t \mapsto QC(t)
      \quad \text{where} \quad 
      c_{ij}(t) =
      \left\{\begin{array}{ll}
        t \cdot r_{ij} & i \neq j\\
      \underbrace{1 + t \cdot (r_{ii} - 1)}_{> 0} & i = j
      \end{array} \right.
    \end{align*}
    Which is continuous since $t \mapsto c_{ij}(t)$ is continuous. Then we have
    \begin{align*}
      \gamma(0) = Q \in \text{U}(n),
      \quad
      \gamma(1) = QR = A
      ,
      \quad
      \det C_t = \prod_{i=1}^{n}c_{ii}(t) \neq 0 \forall t \in [0,1]
    \end{align*}
\end{enumerate}
\end{sol}

%\begin{exr}[]
%  Let $X$ be a topological space, $A \subseteq X$. Compare the three spaces
%  \begin{align*}
%    A, \text{Cone}(\del A), \del \text{Cone}(A)
%  \end{align*}
%  for
%\end{exr}

%\begin{exr}[Rotations]
%  Consider the group of rotations in $3$-dimensional space
%  \begin{align*}
%    \SO(3) := \left\{
%      R \in \Mat(3 \times 3,\R)
%      \big\vert
%      R^{T}R = I_3, \det R = 1
%    \right\}
%  \end{align*}
%  One can show that any rotation can be described by some unit vector $\vec{n} \in S^{2}$ and rotation angle $\phi \in [0,2\pi)$ and thus write $R = R_{\vec{n},\phi}$.
%\end{exr}
