\section*{About}
This is a script for the Topology PVK (Prüfungsvorbereitungskurs) in Spring Semester 2023.
It is based on Prof.\ Fellers Topology lectures from Spring Semesters 2021 to 2023 and assumes no prior knowledge of topology, so it should be readable even if you didn't attend/watch any of his lectures (though I highly recommend watching them because they are great!).

Since the lecture and the exam are in German, the German names for definitions and results are written in parenthesis after the English names, unless they are just a direct translation (they are 95\% of the time). 
If uncertain, please ask in class or refer to the official PVK script from 2021.

Because this script is licensed under the GNU Free Documentation License, you have the freedom to copy the source code, modify or distribute this document provided you grant those rights unto others you share it with.
Updated compiled versions can be downloaded from my personal website \url{https://n.ethz.ch/student/kimha}. The source code is available at \url{https://gitlab.ethz.ch/kimha/topology-script}.
If you have any corrections or want to make additions to this script, feel free to create an issue on gitlab.

When compiling, you may adjust the settings in \texttt{main.tex} or simply run \texttt{make all} to generate different versions.
\begin{itemize}
  \item The ``fancy'' version puts Theorems, Lemmas, Remarks etc.\ in color-coded boxes, which I personally find useful because it seperates statements from proofs. Others may find it too distracting or a waste of space/printing ink.
  \item The ``classic'' version is black-and-white without the boxes, making it a couple pages shorter and thus easier on your \url{webprint.ethz.ch} balance.
  \item ``no-solutions'' has no solutions for the exercises.
\end{itemize}


%The exercises have a difficulty rating from easy (1 star) to hard (5 stars), aswell as a marker for when they are important/very useful (TODO), or when they are just for people that want a deeper dive into topology.


\subsection*{Notation}
The goal is to use symbols as consistently as possible.
Although it makes the text a bit more verbose, I will refrain from omitting the type of a symbol, so no definition or theorem shall make use of a symbol before declaring what it is.
Nevertheless, there will be exceptions.

The following symbols/modifiers will be used without comment.
\begin{itemize}
  \item $\emptyset = \{\}$ is the empty set.
  \item $\N = \{\bm{0},1,2,\ldots\}$. (Yes, zero is a natural number.) We use $\N^{+} = \{1,2,\ldots\}$.
  \item For $X,Y$ sets, let $Y^{X} = \Hom_{\Set}(X,Y) = \{f: X \to Y\}$ be the set of functions from $X$ to $Y$.
  \item For $X$ a set, $\mathcal{P}(X) = 2^{X} = \{A \big\vert A \subseteq X\}$ is the power set of $X$.
    For example, $\emptyset, \{2,3,5,7,11,\ldots\} \in \mathcal{P}(\N)$.
  \item For $A \subseteq X$, let $X \setminus A = \{x \in X \big\vert x \notin a\}$ denote the complement of $A$. If clear from context, $A^{c}$ will be used.
  \item $\R_{> 0} = (0,\infty)$, $\R_{\geq 0} = \left[0,\infty\right)$, $\C^{\times} = \C \setminus \{0\}$
  \item If $f: X \to Y$ is a function and we do not want to give it a name, we simply write
    \begin{align*}
      (x \mapsto f(x)): X \to Y
    \end{align*}
    For example, we write $(x \mapsto x^{3}): \R \to \R$, or for $V$ a vector space, the bidual map can be written as
    \begin{align*}
      (\underbrace{v}_{\in V} \mapsto (\underbrace{f}_{\in V^{*}} \mapsto f(v))): V \to V^{**}
    \end{align*}
  \item We write $X \sqcup Y$ for the union $X \cup Y$ if $X$ and $Y$ are disjoint.
  \item $\IS^{n} = \{x \in \R^{n+1} \big\vert \|x\| = 1\}$, the $n$-sphere.
    For the circle $\IS^{1}$, we will embed it into the complex plane $\C$ and refer to its points as $z = e^{2 \pi i \phi} \in \IS^{1}$.
  \item $\ID^{n} = \{x \in \R^{n} \big\vert \|x\| \leq 1\}$, the $n$-disk.
  \item $\mathbb{B}^{n} = \{x \in \R^{n} \big\vert \|x\| < 1\}$, the open $n$-ball.
  \item $B_{\epsilon} (x) = \{y \in \R^{n} \big\vert \abs{y-x} < \epsilon\}$. Here $n \in \N$ is usually clear from context.
  \item $X \mono Y$: An injective map.
    If $X \subseteq Y$, we will also write $X \hookrightarrow Y$ for the inclusion map.
  \item $Y \epi X$: A surjective map.
  \item WLOG means without loss of generality. Used in proofs, it means that it suffices to prove the result with some additional assumption, because the general case follows from it.
\end{itemize}

\newpage

\subsection*{Note to the TAs}
The script is too long to cover all the material in the $4 \times 4$ hours in a PVK, so you have to be selective with what you cover and not.
I recommend skipping most proofs except those that show equivalences between definitions because they are used implicitly throughout the script (and in the lecture!).
%Some definitions and results are used all the time and some are only used in one section or chapter. In the latter case, this is remarked at the end of the section. 
I tried adding a lot of examples to every definition, but they are not spread out evenly. So before giving the course, check which definitions have many/few examples and choose/add more accordingly.
%The proofs are generally written in beta-reduced\footnote{\url{https://ncatlab.org/nlab/show/beta-reduction#informal_usage}} form unless we explicitly want to make use of a previous theorem.
As such, they tend to be slower but hopefully more easier to understand for someone struggling with the subject.

%After each day, please send an issue on gitlab with an estimate on how long each section took to cover. This will be very valuable to all future TAs.

While the content follows the lecture, I made some changes to the order in which they are presented. In particular:
\begin{itemize}
  \item Cover continuous functions before products and basis. If done the other way, we need to do alot of the stuff about continuous functions at once and it makes the section too long.
  \item Put topological groups and the topological orbit theorem at the end of chapter 2 and instead do glueing of topological spaces first.
    They are much more intuitive and more of a necessity in the later chapters, whereas the homogeneous spaces are pretty self contained.
    
\end{itemize}

%\subsubsection*{Feedback from 2022 PVK}
%The first chapter contained a \emph{lot} of definitions that the students knew already.  I would try to find a way to either skip them or to ensure that the students know them by heart.
%For this, I recommend that you
%\begin{itemize}
%  \item Skip definition of things that are covered in first year courses.
%\end{itemize}
