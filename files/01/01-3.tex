\subsection{Continuous maps}\label{sec:continuous-maps}
So far, we have studied individual topological spaces on their own; in isolation.
The notion of continuous maps lets us compare and contrast different topological spaces. 
One can gain a lot of insight of a topological space $X$ by studying how it can be continually mapped into another space $Y$.

Recall the definition of continuity from Analysis. A function $f: \R \to \R$ is \emph{continuous}\index{continuous}, if
\begin{align*}
  \forall x \in \R: \forall \epsilon > 0: \exists \delta > 0: \forall y \in \R: \abs{x - y} < \delta \implies \abs{f(x) - f(y)} < \epsilon
\end{align*}
and compare this with the topological definition of continuity:
\begin{dfn}[]
Let $X,Y$ be topological spaces. 
\begin{itemize}
  \item We say that a function $f: X \to Y$ is \textbf{continuous}, if the preimage of open subsets is open. So $\forall V \in \tau_Y: f^{-1}(V) \in \tau_X$
  \item We write $\Hom_{\Top}(X,Y)$ (or $\Top(X,Y)$) for the set of continuous maps $f: X \to Y$.
\end{itemize}
\end{dfn}


The following are pretty easy to prove:
\begin{rem}[]\label{rem:properties-continuous}
  Let $X,Y$ be topological spaces and $f: X \to Y$ a function.
  \begin{enumerate}
    \item The identity function $\id_X = (x \mapsto x): X \to X$ is continous.
      \item The composition of continuous functions is continuous.
    \item If $A \subseteq X$ is a subset and $f$ is continuous, then the restriction $f|_A: A \to Y$ is continuous (with respect to the subspace topology). In particular, the inclusion map $\iota: A \hookrightarrow X$ is continuous.
    \item If $X$ is discrete, then any function $f: X \to  Y$ is continuous. 
      If $X$ is indiscrete, then only constant functions are continuous. If $Y$ is indiscrete, then any function into $Y$ is continuous.
    \item $f$ is continuous if and only if for all $x \in X$, $f$ is \textbf{continuous at}\index{continuous!at point} $x$:
      For every neighborhood $V$ of $f(x)$ there exists a neighborhood $U$ of $x$ such that $f(U) \subseteq V$.
    \item Let $(X,d_X),(Y,d_Y)$ be metric spaces, $f: X \to Y$ a function.
      Then $f$ is continuous (with respect to the induced topology) if and only if 
      \begin{align*}
        \forall x \in X: \forall \epsilon > 0: \exists \delta> 0: \forall x' \in X: d_X(x,x') < \delta \implies d_Y(f(x),f(x')) < \epsilon
      \end{align*}
  \end{enumerate}
\end{rem}
Point (e) is used implicitly all the time, so make sure you can prove it and understand when it makes sense to use one definition or the other.

\begin{dfn}[]
  A bijective continuous map $f: X \to Y$ such that its inverse $f^{-1}$ is continuous is called a \textbf{homeomorphism}\index{homeomorphism} and we write $f: X \stackrel{\iso}{\to} Y$ or $X \iso Y$ and say that $X$ is \textbf{homeomorphic} to $Y$.
\end{dfn}
\begin{xmp}[]
  When two spaces are homeomorphic, it means that from the point of view of topology, they are the same.
  We can prove that two spaces are homeomorphic by providing two inverse maps and showing continuity.
  For now, it is rather difficult to prove that two spaces are \emph{not} homeomorphic. We will later define tools that will make this much easier.

\begin{itemize}
  \item \textbf{Lengths are not a topological property:}
    $(0,1)$ is homemorphic to $(-10,10)$ with maps
    \begin{align*}
      (x \mapsto  20x-10): (0,1) \to (-10,10)
      \quad \text{and} \quad 
      (x \mapsto  \frac{x + 10}{20}): (-10,10) \to (0,1)
    \end{align*}
  \item \textbf{Having ``corners'' is not a topological property:} 
    The circle $\IS^{1} \subseteq \R^{2}$ is homeomorphic to the square $Y = \del([0,1] \times [0,1]) \subseteq \R^{2}$.
    
  \item \textbf{Boundedness is not a topological property:}
    $(0,1) \subseteq \R$ is homeomorphic to $\R$. To see this, we first shape the line into a punctured circle.
    Then we put this circle into $\R^{2}$ centered at $(0,\tfrac{1}{2})$.
    Then for every point $p$ this circle (except for the north pole), we draw a straight line from the north pole to that point.
    This line will intersect the real line at some point and we set this point to be $f(p)$.
    This map is referrted to as \textbf{stereographic projection}.
\end{itemize}
\end{xmp}


\vfill
You should now know \ldots
\begin{itemize}
  \item the different equivalent definitions of when a function $f: X \to Y$ is continuous.
  \item properties of continuous functions (especially Remark~\ref{rem:properties-continuous}) and how to prove them.
  \item the difference between bijective, bijective continuous and homeomorphic maps and show examples where they are different.
\end{itemize}

