\subsection{Homotopy equivalence}
We define what it means for two spaces to be ``continuously deformable'' into eachother allowing squishing and blowing up points.

\begin{dfn}[]
  We say that two topological spaces $X,Y$ are \textbf{homotopy equivalent} and write $X \sim Y$, if there exist continuous maps $f: X \to Y$, $g: Y \to  X$ such that $g \circ f \sim \id_X$ and $f \circ g \sim \id_Y$.
  We call $f,g$ \textbf{homotopy equivalences} and $g$ a \textbf{homotopy inverse} of $f$.
\begin{align*}
  \xymatrix{
    X
    \ar@{->}[r]^f
    &
    Y
    \ar@{->}[r]^g
    &
    X
  }
  \xymatrix{
    X
    \ar@/^2ex/[rr]^{g \circ f}
    \ar@/_2ex/[rr]_{\id_X}
    \ar@{}[rr]|{\Downarrow}
    &
    &
    X
  }
  \qquad
  \xymatrix{
    Y
    \ar@/^2ex/[rr]^{f \circ g}
    \ar@/_2ex/[rr]_{\id_Y}
    \ar@{}[rr]|{\Downarrow}
    &
    &
    Y
  }
\end{align*}
A topological space $X$ is called \textbf{contractible}, if it is homotopy equivalent to $\{\ast\}$.
\end{dfn}
Note that a homotopy equivalence is much weaker than a homeomorphism.
\begin{xmp}[]\label{xmp:circle-euc-minus-point}
  \begin{itemize}
    \item 
      For example $\R^{n}$ is homotopy equivalent to the singleton space $\{\ast\}$
      as all continuous maps $\R^{n} \to \R^{n}$ are homotopic.
      So compactness is not a homotopical property.
    \item 
      The circle $\IS^{1}$ and $\R^{2} \setminus \{0\}$ are homotopy equivalent.
      In fact, $\IS^{n-1} \sim \R^{n} \setminus \{0\}$ for all $n \geq 1$.
      To see this, consider the inclusion map $\iota: \IS^{n-1} \hookrightarrow \R^{n} \setminus \{0\}$ and the ``normalize vector'' map
      $
      \rho: \R^{n} \to \IS^{n-1}, v \mapsto \frac{v}{\abs{v}}
      $
      we have $\rho \circ \iota = \id_{\IS^{n-1}}$ and
      \begin{align*}
        \iota \circ \rho = \rho \sim_H \id_{\R^{n} \setminus \{0\}}
        \quad \text{for} \quad 
        H(v,t) = t \cdot v + (1-t) \frac{v}{\abs{v}}
      \end{align*}
  \end{itemize}
\end{xmp}

\begin{rem}[]
  If $X$ and $Y$ are homotopy equivalent, then
  \begin{enumerate}
    \item $X$ path connected $\iff Y$ path connected
    \item $X$ connected $\iff Y$ connected
    \item Homotopy equivalence forms an ``equivalence relation'' in the following sense:
      For all topological spaces $X,Y,Z$:
      \begin{align*}
        X \sim Y \land Y \sim Z \implies X \sim Z, \quad X \sim X, \quad X \sim Y \implies Y \sim X
      \end{align*}
  \end{enumerate}
\end{rem}

We would like to find some criteria when spaces can be homotopy equivalent to each other. The following definition will aide us in doing so.

\begin{dfn}[] \label{dfn:retraction}
Let $X$ be a topological space, $A \subseteq X$ and let $\iota: A \hookrightarrow X$ be the inclusion map.

We say that $A$ is a \textbf{retract} of $X$, if there exists a continuous map $\rho: X \to A$ such that $\rho \circ \iota= \id_A$.
We call $\rho$ a \textbf{retraction} of $X$ unto $A$.
\begin{align*}
  \xymatrix{
    A
    \ar @{^{(}->}[r]^\iota
    &
    X
    \ar[r]^\rho
    &
    A
  }
\end{align*}
\end{dfn}

\begin{xmp}[]
  Informally, a retraction $\rho: X \to A$ is something that doesn't create any holes:
\begin{itemize}
  \item The set $A = [0,1] \subseteq X = [0,1] \cup [2,3]$ is a rectract of $X$.
  \item $A = \{a,b\} \subseteq X = [0,1]$ is \emph{not} a retract for $a \neq b$. (Why?)
  \item The circle $\IS^{1}$ is not a retract of the $2$-disk $\ID^{2}$ and we will prove this later.
    More generally, one can show that $\IS^{n-1} \subseteq \mathbb{D}^{n}$ is not a retract for all $n \geq 1$, but this requires some mathematical tools that aren't covered in this course. (They will be covered in algebraic topology which is a course you can take next semester winke-winke).
\end{itemize}
\end{xmp}


\begin{dfn}[]
Let $X$ be a topological space, $A \subseteq X$.
\begin{itemize}
  \item A retraction $\rho: X \to A$ is called a \textbf{deformation retraction} if additionally $\iota \circ \rho$ is homotopic to $\id_X$.
    \begin{center}
    \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
      X \arrow[]{r}{\rho} & A \arrow[hook]{r}{\iota} & X
    \end{tikzcd}
    \end{center}
  \item If the homotopy to the identity map can be chosen such that $h(a,t) = a$ for all $a \in A, t \in [0,1]$, the deformation retraction is called \textbf{strong}.
  \item $A$ is called a (strong) \textbf{deformation retract} of $X$, if such a (strong) deformation retraction $\rho$ exists.
\end{itemize}
\end{dfn}

\begin{xmp}[]
  Informally, deformation retracts are retracts that preserve holes:
  \begin{itemize}
    \item The subset $A = \IS^{n-1} \subseteq X = \R^{n} \setminus \{0\}$ is a strong deformation retract of $X$ as we can use the same mapping $\rho$ from Example~\ref{xmp:circle-euc-minus-point}
    \item The fine brush 
      \begin{align*}
        \text{Cone}\left(
          \{0\} \cup \{\frac{1}{n} \big\vert n \in \N^+\}
        \right)
      \end{align*}
  \end{itemize}
\end{xmp}

If retracts don't create holes and deformation retracts also preserve holes, then we should expect that the following holds:
\begin{lem}[]\label{lem:deformation-retract-homotopy}
  Every space $X$ is homotopy equivalent to its deformation retract $A \subseteq X$
\end{lem}
\begin{proof}
If $\rho$ is the deformation retract and $\iota: A \to X$ is the inclusion mapping, then
\begin{align*}
  \iota \circ \rho \sim \id_X \quad \text{and} \quad \rho \circ \iota = \id_A
\end{align*}
\end{proof}

\begin{xmp}[]
  \begin{itemize}
    \item 
  Glueing a punctured $n$-disk $\ID^{n} \setminus \{0\}$ onto a space $Y$ is a homotopy-invariant operation.

Let $\phi: \IS^{n-1} \to Y$ be continuous. Then for
\begin{align*}
  \iota(Y) \subseteq Y \cup_{\phi} (\mathbb{D}^{n} \setminus \{0\}) =: Q
\end{align*}
where $\iota:Y \to Q, y \mapsto [y]$ is the inclusion map, has as inverse a strong deformation retract which acts by making the hole in the puctured $n$-disk bigger and bigger until we reach $\iota(Y)$
The map would be
\begin{align*}
  \rho([v]) = \left[
    \frac{v}{\abs{v}}
    \right]
    = \left[
      \phi(\frac{v}{\abs{v}})
  \right]
  \quad \text{for} \quad v \in \ID^{n} \setminus \{0\}
\end{align*}

  \item 
Let $0 < k \leq n \in \N$. Then
\begin{align*}
  A := \frac{\sqrt{2}}{2}(\IS^{k-1} \times \IS^{n-k}) \subseteq \IS^{n} \setminus \left(
    \IS^{k-1} \times \{0\} \cup \{0\} \times \IS^{n-k}
  \right) =: X \subseteq \R^{k} \times \R^{n-k+1}
\end{align*}
is a strong deformation retract.

The mapping can be given by
\begin{align*}
  \rho: X \to  A, \quad (v,w) \mapsto \frac{\sqrt{2}}{2} \left(
    \frac{v}{\abs{v}}, \frac{w}{\abs{w}}
  \right)
\end{align*}
where the homotopy is 
\begin{align*}
  h((v,w),t) = 
  \frac{
  \left(
    tv + (1-t) \frac{\sqrt{2}}{2} \frac{v}{\abs{v}}
    ,
    tw + (1-t) \frac{\sqrt{2}}{2} \frac{w}{\abs{w}}
  \right)
}{
  \abs{
  \left(
    tv + (1-t) \frac{\sqrt{2}}{2} \frac{v}{\abs{v}}
    ,
    tw + (1-t) \frac{\sqrt{2}}{2} \frac{w}{\abs{w}}
  \right)
}
}
\end{align*}
up to normalisation.
  \end{itemize}
\end{xmp}


\begin{lem}[]\label{lem:w5-2}
Let $X$ be a topological space. Then $X$ is contractible if and only if there exists an $x_0 \in X$ such that $\{x_0\} \subseteq X$ is a deformation retract.
\end{lem}
\begin{proof}
The proof is trivial and is left as an exercise to the reader.
\end{proof}


But why would we study Homotopy and Homotopy equivalence?
\begin{itemize}
  \item Many topological properties of topological spaces are the same for homotopy equivalent spaces.
  \item Many classical algebraic invariants are the same for homotpy equivalent spaces. 
\end{itemize}
One such example in this course is the \textbf{Fundamental group} $\pi_1$.
The idea behind is to that to each topological space $X$ we associate a group $\pi_1(X)$ and to every continuous map $f: X \to Y$ we want to find a group homomorphism $\pi_1(X) \to \pi_2(X)$ that is a group isomorphism if $f$ is a homotopy equivalence.
In other words, we want to create a \textbf{functor} $\pi_1: \Top \to \Grp$.





