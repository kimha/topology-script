\subsection{Exercises}
\begin{exr}[]\label{exr:subspace-topology}
  Show that the \hyperlink{dfn:subspace-topology}{subspace topology} $\tau_Y$ satisfies all the requirements of a \hyperlink{dfn:topology}{topology}.
  Additionally, prove \emph{transitivity} of openness and closedness:
  \begin{itemize}
    \item If $A \subseteq Y \subseteq X$ and $A$ is open in $Y$ (with respect to the subspacetopology $\tau_Y$) and $Y$ is open in $X$, then $A$ is open in $X$.
    \item If $A \subseteq Y \subseteq X$ and $A$ is closed in $Y$ and $Y$ is closed in $X$, then $A$ is closed in $X$ 
  \end{itemize}
  Find examples $A \subseteq Y \subseteq X$ such that $A$ is open/closed in $Y$, but closed/open/neither in $X$.
  If $A$ is neither in $Y$, can it be open/closed in $X$?
\end{exr}
\begin{sol}
  \textbf{Subspace topology is a valid topology:} 
  We can write
  \begin{align*}
    \emptyset = \emptyset \cap Y
    ,\quad 
    Y = X \cap Y
    , \quad
    \bigcup_{i \in I}(U_i \cap Y) = \left(\bigcup_{i \in I} U_i\right) \cap Y
    , \quad
    (U \cap Y) \cap (V \cap Y) = (U \cap V) \cap Y
  \end{align*}
  so $\emptyset,Y$ the union of opens and finite intersections of opens are open in $Y$.

  \textbf{Transitivity of openness:} 
  If $A$ is open in $Y$, there exists a $U \subseteq X$ open with $A = U \cap Y$. If $Y$ is open in $X$, then $A = U \cap Y$ is the intersection of two open subsets of $X$.

  \textbf{Transivity of closedness:} 
  If $A$ is closed in $Y$ and $Y$ is closed in $X$, let $U \subseteq X$ open with $Y \setminus A = U \cap Y$ and $V \subseteq X$ with $X \setminus Y = V$.
  Then
  \begin{align*}
    (X \setminus A) = (X \setminus Y) \cup (Y \setminus A) = V \cup (U \cap Y) = V \cup (U \setminus \underbrace{Y^{c}}_{= V}) = V \cup U
  \end{align*}
  so the complement of $A$ is the union of two open sets in $X$, so $A$ is closed in $X$. (The proof very easy if you draw a picture).
  The following are counterexamples:
  \begin{itemize}
    \item $A$ is open in $Y$, but closed in $X$: $A = Y = X$.
      (Is also closed in $Y$ and open in $X$).
    \item $A$ open in $Y$, but neither in $X$: $A = Y = [0,1) \subseteq \R$. (Is also closed in $Y$ and neither in $X$)
    \item If $A \subseteq Y$ is open/closed in $X$, then it is also open/closed in $Y$ because $A = A \cap Y$, so if $A$ is neither in $Y$, it cannot be open/closed in $X$.
  \end{itemize}
\end{sol}

\begin{exr}[Alternative characterization of subspace toplogy]\label{exr:alt-subspace}
  Let $(X,\tau)$ be a topological space, $A \subseteq X$ be a subset and $\sigma \in \mathcal{P}(A)$ a topology on $A$.
  Show that the following are equivalent
  \begin{enumerate}
    \item $(A,\sigma)$ is the \hyperlink{dfn:subspace-topology}{subspace topology}.
    \item $(A,\sigma)$ is the \hyperlink{dfn:coarser-toplogy}{coarsest} topology on $A$ such that the inclusion map $\iota: A \hookrightarrow X$ is continuous.
  \end{enumerate}
\end{exr}
\begin{sol}
  Denote the subspace topology by $\sigma_0 \subseteq \mathcal{P}(A)$.

  Let $U \in \tau$. Then $\iota^{-1}(U) = U \cap A$, so $\iota: (A,\sigma_0) \to (X,\tau)$ is continuous.
  Now let $\sigma'$ be another topology such that $\iota: (A,\sigma') \to (X,\tau)$ is continuous and let $V = U \cap A \in \sigma_0$ for some $U \in \tau$. By continuity of $\iota$, $V = \iota^{-1}(U)$ is open in $\sigma'$, so $\sigma_0$ is coarser than $\sigma'$.

\end{sol}

\begin{exr}[Alternative characterization for interior, closure and boundary]\label{exr:alternative-interior-closure-boundary}
  Let $X$ be a topological space and $B \subseteq X$.
  Prove the following statements
  \begin{enumerate}
    \item $B^{\circ} \subseteq B$ and it is the largest open set contained in $B$. In fact
      \begin{align*}
        B^{\circ} = \bigcup_{U \subseteq B \text{\ open}} U
      \end{align*}
    \item $\overline{B} \supseteq B$ and is the smallest closed set that contains $B$. In fact
      \begin{align*}
        \overline{B} 
        &= \bigcap_{C \supseteq B \text{\ closed}} C
        \\
        &= \left\{
          x \in X \big\vert U \cap B \neq \emptyset \text{\ for every neighborhood $U$ of $x$}
        \right\}
      \end{align*}
    \item $\del B = \overline{B} \setminus B^{\circ}$ and $\del B$ is closed.
  \end{enumerate}
\end{exr}
\begin{sol}
The first two are from exercise sheet 1.
The third one follows from the fact that for any $x \in X$ we have
\begin{align*}
  B^{c} \text{\ is not a neighborhood of $x$} \quad \iff  \quad x \in \overline{B}
\end{align*}
and
\begin{align*}
  \text{$B$ is not a neighborhood of $x$}
  \quad \iff  \quad x \notin B^{\circ}
\end{align*}
$\del B$ is closed because $\overline{B} \setminus B^{\circ} = \overline{B} \cap (B^{\circ})^{c}$ is the intersection of two closed sets.
\end{sol}

\begin{exr}[]\label{exr:properties-interior-closure-boundary}
  Let $X$ be a topological space and $B \subseteq X$. Prove or disprove:
\begin{enumerate}
  \item If $B$ is closed, then $\overline{B} = B$.
  \item If $B$ is open, then $B^{\circ} = B$.
  \item $\del B = \del (B^{c})$.
  \item $\del B = \del (\overline{B})$
  \item ${(\overline{B})}^{\circ} = B^{\circ}$
  \item (Trichotomy) $X = B^{\circ} \sqcup \del B \sqcup {(B^{c})}^{\circ}$.
  \item $\overline{(B^{\circ})} = \overline{B}$
  \item ${(\del B)}^{\circ} = \emptyset$
  \item $\del(\del B) = \del B$
  %\item $\del^{2}B = \del^{3} B$
\end{enumerate}
\end{exr}
\begin{sol}
  The first two follow immediately from the previous exercise.
  (d), (e), (g), (h), (i) are wrong.
  \begin{enumerate}
    \setcounter{enumi}{2}
    \item A point $x \in X$ is a boundary point of $B$, if neither $B$ nor $B^{c}$ are neigborhoods of $x$.
      The result follows from exchanging $B$ with $B^{c}$.
    \item Take $B = (-1,0) \sqcup (0,1)$. Then $\del B= \{-1,0,1\}$, but $\overline{B} = (-1,1)$ so $\del(\overline{B}) = \{-1,1\}$.
    \item Again, take $B = (-1,0) \sqcup (0,1)$. Then
      \begin{align*}
        B^{\circ} = (-1,0) \sqcup (0,1) \quad \text{but} \quad \overline{B} = (-1,1) \implies {(\overline{B})}^{\circ} = (-1,1)
      \end{align*}
    \item Let $x \in X$. Then one of the following is true:
      \begin{itemize}
        \item $B$ is a neighborhood of $x$, so $x \in B^{\circ}$.
        \item $B^{c}$ is a neighborhood of $x$, so $x \in (B^{c})^{\circ}$
        \item Neither $B$ nor $B^{c}$ are neighborhoods of $x$, so $x \in x \in \del B$.
      \end{itemize}
      Disjointness follows because $B$ and $B^{c}$ cannot both be neithborhoods of $x$ as $B^{\circ} \cap (B^{c})^{\circ} \subseteq B \cap B^{c} = \emptyset$.
    \item Take $B = \Q \subseteq \R$. Then $\Q^{\circ} = \emptyset$ and $\overline{\Q} = \R$.
    \item Take again $B = \Q \subseteq \R$. Then $(\del \Q)^{\circ} = \R$.
    \item Again, $B = \Q \subseteq \R$ is a counterexample.
  \end{enumerate}
\end{sol}

There are of alternative ways to define a topology.
\begin{itemize}
  \item Instead of focusing on the open sets, we could just as well have started with the closed subsets, where we swap the finiteness condition for unions and intersections.
  \item Haussdorff's approach was to focus on neighborhoods instead of the open sets: A topological space is a tuple $(X,\mathcal{U})$ consisting of a set $X$ and a collection of families of subsets $\mathcal{U} = \{\mathcal{U}_x\}_{x \in X}$ with $\mathcal{U}_x \in \mathcal{P}(X)$ (the neighborhoods of $x$) such that
    \begin{enumerate}
      \item $x \in U_x$ and $X$ is a neighborhood of everypoint.
      \item If $V$ contains a neighborhood of $x$, then $V$ is also a neighborhood of $x$
      \item The intersection of two neighborhoodsof $x$ is again a neighborhood of $x$.
      \item Every neighborhood of $x$ contains a neighborhood of $x$ that contains all of its points.
    \end{enumerate}
  \item A more motivated approach is that using Kuratowskis \textbf{Hull axioms}.
    

    A topological space is a tuple $(X, \overline{\phantom{ }})$ consisting of a set $X$ and a map $\overline{\cdot}: \mathcal{P}(X) \to  \mathcal{P}(X)$ that satisfies
    \begin{enumerate}
      \item $\overline{\emptyset} = \emptyset$
      \item $A \subseteq \overline{\emptyset}$ for all $A \subseteq X$
      \item $\overline{\overline{A}} = \overline{A}$ for all $A \subseteq X$
      \item $\overline{A \cup B} = \overline{A} \cup \overline{B}$ for all $A,B \subseteq X$
    \end{enumerate}
\end{itemize}


\begin{exr}[]
Show that all alternative definitions (given above) of a topological space are equivalent in the following sense
\begin{enumerate}
  \item[(a1)] Let $(X,\tau)$ be a topological space. Show that the closure operator $\overline{\phantom{ }}$ satisfies the Hull axioms.
  \item[(a2)] Let $(X,\overline{\phantom{ }})$ be a topological space according to Kuratowskis definition. Then there exists a unique topology on $X$ such that for all $A \subseteq X$, $\overline{A}$ is the closure of $A$.
  \item[(b1)] Let $(X,\tau)$ be a topological space.
    Show that the family of neighborhoods $\{\mathcal{U}_x\}_{x \in X}$ satisfy the neighborhood axioms.
  \item[(b2)] Let $(X,\mathcal{U})$ be a topological space according to Hausdorff. Show that there exists a unique topology on $X$ such that $\mathcal{U}$ is the family of neighborhoods of the topology.
\end{enumerate}
\end{exr}
\begin{sol}
See Exercise Sheet 1.
\end{sol}

\begin{exr}[]
Show that there exist topological spaces that are not \textbf{metrisable}, i.e.\ there does not exist a metric on the set that induces the topology.
\end{exr}
\begin{sol}
  Take any set $X$ with more than two elements and the indiscrete topology $\tau = \{\emptyset, X\}$. The positive definiteness forbids this.
\end{sol}

\begin{exr}[]\label{exr:different-metrics}
  Let $(X,d)$ be a metric space. Show that there exists a bounded metric $\tilde{d}$ on $X$ that induces the same topology.
  Bounded here means $\exists R >0: \forall x,y \in X: \tilde{d}(x,y) \leq R$.
\end{exr}
\begin{sol}
  Define
  \begin{align*}
    \tilde{d}(x,y) = \frac{d(x,y)}{1 + d(x,y)} < 1
  \end{align*}
  Using the fact that function $f(x) = \frac{x}{1 + x}$ is strictly monotonously increasing (since $f'(x) > 0$ for $x > -1$), we conclude the equivalence
  \begin{align*}
    d(x,y) <\epsilon \iff \frac{d(x,y)}{1 + d(x,y)} < \frac{\epsilon}{1 + \epsilon}
  \end{align*}
  so they both induce the same topology.
\end{sol}

\begin{exr}[]
Show that if $f: X \to Y$ is a homeomorphism and $A \subseteq X$ is a subset, then $f|_A: A \to f(A)$ is a homeomorphism.
\end{exr}
\begin{sol}
Let $f^{-1}: Y \to X$ denote the continuous inverse to $f$.
If $V$ is open in $Y$, then $f^{-1}(V \cap f(A)) = f^{-1}(V) \cap A$ is open in $A$.
If $U$ is open in $X$, then $f(U \cap A) = (f^{-1})^{-1}(U) \cap f(A)$ is the open in $f(A)$ by continuity of $f^{-1}$.
\end{sol}

\begin{exr}[]
\begin{enumerate}
  \item Find a bijective function $f: X \to Y$ that is not continuous.
  \item Find a continuous bijective function $f: X \to Y$ that is not a homeomorphism.
\end{enumerate}
\end{exr}
\begin{sol}
\begin{enumerate}
  \item For $X$ a set and $\tau,\tau'$ different topologies, the identity $(x \mapsto x): X \to X$ is bijective but not continuous.
    
    Another example is the function
    \begin{align*}
      [0,1] \to [0,1),
      x \mapsto \left\{\begin{array}{ll}
          \tfrac{x}{2} & x \in \{\tfrac{1}{2^{n}} \big\vert n \in \N\}\\
         x & \text{otherwise}
      \end{array} \right.
    \end{align*}
\end{enumerate}
\end{sol}


%Note that \emph{not} every open subset $W \subseteq X \times Y$ is of the form $W = U \times Y$, for $U \subseteq X$, $V \subseteq Y$ open. For example, the product topology on $\R^{2}$ contains open circles.

\begin{exr}[Continuous in both variables?]
  Let $X,Y$ be \emph{non-empty} topological spaces, and $f: X \times Y \to Z$ a function. 
  For $x_{0} \in X$ and $y_0 \in Y$, define
\begin{align*}
  f_{x_0} &= f(x_0,-) = (y \mapsto f(x_0,y)): Y \to \Z
  \\
  f_{y_0} &= f(-,y_0) = (x \mapsto f(x,y_0)): X \to \Z
\end{align*}
  \begin{enumerate}
    \item If $f$ is continuous, are $f_{x_0}$ and $f_{y_0}$ also continuous?

      \hfill\textsf{Hint (backwards): {}.snoitcnuf suounitnoc owt fo noitisopmoc eht sa $f_{x_0}$ etirW} 
      
    \item If $f_x$ and $f_y$ are continuous for all $x \in X, y \in Y$, does it follow that $f$ is continuous?
\end{enumerate}
\end{exr}
\begin{sol}
  \begin{enumerate}
    \item For $x_0 \in X$ consider the function
      \begin{align*}
        \iota_{x_0} = (y \mapsto (x_0,y)): Y \to X \times Y
      \end{align*}
      We claim that this function is continuous. From this it follows that 
      \begin{align*}
        f_{x_0} = f \circ \iota_{x_0} = \left(
          y \mapsto (x_0,y) \mapsto f(x_0,y)
        \right)
      \end{align*}
      is the composition of two continuous functions and is thus continuous.

      \emph{Proof claim:} 
      Let $y \in Y$. We show that $\iota_{x_0}$ is continuous at $y$ (see Remark~\ref{rem:properties-continuous}-(e)).
      Let $W \subseteq X \times Y$ be a (WLOG open) neighborhood of $\iota_{x_0}(y)$.
      By definition of the product topology, there exist $U \subseteq X, V \subseteq Y$ open with $(x_0,y) \in U \times V \subseteq W$.
      This means
      \begin{align*}
        y = \pi_Y(x_0,y) \in \pi_Y(U \times V) = V
        \quad \text{and} \quad 
        \iota_{x_0}(V) = \{x_0\} \times V \subseteq U \times V \subseteq W
      \end{align*}
      As $y \in Y$ was arbitrary, $\iota_{x_0}$ is continuous.
    \item 
  No. If we take $X = Y$, the assumption then only says that $f$ is continuous along the perpendicular lines, but not necessarily along the diagonal.
  An example of such a function is
  \begin{align*}
    f(x,y) = \left\{\begin{array}{ll}
      \frac{xy}{x^{2} + y^{2}} & (x,y) \neq (0,0)\\
      (0,0) & (x,y) = (0,0)
    \end{array} \right.
  \end{align*}
  Along the diagonal $x = y > 0$, its value is always $\frac{1}{2}$, which comes arbitrarily close to $(0,0)$, where the function is zero.
\end{enumerate}
\end{sol}



\begin{exr}[]
Let $X,Y,Z$ be topological spaces. A function $f: Z \to X \times Y$ is continuous if and only if the compositions
\begin{align*}
  f_X: Z \stackrel{f}{\to}X \times Y \stackrel{\pi_X}{\to} X \quad \text{and} \quad 
  f_Y: Z \stackrel{f}{\to}X \times Y \stackrel{\pi_Y}{\to} Y
\end{align*}
are both continuous.
\end{exr}
\begin{sol}
  \textbf{``$\bm{\implies}$'':}  If $f$ is continuous, then $f_X = \pi_X \circ f$ and $f_Y = \pi_Y \circ f$ are compositions of continuous functions.

  \textbf{``$\bm{\impliedby}$'':} We show that $f$ is continuous at every point (see Remark~\ref{rem:properties-continuous}. Let $z \in Z$ and $W \subseteq X \times Y$ a neighborhood of $f(z)$.
  By the definition of the product topology, there exist open sets $U \subseteq X, V \subseteq Y$ such that $U \times V \subseteq W$.
  By continuity of $f_X,f_Y$, the sets
  \begin{align*}
    (\pi_X \circ f)^{-1}(U) =  \{z \in Z \big\vert f(z) \in U \times Y\}
    \quad \text{and} \quad 
    (\pi_Y \circ f)^{-1}(V) =  \{z \in Z \big\vert f(z) \in X \times V\}
  \end{align*}
  are open in $Z$. Then their intersection is also open, which just happens to be
  \begin{align*}
    f_X^{-1}(U) \cap f_Y^{-1}(V) = \{z \in Z \big\vert f(z) \in U \times V\}
  \end{align*}
  which is an open neighborhood of $z$ such that its image is contained in $W$, proving continuity of $f$.
\end{sol}

\begin{exr}[Understanding the product topology]\label{exr:understanding-product}
  \begin{enumerate}
    %\item Show that the product topology is finer (as in Definition~\ref{dfn:finer-coarser}) than the box topology. That is, if $W \subseteq X \times Y$ is open in $\tau_{\text{box}}$, then it is also open in $\tau_{X \times Y}$.
    %\item Find topological spaces $X,Y$ and a subset $W \subseteq X \times Y$ that is open in the product topology, but is not open in the box topology.
    \item 
      Show that the product topology on $\R \times \R$, where $\R$ has the euclidean topology is the euclidean topology on $\R^{2}$.
    \item Show that the projection maps $\pi_X, \pi_Y$ are open maps i.e.\ if $W \subseteq X \times Y$ is open, then $\pi_X(W) \subseteq X$ and $\pi_Y(W) \subseteq Y$ are open.
      
      \textsf{Hint (backwards): {}.\ref{rem:alt-open}~krameR esU}
  \end{enumerate}
\end{exr}
\begin{sol}
  \begin{enumerate}
    \item Let $A \subseteq \R^{2}$ and $a = (x,y) \in A$ arbitrary.
      If $A$ is open in the product topology, there exist open sets $U,V \subseteq \R$ such that $a \in U \times V \subseteq A$.
      So there exist open intervals $B_{\epsilon}(x) \subseteq U, B_{\epsilon'}(y) \subseteq V$.
      For $\delta = \min\{\epsilon,\epsilon'\}$ we then have
      \begin{align*}
        B_{\delta}(a) \subseteq B_{\epsilon}(x) \times B_{\epsilon'}(y) \subseteq U \times V \subseteq A
      \end{align*}

      If $A$ is open in the euclidiean topology, there exists an $\epsilon > 0$ with $B_{\epsilon}(a) \subseteq A$.
      Then 
      \begin{align*}
        B_{\frac{\epsilon}{\sqrt{2}}}(x) \times B_{\frac{\epsilon}{\sqrt{2}}}(y) \subseteq B_{\epsilon}(a) \subseteq A
      \end{align*}
      so $A$ is also open in the product topology.
    \item Let $W \subseteq X \times Y$ be open. If $\pi_X(W) = \{x \in X \big\vert (x,y) \in W \text{\ for some }y \in Y\}$ is empty, it is open and we are done. 
      Otherwise, let $x_0 \in \pi_X(W)$.
      We wish to find an open set $U \subseteq X$ with $x_0 \in U \subseteq \pi_X(W)$ (see Remark~\ref{rem:alt-open} why).
      Let $y_0 \in Y$ such that $(x_0,y_0) \in W$.
      By definition of the product topology, $W$ being open means that there exist $U \subseteq X, V \subseteq Y$ open with $(x_0,y_0) \in U \times V \subseteq W$. So
      \begin{align*}
        x_0 = \pi_X(x_0,y_0) \in \pi_X(U \times V) = U 
        \quad \text{and} \quad 
        U = \pi_X(U \times V) \subseteq \pi_X(W)
      \end{align*}
      as $x_0 \in \pi_X(W)$ was arbitrary, $\pi_X(W)$ is open in $X$.
      The proof for $\pi_Y(W)$ is analogous.
  \end{enumerate}

\end{sol}

%Note that \emph{not} every open subset $W \subseteq X \times Y$ is of the form $W = U \times Y$, for $U \subseteq X$, $V \subseteq Y$ open. For example, the product topology on $\R^{2}$ contains open circles.


\begin{exr}[]
  Let $A,B \subseteq X$.
Which of the following are true?
\begin{enumerate}
  \item If $A$ is connected, then $A^{c}$ is connected.
  \item If $A,B$ are connected, then $A \cap B$ is connected.
  \item If $A,B$ are connected, then $A \cup B$ are connected.
  \item If $A,B$ are disconnected, then $A \cap B$ is disconnected.
  \item If $A,B$ are disconnected, then $A \cup B$ is disconnected.
  \item Let $W \subseteq X \times Y$.
    If $\pi_X(W)$ and $\pi_Y(W)$ are connected, then $W$ is connected.
\end{enumerate}
\end{exr}
\begin{sol}
None of these are true. If you guessed wrong, look for counterexamples in $\R^{2}$ before checking the solution.

\begin{enumerate}
  \item $A = [0,1] \subseteq \R$.
  \item $A = \{x + yi \in \IS^{1} \big\vert x < \tfrac{1}{2}\}$, $B = \{x + yi \in \IS^{1} \big\vert x > -\tfrac{1}{2} \}$.
  \item $A = \{0\}, B = \{1\}$
  \item $A = \IS^{1} \setminus \{\pm 1\}$, $B = \IS^{1} \setminus \{\pm i\}$.
  \item $A = [0,1] \cup [2,3]$,  $B = [2,3] \cup [4,5]$
  \item $W = \{(x,y) \in \R^{2} \big\vert x \neq y\}$.
\end{enumerate}
\end{sol}
\begin{exr}[]
Find a topological space that is connected but not path connected.
Find more than one.
\end{exr}
\begin{sol}
A famous example is the Topologists sine curve
\begin{align*}
  X = \{0\} \times [-1,1] \sqcup
  \left\{
    (t, \sin \tfrac{1}{t}) \in \R^{2} \big\vert t \in (0,\pi]
  \right\}
  \subseteq \R^{2}
\end{align*}
which is connected, but not path-connected.
Let's call the two components $X_0$ and $X_1$.

Let $a = (\pi,0)$ and $b = (0,0)$.
Assume there exists a path $[0,1] \to X$ from $a$ to $b$.

Since the set $\{t \in [0,1] \big\vert \gamma(t) = 0\}$ is non-empty and closed, it attains its maximum, let's call it $s$.
But then
\begin{align*}
  \gamma([0,s)] \subseteq (0,1] \implies \lim_{t \to s} \gamma(t) = 0 \text{\ and \ } \gamma(0) = \pi\\
  \gamma_1([0,s]) = (0, \pi] \implies \gamma([0,s) = X_1
\end{align*}
By the form of the sine curve, we can get a sequence of points $(t_n)_{n \in \N}$ in $[0,1]$ whose image under $\gamma$ are the peaks of the sine curve, so
\begin{align*}
  \lim_{n \to \infty} t_n = s \quad \text{and} \quad \gamma(s_n) = (?,1)
\end{align*}
(here, the ``?'' means that the position of the $x$-axis is unimporant).
Likewise, there must be a sequence of points $(s_n)_{n \in \N}$ in $[0,1]$ whose images are the valleys of the sine curv, so
\begin{align*}
  \lim_{n \to \infty} s_n = s \quad \text{and} \quad  \gamma(s_n) = (?,-1)
\end{align*}
which contradicts continuity of $\gamma$.

The fact that $X$ is connected boils down to the fact that every neighborhood of $(0,0)$ must intersect $X_1$.

The integers $\Z$ with the co-finite topology is connected but not path-connected.
We do not provide a proof of this. Please search online.
\end{sol}


\begin{exr}[]
Show that the three spaces $[0,1]$, $[0,1)$ and $(0,1)$ are pairwise \emph{not} homeomorphic.
\end{exr}
\begin{sol}
$[0,1]$ is the only one of the three that is compact.

Assume $f: [0,1) \to (0,1)$ were a homeomorphism and set $p = f(0)$. Then $f|_{(0,1)}$ would be a continous map from a connected set $(0,1)$ surjecting to a disconnected set $(0,p) \sqcup (p,1)$.
\end{sol}

\begin{exr}[Cantor-Schröder-Bernstein?]
  Recall the Cantor-Schröder-Bernstein theorem from Analysis I (which was pretty hard to prove):
  Let $X,Y$ be sets, $f: X \mono Y$ and $g: Y \mono X$ injective functions.
  Then $X$ and $Y$ are in bijection.

  Is the continuous analoague true? Let $X,Y$ be topological spaces and $f: X \to Y, g: Y \to X$ functions.
  \begin{enumerate}
    \item If $f,g$ are continuous injective functions,
      does it follow that $X$ and $Y$ are homeomorphic?
    \item If $f,g$ are continous surjective functions, does it follow that $X$ and $Y$ are homeomorphic?
    \item If $f,g$ are continous bijective functions, does it follow that $X$ and $Y$ are homeomorphic?
    %\item Let $X,Y$ be \emph{finite} topological spaces, $f: X \mono Y$ and $g: Y \mono X$ injective continuous functions.
  %Does it follow that $X$ and $Y$ are homeomorphic?
%\item Let $f,g$ are continous bijections, does it follow that $X$ and $Y$ are homeomorphic?
  \end{enumerate}
\end{exr}
\begin{sol}
  \begin{enumerate}
    \item If you recall from the proof of the original theorem, the constructed bijection is highly non-continuous, so the answer is no.
      Set $X = [0,1)$ and $Y = [0,1]$ and consider the maps
  \begin{align*}
    \iota: X \hookrightarrow Y
    \quad \text{and} \quad 
    g = (x \mapsto \tfrac{x}{2}): [0,1] \mono [0,1)
  \end{align*}
  As shown in the previous exercise, $X$ and $Y$ are not homeomorphic.

  \item No. Take $X = \R$ and $Y = [0,\infty)$ with the maps
    \begin{align*}
      (x \mapsto x^{2}): \R \to [0,\infty)
      \quad \text{and} \quad 
      [0,\infty) \hookrightarrow \R
    \end{align*}
  \item We start with the counter-example from (a).
    Let
    \begin{align*}
      X_0 := [0,1), &\quad Y_0 := [0,1]\\
      f_0: X_0 \to Y_0, &\quad x \mapsto x\\
      g_0: Y_0 \to X_0, &\quad x \mapsto \frac{x}{2}
    \end{align*}
    Because $f_0$ is not surjective, let's also add a point $\{2\}$ to $X_0$ and map it to $\{1\}$. (We don't add $\{1\}$ to the right end of $[0,1)$ because we don't want $X$ and $Y$ to be homeomorphic.)

    Because $g_0$ is not surjective (its image is missing $(\tfrac{1}{2},1)$) let's add a line segment $(3,4)$ to $X_0$ and map it to $(\tfrac{1}{2},1)$ aswell as a point $\{5\}$ to make up for the change in $Y_0$.
    We then have
    \begin{align*}
      X_1 := [0,1) \sqcup \{2\}, \quad Y_1 := [0,1] \sqcup (3,4) \sqcup \{5\}
    \end{align*}
    with maps $f_1,g_1$ as described above.
    But then, $f_1$ is again no longer surjective so add the line segment $(6,7)$ and the point $\{5\}$ to $X_1$.
    $g_1$ is also no longer surjective so we add a line $(3,4)$ and another point $\{8\}$.

    Repeat to infinity and define the sets
    \begin{align*}
      X &:= [0,1) \sqcup \{2\} \sqcup (3,4) \sqcup \{5\} \sqcup (6,7) \sqcup \{8\} \sqcup \ldots 
      \\
      &= [0,1) \sqcup \bigcup_{n = 1}^{\infty} \{3n - 1\} \sqcup (3n,3n+1)\\
      Y &:= [0,1] \sqcup (3,4) \sqcup \{5\} \sqcup (6,7) \sqcup \{8\} \ldots 
      \\
        &= (X \setminus \{2\}) \cup \{1\}
    \end{align*}
    with functions
    \begin{align*}
      f: X \to Y, \quad
      x \mapsto \left\{\begin{array}{ll}
        1 & x = 2\\
        x & \text{otherwise}
      \end{array} \right.
    \end{align*}
    \begin{align*}
      g: Y \to X, \quad
      y \mapsto \left\{\begin{array}{ll}
        \tfrac{y}{2} & y \leq 2\\
        y - 3 & \text{otherwise}
      \end{array} \right.
    \end{align*}
    This defines continuous bijections between non-homeomorphic spaces.
\end{enumerate}
\end{sol}

\begin{exr}[Generating a topology (Lemma~\ref{lem:generated-topology})]
  Let $(X,\tau)$ be a topological space and $\mathcal{B} \subseteq \mathcal{P}(X)$ a collection of subsets.
  Show that the following are equivalent:
  \begin{enumerate}
    \item $\mathcal{B}$ is a subbasis of $\tau$.
    \item $\tau$ is the smallest topology on $X$ containing $\mathcal{B}$.
  \end{enumerate}
\end{exr}
%\begin{sol}
%  \textbf{(a) $\bm{\implies}$ (b):} 
%  Let $\sigma$ be a topology on $X$ containing $\mathcal{B}$ and let $U \in \tau$.
%  Since $\mathcal{B}$ is a subbasis for $\tau$, $U$ can be written as a union of finite intersection of elements of $\mathcal{B} \subseteq \sigma$. Since $\sigma$ is a topology, $U \in \sigma$
%
%  \textbf{(b) $\bm{\implies}$ (a):} TODO.
%\end{sol}


\begin{exr}[Alexandroff one-point-compactification]
  Let $X$ be a topological space and let $\infty$ be some symbol not in $X$.
  The one-point compactification of $X$ is the space $(X^{\ast},\tau^{\ast})$ with
  \begin{align*}
    X^{\ast} = X \sqcup \{\infty\} 
    ,\quad 
    \tau^{\ast} = \tau \cup \left\{
      (X \setminus K) \cup \{\infty\} \big\vert K \text{\ is compact and closed}
    \right\}
  \end{align*}
  \begin{enumerate}
    \item Show that $X^{\ast}$ is compact.
    \item Find subsets of $\R^{m}$ that are homeomorphic to the one-point compactification of the following spaces
      \begin{enumerate}
        \item $X = [0,1)$
        \item $X = (0,1)$
        \item $X = [0,1]$
        \item $X = \R^{n}$
        \item $X = \mathbb{B}^{2}$
        \item $X =$ the shape ``$X$'' $\subseteq \R^{2}$ without the corners.
      \end{enumerate}
  \end{enumerate}
\end{exr}
See Exercise Sheet 4 for solutions.


\begin{exr}[Compact-open topology]\label{exr:compact-open}
  From linear algebra, you should know that the set of linear maps $f: V \to W$ forms a vector space $\Hom(V,W)$.
  In this exercise, we would like to put a topology on the set of continuous functions $f: X \to Y$.
  For $K \subseteq X$ compact and $U \subseteq Y$ open, let
  \begin{align*}
    V(K,U) = \{f: X \to Y \big\vert f(K) \subseteq U\}
  \end{align*}
  The topology \hyperlink{lem:generated-topology}{generated by} these $V(K,U)$ is called the \hypertarget{dfn:compact-open}{\textbf{compact-open}} topology and we write $\Hom_{\Top}(X,Y)$ (or $Y^{X}$) for this space.

  Show the following:
  \begin{enumerate}
    \item Let $n \in \N$ and take $X = \{1,\ldots,n\}$ with the discrete topology and $Y = \R$.
      What are the open sets in $\Hom_{\Top}(X,Y)$?
      Compare the result with the euclidean topology on $\R^{n}$.

    \item If $X$ is compact and $(Y,d_Y)$ is a metric space, define a metric on $\Hom_{\Top}(X,Y)$.
    \item Show that this metric \hyperlink{dfn:induced-topology}{induces} the compact-open topology.
  \end{enumerate}
\end{exr}
\begin{sol}
  \begin{enumerate}
    \item For an $f \in \Hom_{\Top}(X,Y)$, write $\vec{f} = (f(1),f(2),\ldots,f(n)) \in \R^{n}$.
      A collection of functions $W \subseteq \Hom_{\Top}(X,Y)$ is open, if and only if for every $f \in V$, there exist a $K \subseteq X$ compact and $U \subseteq \R$ open such that $f \in V(K,U)$ and $V(K,U) \subseteq W$.

      Stating this in terms of $\vec{W} = \{\vec{f} \big\vert f \in V\} \subseteq \R^{n}$, we see that the $V(K,U)$ correspond to subsets of the form $\bigcap_{i \in K}\pi_i^{-1}(U)$, which form a subbasis of the product topology.
    \item 
  Let $f,g \in \Hom_{\Top}(X,Y)$ and define
  \begin{align*}
    d(f,g) := \sup_{x \in X} \{d_Y(f(x),g(x))\}
  \end{align*}
  because $X$ is compact, the function $(x \mapsto d(f(x),g(x)))$ is bounded, so this metric is well-defined.

  Positive definiteness and the triangle inequality follow from the properties of $d_Y$.
  \item Missing.
  \end{enumerate}
\end{sol}

\begin{exr}[]
  Consider the space $X = (\mathbb{R},\tau)$ with
  \begin{align*}
    \tau = \{[a,\infty) \big\vert a \in \mathbb{R} \cup \{\pm \infty\}\}
  \end{align*}

  Classify all continuous functions $f: X \to \R$.

  \textsc{Hint (backwards): {}.tnatsnoc eb tsum noitcnuf suounitnoc yna taht wohS}
\end{exr}
\begin{sol}
  Let $f: X \to \R$ be continuous. Assume it takes on the value $0$ at $x_0 \in \R$. Then $\forall \epsilon > 0$ continuity implies that
  \begin{align*}
    x_0 \in f^{-1}((-\epsilon,\epsilon)) = [a,\infty) \text{ für ein $a \in \mathbb{R} \cup \{\pm 1\}$}
  \end{align*}
  It follows $a \leq x_0$, so for all $x \in [x_0, \infty)$, the function must be constant $0$.

  The same holds true by varying $f(x_0) = y$ for $y \in \R$. Letting $x_0 \to - \infty$, we see that the continuous functions are precisely the constant ones.
\end{sol}

%\begin{exr}[]
%Find examples of continuous functions $f: X \to Y$ such that
%\begin{enumerate}
%  \item $f(K)$ is compact, for every compact set $K \subseteq Y$.
%\end{enumerate}
%\end{exr}

