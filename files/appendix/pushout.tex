\section{The pushout*}\label{sec:pushout}
\textbf{Note:} This is supplementary material and not part of the lecture material itself.
\subsection{Pushout of sets}
\subsection*{The disjoint union}
Recall that for sets $X,Y$, their disjoint union is a set $X \sqcup Y = \{(x,0) \big\vert x \in X\} \cup \{(y,1) \big\vert y \in Y\}$ that comes with two maps
\begin{align*}
  X \stackrel{\iota_X}{\hookrightarrow} &X \sqcup Y \stackrel{\iota_Y}{\hookleftarrow Y}\\
  x \mapsto &(x,0)\\
  &(y,1) \mapsfrom y
\end{align*}
and it has the following \textbf{universal property}: For any other set $T$ with maps $X \stackrel{f}{\to}T \stackrel{g}{\from} Y$, there exists a unique map $\phi: X \sqcup Y \to T$ such that the following diagram commutes:
\begin{center}
  \begin{tikzcd}[]
    & Y
    \arrow[]{d}{\iota_Y}
    \arrow[bend left]{ddr}{g}
    \\
    X 
    \arrow[]{r}{\iota_X}
    \arrow[bend right,swap]{drr}{f}
    & X \sqcup{} Y
    \arrow[dotted]{dr}{\exists!\phi}
    \\
    &&T
  \end{tikzcd}
\end{center}
Indeed, the map $\phi: X \sqcup Y \to T$ is given by
\begin{align*}
  \phi(z)
  = \left\{\begin{array}{ll}
    f(x) & \text{if } z = (x,0)\\
    g(y) & \text{if } z = (y,1)
  \end{array} \right.
\end{align*}
\subsubsection*{The (usual) union}
As it turns out, the usual union of sets is a special case of a generalisation of the disjoint union.
\begin{dfn}[Pushout of sets]\label{dfn:pushout-sets}
Let $X_1,X_2,Z$ be sets with fixed functions $X_1 \stackrel{\ell_1}{\from} Z \stackrel{\ell_2}{\to}X_2$.
\begin{itemize}
  \item A set $C$ together with functions $X_1 \stackrel{\psi_1}{\to} C \stackrel{\psi_2}{\from}X_2$ is called a \textbf{cocone} over $\ell_1,\ell_2$, if the following diagram commutes:
\begin{center}
\begin{tikzcd}[ ] 
  Z 
  \arrow[]{r}{\ell_2}
  \arrow[swap]{d}{\ell_1}
  & X_2
  \arrow[]{d}{\psi_2}
  \\
  X_1
  \arrow[]{r}{\psi_1}
  & C
\end{tikzcd}
\end{center}
  \item a co-cone $X_1 \stackrel{\psi_1}{\to}P \stackrel{\psi_2}{\from}X_2$ is called a \textbf{pushout} (or universal co-cone) over $\ell_1,\ell_2$, if for any other co-cone $X_1 \stackrel{f}{\to}T \stackrel{g}{\from}X_2$, there exists a \emph{unique} map $\phi: P \to T$ such that the following diagram commutes:
  \begin{center}
    \begin{tikzcd}[]
      Z 
      \arrow[]{r}{\ell_2}
      \arrow[]{d}{\ell_1}
      & X_2
      \arrow[]{d}{\psi_2}
      \arrow[bend left]{ddr}{g}
      \\
      X_1
      \arrow[]{r}{\psi_1}
      \arrow[bend right,swap]{drr}{f}
      & P
      \arrow[dotted]{dr}{\exists!\phi}
      \\
      &&T
    \end{tikzcd}
  \end{center}
  here, commutativity of the diagram just means that $\phi \circ \psi_1 = f$ and $\phi \circ \psi_2 = g$.
\end{itemize}
\end{dfn}

\textbf{Examples:}
\begin{enumerate}
  \item If $X$ is a set and $A,B \subseteq X$ are subsets and $\ell_{A}: A \cap B \hookrightarrow A$, $\ell_B: A \cap B \hookrightarrow B$ are the inclusion maps, then the union $A \cup B$ is the pushout over $\ell_x,\ell_Y$, resulting in the pushout diagram
\begin{center}
\begin{tikzcd}[ ] 
  A \cap{} B
  \arrow[hook]{r}{\ell_B}
  \arrow[hook,swap]{d}{\ell_A}
  & B
  \arrow[hook]{d}{}
  \\
  A 
  \arrow[hook]{r}{}
  & 
  A \cup{} B
\end{tikzcd}
\end{center}
indeed, the diagram clearly commutes and is thus a co-cone. Moreover, if $A \stackrel{\phi_1}{\to}T \stackrel{\phi_2} \from B$ is another co-cone, then we can define
\begin{align*}
  \phi: A \cup B \to T,
  \quad
  \phi(x) = 
  \left\{\begin{array}{ll}
    f(x) & \text{if }x \in A\\
    g(x) & \text{if }x \in B
  \end{array} \right.
\end{align*}
which is well defined as $T$ is a co-cone over $\ell_A,\ell_B$, meaning $f$ and $g$ agree on $A \cap B$.
  \item If $Z$ is the empty set, then the pushout is the disjoint union of $X_1,X_2$.
  \item In the case where $X_1 = X_2$, the pushout of two parallel functions
    $\xymatrix@1{
      Z
      \ar@/^2ex/[r]^{f}
      \ar@/_2ex/[r]_{g}
      &
      X
    }$
    a co-cone is a set $C$ with two parallel functions $\psi_1,\psi_2: X \to C$ such that $\psi_1 \circ f = \psi_2 \circ g$.
    In particular, if $f(z) = g(z) = x$, then it must hold $\psi_1(x) = \psi_2(x)$.
\end{enumerate}


\subsection{Pushout of topological spaces}
The above definition can be adapted to the topological setting to recover some familiar notions.
\begin{dfn}[Pushout of topological spaces]
Let $X_1,X_2,Z$ be topological spaces with continuous functions $X_1 \stackrel{\ell_1}{\from} Z \stackrel{\ell_2}{\to}X_2$.
\begin{itemize}
  \item A topological space $C$ together with continuous functions $X_1 \stackrel{\psi_1}{\to} C \stackrel{\psi_2}{\from}X_2$ is called a \textbf{cocone} over $\ell_1,\ell_2$, if the following diagram commutes:
\begin{center}
\begin{tikzcd}[ ] 
  Z 
  \arrow[]{r}{\ell_2}
  \arrow[swap]{d}{\ell_1}
  & X_2
  \arrow[]{d}{\psi_2}
  \\
  X_1
  \arrow[]{r}{\psi_1}
  & C
\end{tikzcd}
\end{center}
  \item a co-cone $X_1 \stackrel{\psi_1}{\to}P \stackrel{\psi_2}{\from}X_2$ is called a \textbf{pushout} (or universal co-cone) over $\ell_1,\ell_2$, if for any other co-cone $X_1 \stackrel{f}{\to}T \stackrel{g}{\from}X_2$, there exists a \emph{unique} continuous map $\phi: P \to T$ such that the following diagram commutes:
  \begin{center}
    \begin{tikzcd}[]
      Z 
      \arrow[]{r}{\ell_2}
      \arrow[]{d}{\ell_1}
      & X_2
      \arrow[]{d}{\psi_2}
      \arrow[bend left]{ddr}{g}
      \\
      X_1
      \arrow[]{r}{\psi_1}
      \arrow[bend right,swap]{drr}{f}
      & P
      \arrow[dotted]{dr}{\exists!\phi}
      \\
      &&T
    \end{tikzcd}
  \end{center}
\end{itemize}
\end{dfn}
Note that compared to Definition~\ref{dfn:pushout-sets}, we just replaced sets with topological spaces and functions with continuous maps.

\textbf{Examples:}
\begin{enumerate}
  \item If $X$ is a topological space, and $A,B \subseteq X$ are subspaces, then their pushout is the topological space $A \cup B$ with the subspace topology of $X$.
    From the example for sets, it should be clear that the underlying set of the pushout should be $A \cup B$, but we have to rule out different topologies on $A \cup B$.
    Let $\tau \subseteq \mathcal{P}(A \cup B)$ denote the subspace topology on $A \cup B$, $\tau'$ be another topology.
    If $\tau \subseteq \tau'$, ($\tau'$ is \emph{coarser}), then the identity function $\id: (A \cup B,\tau') \to  (A \cup B, \tau')$ is continuous.
\end{enumerate}
