\subsection{The deck transformation group and universal coverings}

\begin{dfn}[]
Let $\pi:Y \to X$ be a covering.
A homeomorphism $\phi: Y \to  Y$ with $\pi \circ \phi = \pi$ is called a \textbf{deck transformation}.
We denote the set of deck transformations $\text{Deck}(\pi)$ the \textbf{deck transformation group}.
\end{dfn}
To see that it is indeed a group, first note that the set of endomorphisms on $Y$ forms a group. 
It is therefore sufficient to show that $\text{Deck}(\pi)$ is closed under composition and thus forms a subgroup. 

\begin{xmp}[]

We will show in exercise sheet 13 that the deck transformation group of the covering $\pi: \R \to \IS^{1}, x \mapsto  e^{2 \pi i x}$ is given by the set of integer-translations
\begin{align*}
  \text{Deck}(\pi) = \{\phi_k : \R \to  \R, r \mapsto  k + r \big\vert k \in \Z\} \iso \Z
\end{align*}
which turns out to be isomorphic of the fundamental group $\pi_1(\IS^{1})$.
\begin{align*}
  \Phi: \pi_1(\IS^{1}) \to  \text{Deck}(\pi), [\alpha_k] \mapsto \phi_k
\end{align*}
\end{xmp}

\begin{xmp}[]
The deck transformation group of the covering $\pi_n : \IS^{1} \to \IS^{1}, z \mapsto  z^{n}$ is
\begin{align*}
  \text{Deck}(\pi_n) = \{\phi_{\overline{k}}: z \mapsto  e^{2 \pi i k/n}z \big\vert \overline{k} \in \{\overline{0},\overline{1},\ldots,\overline{n-1}\}\}\iso \Z/n\Z \iso \faktor{\pi_1(\IS^{1})}{G(\pi)}
\end{align*}
\end{xmp}

The isomorphisms between the deck transformation group and the quotient of the fundamental group is no coincidence.


For a group $G$, and $H \subseteq G$, let $N_H$ denote its \textbf{normalizer}
\begin{align*}
  N_H := \{g \in G \big\vert g^{-1}Hg = H\}
\end{align*}
\begin{thm}[\hypertarget{thm:deck-transformation-group-theorem}{Deck transformation group theorem} (Satz über Deckbewegungen)]
  Let $X,Y$ be path connected and locally path connected, $\phi:(Y,y_0) \to (X,x_0)$ a covering and $G := G(\pi) = \pi_{\ast}(\pi_1(Y,y_0)) \subseteq \pi_1(X,x_0)$.
  Then for all elements in the normalizer $[\alpha] \in N_G < \pi_1(X,x_0)$ there exists a unique deck transformation $\phi_{[\alpha]} \in \text{Deck}(\pi)$ with $\phi_{[\alpha]}(y_0) = \tilde{\alpha}(1)$.

  \begin{center}
  \begin{tikzcd}[ ] 
    Y \arrow[]{r}{\pi_{[\alpha]}}
    & 
    Y \arrow[]{d}{\pi} 
    && 
    \pi_1(Y,y_0) \arrow[]{d}{\pi_{\ast}}
    \\
    \text{$[0,1]$}
    \arrow[]{r}{\alpha} 
    \arrow[]{ur}{\tilde{\alpha}}
    & 
    X 
    && 
    G \lhd N_G < \pi_1(X,x_0)
  \end{tikzcd}
  \end{center}

  Moreover, we have a group isomorphism
  \begin{align*}
    \Psi: \faktor{N_G}{G} \to  \mathrm{Deck}(\pi), 
    \quad
    [\alpha] \mapsto  \phi_{[\alpha]} 
  \end{align*}
  Often, the theorem is used in the special cases where
  \begin{itemize}
    \item If $G$ is already a normal divisor, then $N_G = \pi_1(X,x_0)$, so 
      \begin{align*}
        \Psi: \faktor{\pi_1(X,x_0)}{G} \to \mathrm{Deck}(\pi) 
      \end{align*}
    \item And if additionnally, $G(\pi) = \{1\}$, then $\pi_1(X,x_0) \iso \text{Deck}(\pi)$
  \end{itemize}
\end{thm}
We will only prove the first part
\begin{proof}
  Let $y_0,y_1 \in \pi^{-1}(y_0)$. Then by the uniqueness theorem \ref{cor:uniqueness-theorem}
  \begin{align*}
    !\exists \phi \in \text{Deck}(\pi) \text{ with } \phi(y_0) = y_1 \iff G(Y,y_0) := \pi_{\ast}(\pi_1(Y,y_0))= \pi_{\ast}(\pi_1(Y,y_1)) = G(y,y_1)
  \end{align*}

  Let $[\alpha] \in \pi_1(X,x_0)$ with $\tilde{\alpha}(1) = y_1$. Then
  \begin{align*}
    G(Y,y_1) 
    &= \pi_{\ast}\left(
      \left\{
        [\tilde{\alpha}^{-} (\tilde{\delta} \tilde{\alpha}] \big\vert \tilde{\delta} \text{ is a loop at } y_0
      \right\}
    \right)\\
    &= \{\left[\alpha^{-1}\left(
        (\pi \circ \tilde{\delta}) \alpha
    \right)\right]
    \big\vert \tilde{\delta} \text{ is a loop at }y_0
  \}
  \\
    &=
    \left\{[\alpha^{-}][\delta][\alpha] \big\vert \delta \in G(Y,y_0)\right\}\\
    &=
    [\alpha]^{-1} G(Y,y_0)[\alpha]
  \end{align*}
  and thus for any $[\alpha] \in \pi_1(X,x_0)$ such a Deckbewegung $\phi_{[\alpha]}$ exists if and only if
  \begin{align*}
    G(y,y_0) = G(Y,y_1)  = [\alpha]^{-1} G(Y,y_0)[\alpha] \iff [\alpha] \in N_{G(Y,y_0)}
  \end{align*}
\end{proof}

\begin{xmp}[]\label{xmp:pi-of-RPn}
For the covering $\pi: \IS^{n} \to  \R\IP^{n} = \IS^{n}/\{\pm\}$, the deck transformation group is
$\text{Deck}(\pi) = \{\id, - \id\}$
because for $v_0 \in \IS^{n}$ we have
\begin{align*}
  \pi^{-1}(\pi(v_0)) = \pi^{-1}([v_0]_{\sim}) = \{v_0,-v_0\}
\end{align*}
so $\phi \in \text{Deck}(\pi)$ means either $\phi(v_0) = v_0$ or $\phi = - \id$.

The theorem lets us easily calculate the fundamental group
\begin{align*}
  \pi_1(\R\IP^{n}) \iso \text{Deck}(\pi) \iso \Z/2\Z
\end{align*}
So, for a path $\tilde{\alpha}:[0,1] \to \IS^{n}$ from $v_0$ to $-v_0$, we set $\alpha = \pi \circ \tilde{\alpha}$.
Then
\begin{align*}
  \pi_1(\R\IP^{n},[v_0]) = \{1\, [\alpha]\}
\end{align*}
\end{xmp}


\begin{dfn}[]
Let $\pi: Y \to  X$ be a covering with $Y,X$ path connected and locally path connected.

We call $\pi$ a \textbf{universal covering}, if $Y$ is simply connected.

$\pi$ is called a \textbf{normal} covering, if $G(\pi) < \pi_1(X,x_0)$ is a normal divisor.
\end{dfn}

\begin{rem}[]
  $\pi$ is normal if and only if $\text{Deck}(\pi)$ acts transitiviely on $\pi^{-1}(x_0)$.

  Universal coverings --- if they exist --- are unique, up to isomorphisms in $\Top^{\ast}$.

  ``The'' universal covering $\tilde{\pi}: (\tilde{X},\tilde{x}_0) \to (X,x_0)$ has the following universal property:
  For any connected covering $\pi:(Y,y_0) \to  (X,x_0)$, there exists a unique continuous map
  $\phi: (\tilde{X},\tilde{x}_0) \to  (Y,y_0)$ such that the following diagram commutes:

  \begin{center}
    \begin{tikzcd}[column sep=0.8em] %\arrow[bend right,swap]{dr}{F}
      (\tilde{X},\tilde{x}_0) \arrow[]{rr}{\exists!\phi} \arrow[]{dr}{\tilde{\pi}}& & (Y,y_0)\arrow[swap]{dl}{\pi}\\
                              & (X,x_0)
    \end{tikzcd}
  \end{center}
  In other words, the universal covering is the initial object in the category of coverings of $(X,x_0)$.
\end{rem}



