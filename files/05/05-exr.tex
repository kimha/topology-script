\subsection{Exercises}
\begin{exr}[]
  We have shown that $\pi_1(\IS^{n}) \iso \{e\}$ for all $n \geq 2$.
  But we also know that $\pi_1(\IS^{1}) \iso \Z$. Explain why the proof of the former fails for $n = 1$. 
\end{exr}
\begin{sol}
  In the proof of $\pi_1(\IS^{n}) \iso \{e\}$ for $n \geq 2$, we used Seifert-van Kampen's Theorem by splitting the sphere into two parts
  \begin{align*}
    A = \IS^{n} \setminus \{\text{north pole}\}
    , \quad
    B = \IS^{n} \setminus \{\text{south pole}\}
  \end{align*}
  But the theorem requires that $A \cap B$ be path-conneted, but for $n = 1$, $A \cap B$ consists of two seperated lines.
\end{sol}

\begin{exr}[]
  Let $A$ be a set and assume $A$ is equipped with two binary operations $\ast, \circ: A \times A \to  A$ such that:
  \begin{enumerate}[{(}i{)}]
    \item $\ast$ and $\circ$ have a common two-sided unit $1 \in A$. That is:
      \begin{align*}
         a \circ 1 = 1 \circ a = a = a \ast 1 = 1 \ast a, \quad \text{for all}\quad a \in A
      \end{align*}
    \item $\ast$ and $\circ$ are mutually distributive, that is
      \begin{align*}
        (a \ast b) \circ (c \ast d) = (a \circ c) \ast (b \circ d)
        \quad \text{for all} \quad a,b,c,d \in A
      \end{align*}
  \end{enumerate}
  Do the following:
  \begin{enumerate}
    \item Show that $\ast$ and $\circ$ coincide, i.e. $a \ast b = a \circ b$ for all $a,b \in A$ and they are (both) commutative and associative
    \item For $G = (G,e,\cdot)$ a topological group, show that the group multiplication of $G$ induces a binary operation
      \begin{align*}
        \ast: \pi_1(G,e) \times \pi_1(G,e) &\to \pi_1(G,e),
        \\
        ([\alpha],[\beta]) &\mapsto [s \mapsto  \alpha(s) \cdot \beta(s)]
      \end{align*}
      with the equivalence class of the constant loop $\text{const.} e$ as two-sided identity.
    \item Conclude that the fundamental group of any topological group is commutative.
    \item Verify the fact that ``$\ast = \circ$'' in the case of $G = U(1) \subseteq \C$.
  \end{enumerate}
\end{exr}
\begin{sol}
  Let $a,b,c \in A$.
  \begin{enumerate}
    \item \textbf{$\bm{\circ \text{\ and } \ast}$ are the same:} 
      \begin{align*}
        c \ast b = (1 \circ c) \ast (b \circ 1) = (c \circ 1) \ast (1 \circ b) = (c \ast 1) \circ (1 \ast b) = c \circ b
      \end{align*}
    \textbf{Commutativity:} 
      \begin{align*}
        b \circ c = (1 \ast b) \circ (c \ast 1) = (1 \circ c) \ast (b \circ 1) = c \ast b = c \circ b
      \end{align*}
      \textbf{Associativity:} 
      \begin{align*}
        a \circ (b \circ c) = (a \ast 1) \circ (b \ast c) = (a \circ b) \ast (1 \circ c) = (a \circ b) \circ c
      \end{align*}
    \item \textbf{$\bm{\ast}$ is well defined:} 
      Let $\alpha,\beta: [0,1] \to G$ be loops with basepoint $e$. We need to show that if $\alpha',\beta'$ are loops with $\alpha' \simeq \alpha, \beta' \simeq \beta$, then $\alpha \ast \beta \simeq \alpha' \ast \beta'$.

      Let $H,J: [0,1]^2 \to G$ be homotopies \rep. between $\alpha,\alpha'$ and $\beta,\beta'$ respectively. Define
      \begin{align*}
        H \ast J: [0,1]^{2} \to G, \quad (s,t) \mapsto H(s,t) \cdot J(s,t)
      \end{align*}
      this is the composition of various continuous maps and thus continuous. Moreover
      \begin{align*}
       (H \ast J)(s,0) = H(s,0) \cdot J(s,0) = \alpha(s) \cdot \beta(s) = (\alpha \ast \beta)(s)\\
       (H \ast J)(s,1) = H(s,1) \cdot J(s,1) = \alpha'(s) \cdot \beta'(s) = (\alpha' \ast \beta')(s)\\
       (H \ast J)(0,t) = H(0,t) \cdot J(0,t) = \alpha(0) \cdot \beta(0) = e \cdot e = e
      \end{align*}
      which means that $H \ast J$ is a homotopy \rep{} between $\alpha \ast \beta$ and $\alpha' \ast \beta$.

      \textbf{$[\text{const.} e]$ is two-sided identity:} This follows from the fact that $e \in G$ is the identity of group multiplication.
    \item Writing $\circ$ for the group multiplication of $\pi_1(G,e)$ as defined in Lemma~\ref{lem:fundamental-group}, we need to check that for any loops $\alpha,\beta,\gamma,\delta: [0,1] \to  G$ it holds
      \begin{align*}
        ([\alpha] \ast [\beta]) \circ ([\gamma] \ast [\delta])
        =
        ([\alpha] \circ [\gamma]) \ast ([\beta] \circ [\delta])
      \end{align*}
      Once this is shown, the result follows immediately from (a).

      Let $L,R: [0,1] \to G$ be the loops corresponding to the left and right hand side of the previous equation:
      \begin{align*}
        L(s) = \left\{\begin{array}{ll}
          \gamma(2s) \cdot \delta(2s) & s \leq \tfrac{1}{2} \\
           \alpha(2s-1) \cdot \beta(2s-1) & s \geq \tfrac{1}{2}
        \end{array} \right.\\
        R(s) = (\alpha \circ \gamma)(s) \cdot (\beta \circ \delta)(s) = 
        \left\{\begin{array}{ll}
          \gamma(2s) \cdot \delta(2s) & s \leq \tfrac{1}{2} \\
           \alpha(2s-1) \cdot \beta(2s-1) & s \geq \tfrac{1}{2}
        \end{array} \right.
      \end{align*}
      so they are the same.
    \item Since $U(1) \iso S^1$, the fundamental group $\pi_1(U(1),1) \iso \Z$ has representants
      \begin{align*}
        \alpha_k: [0,1] \to U(1), \quad t \mapsto e^{2 \pi i kt}, \quad k \in \Z
      \end{align*}
      which are loops that go around the origin $k$-times (counter clockwise).
      The operation $\ast$, maps $[\alpha_k], [\alpha_\ell]$ to the class
      \begin{align*}
        [\alpha_k] \ast [\alpha_\ell] =  \left[(t \mapsto e^{2 \pi i k t} \cdot e^{2 \pi i \ell t} = e^{2 \pi i (k + \ell)t}\right] = [\alpha_{k + \ell}]
      \end{align*}
      which coincides with the usual multiplication as exhibited in Theorem~\ref{thm:fund-group-of-circle}.
  \end{enumerate}
\end{sol}
