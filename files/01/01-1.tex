\subsection{The topological space}\label{sec:the-top-space}
The central idea behind the minimal definition of a topological space is the notion of an \emph{open} set. 
In Analysis we know that a subset $U \subseteq \R$ is \emph{open}, if for every $x \in U: \exists \epsilon >0$ such that $(x - \epsilon, x + \epsilon) \subseteq U$.
\begin{enumerate}
  \item Looking at its properties we know that the union of open sets is open and that finite intersections of open sets is open. Moreover, the empty set $\emptyset$ and $\R$ itself are open.
  \item We called a set $A \subseteq \R$ \emph{closed}, if $\R \setminus A$ was open.
  \item We noted that the \emph{closure} of the open interval $(0,1)$ was the closed interval $[0,1]$.
  \item Sets can be neither open nor closed, or even both as the examples $[0,1)$ and $\R$ show.
\end{enumerate}
All of this information can be derived just from knowing what the open sets look like. They tell us how patches of space ``hang together''.

\begin{dfn}[]\label{dfn:topological-space}
  Let $X$ be a set\footnote{Some authors require $X$ to be non-empty, but Prof.\ Feller allowed non-empty spaces, so we shall use this convention.}. A \hypertarget{dfn:topology}{\textbf{topology}}\index{topology} on $X$ is a collection of subsets $\tau \subseteq \mathcal{P}(X)$ such that
  \begin{itemize}
    \item $\emptyset, X \in \tau$
    \item If $\{U_i\}_{i \in I}$ is a collection with $U_i \in \tau\ \forall  i$, then $\bigcup_{i \in I} U_i \in \tau$ 
    \item $U,V \in \tau \implies U \cap V \in \tau$
  \end{itemize}
  A subset $U \in \tau$ is called \textbf{open} and $(X,\tau)$ is called a \textbf{topological space}.
\end{dfn}
\begin{xmp}[]
  The following are topological spaces
  \begin{itemize}
    \item $X = \emptyset, \tau = \mathcal{P}(\emptyset) = \{\emptyset\}$.
    \item $X = \{a,b,c,d\}, \tau = \{\emptyset, X, \{a,b\}, \{b,c\}, \{b\}, \{a,b,c\}\}$
    \item $X = \R^{n}$ where $U \subseteq X$ is open if
      \begin{align*}
        \forall x \in U:\ \exists \epsilon > 0: B_{\epsilon}(x) \subseteq U
      \end{align*}
      We call this the \textbf{euclidean} topology on $\R^{n}$.
    \item $X = \Z$ and $U \subseteq X$ is open if $5 \in U$ or $U = \emptyset$.
    %\item $X = \R^2$, where $A \subseteq X$ is open if there exists a $U \subseteq A$ and a polynomial $f \in \R[X,Y]$ such that $U = \{(x,y) \big\vert f(x,y) \neq 0\}$.
    \item $X = \R$ and $U \subseteq X$ is open if $X \setminus U$ is finite.
      We call this the \textbf{cofinite} topology\index{topology!cofinite} on $\R$.
  \end{itemize}
  Check for yourself that these satisfy all the axioms in Definition~\ref{dfn:topological-space}
\end{xmp}

\begin{rem}[]\label{rem:alt-open}
  Let $X$ be a topological space and $A \subseteq X$.
  Then the following are equivalent.
  \begin{enumerate}
    \item $A$ is open.
    \item For all $a \in A$, there exists a $U \subseteq X$ open with $a \in U \subseteq A$.
  \end{enumerate}
  This means that open-ness is a local property: To check if $A$ is open, it suffices to check some condition is true for all points $a \in A$.
\end{rem}
\begin{proof}
  \textbf{(a) $\bm{\implies}$ (b):} Take $U = A$.

  \textbf{(b) $\bm{\implies}$ (a):} For each $a \in A$, let $U_a$ such that $a \in U_a \subseteq A$. Then $\bigcup_{a \in A} U_a = A$ is open as it is a union of open sets. 
  (Check the inclusions $\bigcup_{a\in A}U_a \subseteq A$ and $\bigcup_{a \in A}U_a \supseteq A$.)
\end{proof}

\begin{dfn}[]\label{dfn:finer-coarser}
Let $X$ be a set. 
\begin{itemize}
  \item The \textbf{discrete topology}\index{topology!discrete} on $X$ is the topology $\tau_{\text{disc.}} = \mathcal{P}(X)$, where every subset is open
  \item The \textbf{indiscrete topoogy}\index{topology!indiscrete} on $X$ is the topology $\tau_{\text{indisc.}} = \{\emptyset,X\}$.
\end{itemize}
Let $X$ be a set and $\tau_1,\tau_2$ be topologies on $X$.
We say that $\tau_1$ is \hypertarget{dfn:finer-topology}{\textbf{finer}} than $\tau_2$, if $\tau_2 \subseteq \tau_1$.
If that is the case, we also say that $\tau_2$ is \hypertarget{dfn:coarser-topology}{\textbf{coarser}} than $\tau_1$.
\end{dfn}
The discrete topology is the finest topology. The indiscrete is the coarsest topology.

\begin{dfn}[]
  Let $X$ be a topological space
  \begin{itemize}
    \item A subset $A \subseteq X$ is called \textbf{closed}, if $A^{c} = X \setminus A$ is open.
    \item A subset $U \subseteq X$ is called a \textbf{neighborhood}\index{neighborhood} (Umgebung) of $x \in X$, if there exists an open set $V \subseteq X$ such that $x \in V \subseteq U$
    \item For a point $x \in X$, we write $\mathcal{U}_x$ for the set of neighborhoods of $x$.
  \end{itemize}
  Let $x \in X, B \subseteq X$. We call $x$
  \begin{itemize}
    \item an \textbf{interior point}\index{interior point} of $B$ is $B$ is a neighborhood of $x$.
    \item an \textbf{exterior point}\index{exterior point} of $B$, if $B^{c}$ is a neighborhood of $x$.
    \item a \textbf{boundary point}\index{boundary point} of $B$ if neither $B$ nor $B^{c}$ are neighborhoods of $x$.
  \end{itemize}
  Analagously, define
  \begin{itemize}
    \item the \textbf{interior} $B^{\circ} := \{x \in X \big\vert x \text{ is an interior point of }B\}$
    \item the \textbf{closure} $\overline{B}:= \{x \in X \big\vert x \text{ is \textbf{not} an exterior point of }B\}$
    \item \textbf{boundary} $\del B:= \{x \in X \big\vert x \text{ is a boundary point of }B\}$
  \end{itemize}
\end{dfn}
From de Morgan's Law ($\left(\bigcup_{i \in I}A_i\right)^{c} = \bigcap_{i \in I}A_i^{c}$), it follows that finite unions of closed sets is closed and that arbitrary intersections of closed sets are closed.

\begin{xmp}[]
  Most of these definitions can be visually understood in the case of $X = \R^{2}$ with the euclidean topology.
  Let
  \begin{align*}
    A := [0,1) \times [0,1) = \{(x,y) \in \R^{2} \big\vert 0 \leq x,y < 1\}
    \quad \text{and} \quad 
    B := (0,1) \times \{0\}
  \end{align*}
  \begin{itemize}
    \item $(\tfrac{1}{2},\tfrac{1}{2})$ is an interior point of $A$.
      $(2,2)$ is an exterior point of $A$. $(1,1)$ is a boundary point of $A$.
      Its interior is $(0,1) \times (0,1)$, the closure is $[0,1] \times [0,1]$.
    \item $B$ has no interior points. Its closure is $[0,1] \times \{0\}$ and its boundary points are $\{(0,0), (1,0)\}$.
  \end{itemize}
\end{xmp}

So a topological space can be seen as a set with some extra data which, among other things, tell us whether a point lies in the interior, boundary or exterior of some subset.
We will soon see that the topology gives us much more useful information than that, but for now it suffices to know that such questions could not be answered if we just had a set. 
If $X$ is a topological space and $Y \subseteq X$ a subset, we can re-use all this data to turn $Y$ into a topological space in its own right.

\begin{dfn}[]
  Let $(X,\tau)$ be a topological space and $Y \subseteq X$. Then
  \begin{align*}
    \tau_Y := \{U \cap Y \big\vert U \in  \tau\}
  \end{align*}
  defines a topology on $Y$ and is called the \hypertarget{dfn:subspace-topology}{\textbf{subspace}}\index{topology!subspace} topology on $Y$.
\end{dfn}

For example, for $Y = [0,1] \subseteq \R$, the ``half open'' interval $[0, \frac{1}{2})$ is open in $Y$.

\begin{rem}[]
  Let $(X,\tau)$ be a topological space and $Y \subseteq X$.
  The closed sets $A \subseteq Y$ with respect to the subset topology are precisely those which are intersections of closed sets with $Y$:
  \begin{align*}
    \{\text{closed sets in $Y$}\}
    = \{B \cap Y \big\vert B^{c} \in \tau\}
  \end{align*}
\end{rem}
\begin{proof}
  \textbf{$\bm{\subseteq}$:} Let $A \subseteq Y$ be closed in $Y$. We want to find a closed subset $B \subseteq X$ such that $A= Y \cap B$.
  Since $Y \setminus A$ is open in $Y$, there exists  $U \subseteq X$ open such that $Y \cap U = Y \setminus A$.
Set $B = U^{c} =X \setminus U$. Then
\begin{align*}
  Y \cap B = Y \cap (X \setminus U) = (Y \cap X) \setminus U = Y \setminus U = Y \setminus (Y \cap U) = Y \setminus (Y \setminus A) = A
\end{align*}
  \textbf{$\bm{\supseteq}$: } Let $B \subseteq X$ be closed (in X). 
  Set $U := B^{c} = X \setminus B$, which is open in $X$. Then 
\begin{align*}
  Y \setminus A = Y \setminus (B \cap Y) = Y \setminus B = Y \cap U
\end{align*}
  is open in $Y$, so $A$ is closed in $Y$.
\end{proof}






\vfill
You should now \ldots
\begin{itemize}
  \item know the definition of a topological space, properties of open and closed sets and neighborhoods.
    Related exercises:~\ref{exr:subspace-topology}.
  \item be able to compute and check properties of the \emph{closure}, \emph{boundary}, \emph{interior} of some subsets of topological spaces (especially of $\R^{n}$).
    Related exercises:%~\ref{exr:alternative-interior-closure-boundary},\ref{exr:properties-interior-closure-boundary}.
  \item understand the subspace topology and how it changes the meaning of closure, boundary and interior.
    Related exercises:
  \item be able to contrast different topologies on the same set (such as $\R$).
\end{itemize}
