\subsection{Urysohn's Lemma}
It turns out that this can reduced to the following problem:

For a topological space $X$ and $A,B \subseteq X$ closed and disjoint, find a continuous function $f: X \to [0,1]$ such that
\begin{align*}
  f(a) = 1 \text{\ for all } a \in A
  \quad \text{and} \quad 
  f(b) = 0 \text{\ for all } b \in B
\end{align*}
This is in general not always possible, but Uryson's Lemma gives a condition when this is true.

Recall the separation axioms $T_1,T_2$. We add two more axioms $T_3,T_4$.
\begin{dfn}[]
  Let $X$ be a topological space.
  \begin{itemize}
    \item $X$ is $T_1$, if single points in $X$ are closed.
    \item $X$ is $T_2$, if for $x,y \in X$ with $x \neq y$, there exist disjoint open neighborhoods of $x$ and $y$.
    \item $X$ is called \textbf{regular} (satisfies the $\bm{T_3}$ \textbf{axiom}), if for all $x \in X$ and $A \subset X$ closed with $x \notin A$, there exist disjoint open neighborhoods of $x$ and $A$.
    \item $X$ s called \textbf{normal} (satisfies the \textbf{$\bm{T_4}$ axiom}), if for all $A,B \subseteq X$ closed and disjoint, there exist open disjoint neighborhoods of $A$ and $B$.
    \item $X$ is called a \textbf{$\bm{T_3}$-space}, if it is regular and Hausdorff.
    \item $X$ is called a \textbf{$\bm{T_4}$-space}, if it is normal and Hausdorff.
  \end{itemize}
\end{dfn}
The confusing nomenclature between $T_4$ spaces and the $T_4$ axiom comes from the fact that early topologists only considered Hausdorff topological spaces and kept the defintion.

\begin{rem}
  A topologist sits in a bar. 
  In comes a closed set and a point. 
  The bartender asks ``What can I get you?''.
  The closed set replies ``I'll have an open set containing me?''.
  The point interjects ``I'll also have an open set containig me, but make it disjoint from \emph{his}'.''
  The bartender thinks for a moment and says ``I'm not sure if I can do that''.
  The topologist responds ``Don't worry they're regulars.''
\end{rem}

\begin{xmp}[]
  Metric spaces are normal.
  Let $A,B \subseteq X$ closed and disjoint.
  Since $B$ is closed, $\forall  a \in A \subseteq B^{c}: \exists \epsilon_a$ such that $B_{\epsilon_a}(a) \cap B = \emptyset$.
  Then we take the union of all such balls, but with half their radii. And do the same for $A$, so set
  \begin{align*}
    U = \bigcup_{a \in A}B_{\frac{\epsilon_a}{2}}(a)
    \quad \text{and} \quad 
    V = \bigcup_{b \in B}B_{\frac{\epsilon_b}{2}}(b)
  \end{align*}
  and clearly, $U \cap V = \emptyset$.
\end{xmp}

The four separation axioms generally do not imply eachother, but some combinations do.
\begin{rem}[]
  \begin{enumerate}
    \item Normal and $T_1$ $\implies$ Regular and $T_1$.
    \item Regular and $T_1$ $\implies$ $T_2$.
    \item $T_2 \implies T_1$.
  \end{enumerate}
\end{rem}



\begin{thm}[Urysohn's lemma]
Let $X$ be a normal space. If $A,B \subseteq X$ are closed disjoint subsets, then there exists a continuous function
$f: X \to [0,1]$ such that $f|_A = 1, f|_B = 0$.
\end{thm}

We will prove it using the following lemma
\begin{lem}[Refinement lemma]
Let $X$ be normal and $M \subseteq N \subseteq X$ with $\overline{M} \subseteq \stackrel{o}{N}$.
Then there exists a subset $L \subseteq X$ with
\begin{align*}
  \overline{M} \subseteq \stackrel{o}{L} \subseteq \overline{L} \subseteq \stackrel{o}{N}
\end{align*}
In this case, we write $M < N \implies \exists L: M < L < N$
\end{lem}
\begin{proof}
  Note that the sets $\overline{M}$ and $X \setminus \stackrel{o}{N}$ are closed and disjoint. Since $X$ is normal there exist disjoint open subsets $U,V$ with $\overline{M} \subseteq U$ and $X \setminus \stackrel{o}{N} \subseteq V$.

  If we chose $L = U$, then $U \subseteq L \subseteq X \setminus V$ and since $X \setminus V$ is closed, we also have $U \subseteq X \setminus V$.
  \begin{align*}
    \overline{M} \subseteq L = \stackrel{o}{L} \subseteq \overline{L} \subseteq X \setminus V
  \end{align*}
\end{proof}

\begin{proof}[Proof theorem]
  We will construct a sequence of functions $f_n: X \to [0,1]$ that converges to a continuou function with $f(A) = \{1\}$ and $f(B) = \{0\}$.

  In the first step, we have $A< (X \setminus B)$ so by our lemma we get a set $L_{\frac{1}{2}}$ with $A < L_{\frac{1}{2}} < B$ and we can define a step function
  \begin{align*}
    f_1: X \to [0,1] \quad f_1|_A = 1, \quad f_1|_{L_{\frac{1}{2}} \setminus A } = \frac{1}{2}, \quad f_1|_{X \setminus L_{\frac{1}{2}}} = 0
  \end{align*}
  and inductively we keep refining the the sets to get differing levels.
  i.e. in the second step we create levels with values $\frac{k}{4}$
  \begin{align*}
    A = L_1 < L_{\frac{3}{4}} < L_{\frac{1}{2}} < L_{\frac{1}{4}} < L_0 = B  
  \end{align*}
  and in the general step 
  \begin{align*}
    A = L_1 < L_{\frac{2^{n}-1}{2^{n}}} < L_{\frac{2^{n-1} -1 }{2^{n-1}}} < \ldots < L_{\frac{1}{2^{n-1}}} < L_{\frac{1}{2^{n}}}<L_0 = B
  \end{align*}
  and define the step function as
  \begin{align*}
    f_n|_{L_{\frac{k}{2^{n}}}/L_{\frac{k+1}{2^{n}}}} = \frac{k}{2^{n}}, \quad k \in \{0,1,\ldots,2^{n-1}\}
  \end{align*}
  and by construction we still have $f|_A = 1, f|_B = 0$.

  Finally, we take the limit $f := \lim_{n \to \infty}f_n$ and show continuity:
  Let $\epsilon > 0, x \in X$.

  If $x \in A$ chose $n$ such that $\frac{1}{2^{n}} < \epsilon$ and set $U = L_{\frac{2^{n}-1}{2^{n}}} \supseteq A$ and clearly for all $y \in U$
  \begin{align*}
    \abs{f(x) - f(y)} = 1 - f(y) \leq 1 - f_n(y) \leq 1 - \frac{2^{n} - 1}{2^{n}} < \epsilon
  \end{align*}
  For $x \in B$ the argument is exactly the same, but for $x \notin A \cup B$ we take an $n in \N$ such that $\frac{1}{2^{n-1}} < \epsilon$.
  Then
  \begin{align*}
    X \setminus (A \cup B) = \bigcup_{k=1}^{2^{n}-1}
    \stackrel{o}{L_{\frac{k-1}{2^{n}}}} \setminus \overline{L_{\frac{k+1}{2^{n}}}}
  \end{align*}
  then there exists an index $k$ such that $x$ is in one of those levels
  \begin{align*}
    x \in \stackrel{o}{L_{\frac{k-1}{2^{n}}}} \setminus \overline{L_{\frac{k+1}{2^{n}}}} := U
  \end{align*}
  and so for all $y \in U$ we have $f(y) \in [\frac{k-1}{2^{n}}, \frac{k+1}{2^{n}}]$ by construction, which shows continuity.
\end{proof}


\vfill

Although the details of the proof of Urysohn's Lemma ren't that important, you should know the statement.
Knowing examples and non-examples of each of the separation axioms (and their combinations) is also advised. These non-examples are a good source for counterexamples for other (wrong) statements.


