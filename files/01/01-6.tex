\subsection{Connectedness}\label{sec:connectedness}

We intuitively know what it means for an object to be connected. 
But putting this in the language of topology is quite tricky, as there will be examples, where the topological definition will disagree with our intuitive understanding.
\begin{dfn}[]
A topological space is \textbf{connected}, if for all pairs of subsets $U, V \subseteq X$, one of the following is false:
\begin{itemize}
  \item $U \cup V = X$
  \item $U \cap V = \emptyset$
  \item $U,V$ are both open.
  \item $U,V$ are both non-empty.
\end{itemize}
in other words, $X$ is connected if it cannot be split into two non-empty disjoint clopen subets.
\end{dfn}
\begin{lem}[]
The connected subsets in $\R$ are exactly the intervals.
\end{lem}
\begin{proof}[Proof Sketch]
  If $A \subseteq R$ is connected, then for all $x,y \in A$ we have $x \leq z \leq y \implies z \in A$, or else 
  \begin{align*}
    I = (I \cap (-\infty,z)) \sqcup (I \cap (z,\infty))
  \end{align*}
  If $I \subseteq \R$ is an interval let $U,V \subseteq I$ open and non-empty such that $I = U \cup V$. 
  Chose $a \in U, b \in V$. Then WLOG $a < b$, so set
  \begin{align*}
    s := \sup \{x \in U \big\vert x < b\} \in I
  \end{align*}
  Then $s \in U$ or $s \in V$. In either case this shows that $U \cap V \neq \emptyset$
\end{proof}


The notion of connected sets gives us the generalisation of the intermediate value theorem.
\begin{thm}[]\label{thm:image-of-connected}
  The image of connected sets under continuous functions is connected.
\end{thm}
\begin{proof}[Proof Sketch]
  Let $f: X \to Y$ continuous and assume $X$ is connected.
  As we are only concered with the image of $f$, we may assume that $f$ is surjective.
  Let $U,V \subseteq Y$.
  \begin{itemize}
    \item If $U \cup V = Y$, then $f^{-1}(U) \cup f^{-1}(V) = X$.
    \item If $U \cap V = \emptyset$, then $f^{-1}(U) \cap f^{-1}(V) = \emptyset$.
    \item If they are open, then $f^{-1}(U), f^{-1}(V)$ are open.
    \item If they are non-empty, then $f^{-1}(U)$ and $f^{-1}(V)$ are non-empty.
  \end{itemize}
  So if $U,V$ were proof that $Y$ is disconnected, then $f^{-1}(U)$ and $f^{-1}(V)$ would prove disconnectedness of $X$.
\end{proof}


Usually, the definition of connectedness matches with our intuition, but there are some examples where that is not the case. 
A \emph{stronger} type of connectedness is that of path-connectedness

\begin{dfn}[]
  A \textbf{path} in a topological space $X$ is a continuous function $\gamma: [0,1] \to X$. 
  For $a = \gamma(0)$ and $b = \gamma(1)$, we say that $\gamma$ is an path \emph{from $a$ to $b$} or that it \emph{connects $a$ and $b$}.

  A topological space $X$ is said to be \textbf{path connected}, if for any two points $a,b \in X$ there exists a path from $a$ to $b$.
\end{dfn}

\begin{rem}\label{rem:path-connected}
  The following are pretty easy to prove
  \begin{enumerate}
    \item $X$ path connected $\implies$ $X$ connected. 
    \item The image of path connected spaces under continuous maps is path connected.
  \end{enumerate}
\end{rem}
\begin{proof}
\begin{enumerate}
  \item Let $U,V \subseteq X$ non-empty and open such that $X = U \cup V$.
    We show that their intersection is non-empty. 
    Chose $a \in U$ and $b \in V$. Since $X$ is path connected, there exists a continuous map $\gamma: [0,1] \to  X$ such that $\gamma(0) = a$ and $\gamma(1) = b$.
    But since $[0,1]$ is connected, its image $\gamma([0,1]) =: A$ must also be connected (by Theorem~\ref{thm:image-of-connected})
    Consider the decomposition
    \begin{align*}
      A = \underbrace{(A \cap U)}_{=: U'} \cup \underbrace{(A \cap V)}_{=: V'}
    \end{align*}
    Note that $U',V'$ are non-empty ($a \in U', b\in V'$), open (in the subspacetopology of $A$) and cover $A$. As $A$ is connected we must have $(U \cap V) \supseteq U' \cap V' \neq \emptyset$.
  \item The composition of continuous maps is continuous.
    So if $\gamma:[0,1] \to X$ is a path connecting $a$ and $b$, then $f \circ \gamma: [0,1] \to f(X)$ is path connecting $f(a)$ to $f(b)$.
\end{enumerate}
\end{proof}

\begin{rem}
  \phantom{a}
  \begin{itemize}
    \item The integers with the co-finite topology is connected but not path connected.
    \item For any finite topological space $X$ it is true that $X$ connected if and only if $X$ is path connected.
    \item For $X,Y$ non-empty topological spaces, then
      \begin{align*}
        X, Y \text{ (path) connected } &\iff X \times Y \text{ (path) connected}
      \end{align*}
    \item For subsets $A,B \subseteq X$ with $A \cap B \neq \emptyset$ we have
\begin{align*}
  A,B \text{ (path) connected } \implies A \cup B \text{ (path) connected}  
\end{align*}
    \item $X$ is \emph{not} path connected if and only if there exists a continuous map
      \begin{align*}
        f: X \to (\{0,1\}, \tau_{\text{disc.}})
      \end{align*}
      As a quick consequence, we get that $O_n(\R)$ is not connected. We can also show that its conneceted components are those with determinant $\pm 1$ each.
    \item $\Q \subseteq \R$ is not connected, because we can partition it into two non-empty disjoint open sets
      \begin{align*}
        \Q = (\Q \cap (-\infty, \sqrt{2})) \sqcup (\Q \cap (\sqrt{2},\infty))
      \end{align*}
  \end{itemize}
\end{rem}

\vfill 
You should now \ldots
\begin{itemize}
  \item know definition and examples of (path)-connected and not (path)-connected topological spaces.
  \item know how (path)-connected spaces behave under continuous functions, products and unions.
\end{itemize}
