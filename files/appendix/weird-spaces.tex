\section{Weird Spaces}
A weird space is a topological space that behaves verymuch unlike ``real-world'' spaces such as $\R^{n}$, where our physical intuition may lead us to come to false conclusions.

It is a good idea to have these weird spaces in mind because they are a great source for counterexamples.


\subsection*{The Topologist's sine curve}

The Topologist's sine curve $\textsf{Tsc}$ is the closure of the graph of the function $\sin(\tfrac{1}{x})$, over some domain, usually $(0,1]$.
\begin{align*} 
  \textsf{Tsc} 
  &= \overline{
    \left\{
      (x,y) \in \R^{2} \big\vert
      x \in (0,1], y = \sin\left(
        \frac{1}{x}
      \right)
    \right\}
  } 
  \\
  &=
    \left\{
      (x,y) \in \R^{2} \big\vert
      x \in (0,1], y = \sin\left(
        \frac{1}{x}
      \right)
    \right\}
    \bigsqcup
    \{0\} \times [-1,1]  \subseteq \R^{2}
\end{align*}
\begin{itemize}
  \item $\textsf{TSC}$ is connected, but not path connected.

    One can show that there is no path that connects the endpoint $(1,\sin(1))$ and $(1,0)$ because if $\gamma: [0,1] \to \textsf{Tsc}$ were such a path, we could find a sequence of times $t_0 < t_1 < \ldots$ with $\lim_{n \to \infty} t_n = 1$ such that $\gamma(t_n) = (x_n,-1)$ for some $x_n \in [0,1]$ and $\lim_{n \to \infty} x_n = 0$.

    For $\gamma$ to be continuous, it would mean that
    \begin{align*}
      \lim_{n \to \infty} \gamma(t_n) = \gamma( \lim_{n \to \infty} t_n) = (0,-1) \neq (0,1)
    \end{align*}
\end{itemize}


\subsection*{$\R/\Z$}

The space $\R/\Z$ is the quotient space obtained by identifying all the integers to a single point.

Not to be confused with $\R/\sim \iso \IS^{1}$, where $x \sim y \iff x-y \in \Z$, which is sometimes also denoted as $\R/\Z$!

It sort of looks like a flower petal with infinite petals.

This space can be used to show that the quotient space does not necessarily retain
\begin{enumerate}
  \item $\R/\Z$ is not locally compact, unlike $\R$.
  \item $\R/\Z$ is not first countable (and thus neither second countable), unlike $\R$
\end{enumerate}


\subsection*{Fine Brush}

For the set $K = \{\tfrac{1}{n} \big\vert n \in \N^+\}$, the fine brush is the set
\begin{align*}
  \textsf{Fine brush} := \text{Cone}\left(
    \{0\} \cup K
  \right) \subseteq \R^{2}
\end{align*}

This is an example where $A = [(0,0)]$, is a deformation retract but not a strong deformation retract.


\subsection*{Sorgenfrey Line $\R_l$}
The Sorgenfrey Line $\R_l$ has the set $\R$, but with the half-open topology, of which the collection of half-open intervals $[a,b), a,b, \in \R$ is a basis.

\begin{itemize}
  \item 
    Because an open interval $(a,b)$ can be written as
    \begin{align*}
      (a,b) = \bigcup_{n \in \N} [a + \frac{1}{n},b)
    \end{align*}
    this topology is finer than the standard topology.

  \item 
    In this topology, the sets $[a,b)$ is clopen, and so are the sets $(-\infty,a)$ and $[a,\infty)$, which shows that the space is disconnected (and even totally disconnected).

  \item $\R_l$ is an example of a first-countable and sepearable, but not second-countable space and therefore not metrizable.
  \item Any compact subset of $\R_l$ is at most countable, so this space is not locally compact.
\end{itemize}

\subsection*{Sorgenfrey Plane $\R_l^{2}$}

The Sorgenfrey Plane is the cartesian product of two Sorgenfrey Lines $\R_l^{2} = \R_l \times \R_l$ with the product topology.


A basis of this is the set of ``half-open'' rectangles. That is the interior of a rectangle plus the southern and western border and the south-western corner.

\begin{itemize}
  \item The anti-diagonal $\Delta := \{(x,-x) \big\vert x \in \R\}$ is an uncountable discrete set and non-separable.
  \item For $K = \Delta \cap \Q^{2}$ the rational points on the diagonal, both sets $K$ and $\Delta \setminus K$ are closed and one can show that they cannot be separated by open sets.
    Therefore, $\R_l^{2}$ shows that a product of normal spaces is not necessarily normal.
\end{itemize}




\subsection*{Line with two origins}
Let $Y = \R \sqcup \R = \{(x,i) \big\vert x \in \R, i =1,2\}$ and glue the two lines together everywhere except for the origin.
So
\begin{align*}
  X = Y/\sim \quad \text{where} \quad (x,1) \sim (x,2) \text{ if } x \neq 0
\end{align*}
A good way to visualize this space is to draw $Y$ (i.e.\ two parallel lines), mark the origins and do the equivalence classes in your head. 

Label the ``upper'' origin $[(0,1)]$ with $0_1$ and the ``lower'' origin with $0_2$.

\begin{itemize}
  \item This space is not Hausdorff, because any neighborhood of $0_1$ and any neighborhood of $0_2$ must intersect.
  \item This space is however $T_1$. Clearly, any point not equal to $0_1,0_2$ is closed, so it suffices to check that ${\{0_1\}}^{c}$ is open.
    For $x \in {\{0_1\}}^{c} \neq 0_2$ this is als clear as we can take the class of $(x-\epsilon,x+\epsilon)$.
    For $x = 0_2$, we can take the the class of $(-\epsilon,\epsilon) \setminus 0$ with the point $0_2$, which is again open.
  \item It is an example of a $T_1$ space, where a sequence may have more than one limit.
    Take the sequence given by the classes of ${(\tfrac{1}{n})}_{n \geq 1}$.
    Then both $0_1,0_2$ are limits of the sequence.
\end{itemize}



\subsection*{$\R$-cofinite}
A set $U \subseteq \R$ is said to be open, if its complement is finite.
So the closed sets are exactly the finite ones.

\subsection*{$\R$-cocountable}
A set $U \subseteq \R$ is said to be open, if its complement is countable.

The construction works for any set $X$ with $X$ infinite (or uncountable).
This gives an example of a $T_1$ space that is not $T_2$.

\subsection*{\hypertarget{exp:long-line}{Long Line}}

%\subsection*{Hawaiian Earring}

