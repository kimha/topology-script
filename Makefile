infile = main
outfile = topo-script

.PHONY: all allstyles classic fancy nosol clean

all:
	$(MAKE) nosol
	$(MAKE) allstyles
	mv $(outfile)-*.pdf ./output

nosol:
	sed -i "s/showsolutions{1}/showsolutions{0}/" $(infile).tex
	$(MAKE) allstyles
	mv $(outfile)-classic-solutions.pdf $(outfile)-classic.pdf
	mv $(outfile)-fancy-solutions.pdf $(outfile)-fancy.pdf
	sed -i "s/showsolutions{0}/showsolutions{1}/" $(infile).tex

allstyles: 
	$(MAKE) classic
	$(MAKE) fancy 

fancy: 
	sed -i "s/environmentstyle{0}/environmentstyle{1}/" $(infile).tex
	latexmk -pdf -silent $(infile).tex
	cp $(infile).pdf $(outfile)-fancy-solutions.pdf

classic: 
	sed -i "s/environmentstyle{1}/environmentstyle{0}/" $(infile).tex
	latexmk -pdf -silent $(infile).tex
	cp $(infile).pdf $(outfile)-classic-solutions.pdf

clean:
	latexmk -C -silent $(infile).tex
