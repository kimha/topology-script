#+TITLE: Topology PVK Script - README
#+AUTHOR: Han-Miru Kim
#+STARTUP: OVERVIEW

* About
Script for the Topology PVK. Based on Prof. Feller's Lecture.
You can download pre-compiled versions from [[https://n.ethz.ch/student/kimha]].


* TODO
- Exercises for all chapters
- Solutions to all Exercises
- More mixed exercises, (maybe from old exams)
- Excursion Free Groups and Free Produkt (because not done in Algebra I FS22)
- (optional) addendum for category theory on products, coproducts, limits, colimits
- (optional) outlook for Algebraic Topology I

* Link to Repository
[[https://gitlab.ethz.ch/kimha/topology-script]]

If you read this from the PVK-Materialien drive, you'll notice that the handwritten notes from Day 1 are missing. This is because the first day was written on the blackboard and only Days 2-4 were written on a tablet.

* Compilation
To compile the file, either edit the main.tex file directly, or run
#+BEGIN_SRC shell
make all
#+END_SRC
and pick the pdf from the ~output~ directory.
** Dependencies:
- GNU make
- latexmk
- TeX installation
  
