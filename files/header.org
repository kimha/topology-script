#+TITLE: Topology PVK Script - Header file
#+AUTHOR: Han-Miru Kim
#+STARTUP: OVERVIEW


* About
This is an emacs org mode file that lets me easily write, structure and document write my header file.

Since I plan on making multiple verions of the header file, I first define all the codeblocks using the noweb syntax, then generate the configuration files by chosing all blocks and tangling them with the output files.

* Packages
** Font settings
lmodern is the standard LaTeX font Latin Modern
#+BEGIN_SRC latex :noweb-ref font-settings
  \usepackage{lmodern}
  \usepackage{fontenc}[t1]
  \usepackage{microtype}
#+END_SRC

** Content
The ~\newcolumntype~ commands adds a math-mode column for left/center/right alignment.
#+BEGIN_SRC latex :noweb-ref content
  \usepackage{caption}
  \usepackage{enumerate}
  \usepackage{hyperref}
  \usepackage{wrapfig}
  \usepackage{xifthen}
  \usepackage{imakeidx} % index
  \makeindex

  \usepackage{array}   
  \newcolumntype{L}{>{$}l<{$}} 
  \newcolumntype{C}{>{$}c<{$}} 
  \newcolumntype{R}{>{$}r<{$}} 

  \usepackage[framemethod=TikZ]{mdframed}
  \usepackage[customcolors,norndcorners]{hf-tikz}

  \usepackage{subcaption}
  \DeclareCaptionFormat{custom}
  {%
    %\textbf{#1#2}
    {\small #3}
  }
  \captionsetup{format=custom}
#+END_SRC

** Page Settings
I make the page a bit wider than standard.
#+BEGIN_SRC latex :noweb-ref page-settings
  \usepackage{geometry}
  \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhf{}
    \hoffset = -0.45 in
    \voffset = -0.3 in
    \textwidth = 500pt
    \textheight = 650pt
    \setlength{\headheight}{25.6pt}
    \setlength{\headwidth}{500pt}
    \setlength{\parindent}{0pt}
    \marginparwidth = 0pt
    \fancyhead[L]{\leftmark}
    \fancyhead[R]{\rightmark}
    \fancyfoot[C]{\thepage}
#+END_SRC



** Math
#+BEGIN_SRC latex :noweb-ref math
  \usepackage{amsmath}
  \usepackage{amssymb} 
  \usepackage{amsthm} 
  \usepackage{dsfont}
  \usepackage{amsfonts}
  \usepackage{mathdots}
  \usepackage{mathrsfs}
  \usepackage{bm} % bold math
  \usepackage{mathtools}
  \usepackage{faktor} % Quotients
  \usepackage{xcolor}
  \usepackage{stmaryrd}

  \let\tmpphi\phi 
  \let\phi\varphi
  \let\varphi\tmpphi 
#+END_SRC
the last lines swap the behaviour of ~\phi~ and ~\varphi~.

** Graphics

#+BEGIN_SRC latex :noweb-ref graphics
  \usepackage{graphicx}
  \usepackage[all]{xy} % xypic
    \SelectTips{cm}{}
    \SilentMatrices

  \usepackage{tikz}
      \usetikzlibrary{cd}
#+END_SRC
The package xypic lets us draw commutative diagrams with higher morphisms.

** Formatting
#+BEGIN_SRC latex :noweb-ref formatting
  \usepackage{sectsty}
      \sectionfont{\LARGE}
      \subsectionfont{\Large}
      \subsubsectionfont{\large}
      \paragraphfont{\large}

  \usepackage[export]{adjustbox}  %\adjustbox{scale=2,center}{}, export allows vertical alignment in includegraphics
  \usepackage{paracol}
  \usepackage{float}
#+END_SRC


* Macros
** Colors
#+BEGIN_SRC latex :noweb-ref colors
  \usepackage{xcolor}
  \definecolor{myblue}{HTML}{81a1c1}
  \definecolor{mygreen}{HTML}{a3be8c}
#+END_SRC


** Symbols
#+BEGIN_SRC latex :noweb-ref symbols
  \newcommand{\N}{\mathbb{N}}
  \newcommand{\Z}{\mathbb{Z}}
  \newcommand{\Q}{\mathbb{Q}}
  \newcommand{\R}{\mathbb{R}}
  \newcommand{\IS}{\mathbb{S}}
  \newcommand{\ID}{\mathbb{D}}
  \newcommand{\IT}{\mathbb{T}}
  \newcommand{\C}{\mathbb{C}}
  \newcommand{\K}{\mathbb{K}}
  \newcommand{\F}{\mathbb{F}}
  \newcommand{\IP}{\mathbb{P}}
  \newcommand{\RP}{\mathbb{RP}}


  \DeclareMathOperator{\charac}{char}
  \DeclareMathOperator{\degree}{deg}  

  \newcommand{\del}{\partial}
  \newcommand{\es}{\text{\o}}
  \newcommand{\rep}{rel.\ endpoints}
  \newcommand{\from}{\leftarrow}
#+END_SRC

** Delimiters
#+BEGIN_SRC latex :noweb-ref delimiters
  \DeclarePairedDelimiter\abs{\vert}{\vert}
  \DeclarePairedDelimiter\ceil{\lceil}{\rceil}
  \DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
  \DeclarePairedDelimiter\Norm{\vert\vert}{\vert\vert}
  \DeclarePairedDelimiter\scal{\langle}{\rangle}
  \DeclarePairedDelimiter\dbrack{\llbracket}{\rrbracket}
#+END_SRC


** Math Operators
Things to write in normal font in math mode

#+BEGIN_SRC latex :noweb-ref math-operators
  % LinAlg
  \DeclareMathOperator{\Hom}{Hom}
  \DeclareMathOperator{\Ker}{Ker}
  \DeclareMathOperator{\Image}{Im}
  \DeclareMathOperator{\image}{im}
  \DeclareMathOperator{\id}{id}
  \DeclareMathOperator{\End}{End}
  \DeclareMathOperator{\ev}{ev}
  \DeclareMathOperator{\trace}{tr}
  \DeclareMathOperator{\Mat}{Mat}
  \DeclareMathOperator{\GL}{GL}
  \DeclareMathOperator{\SO}{SO}
  \DeclareMathOperator{\SL}{SL}

  % Algebra
  \DeclareMathOperator{\lcd}{lcd}
  \DeclareMathOperator{\ggT}{ggT}
  \DeclareMathOperator{\Aut}{Aut}
  \DeclareMathOperator{\sgn}{sgn}

  % Topology
  \DeclareMathOperator{\const}{const}
  \DeclareMathOperator{\Deck}{Deck}

  % Category
  \newcommand*{\Top}{\text{\sffamily Top}}
  \newcommand*{\Htpy}{\text{\sffamily Htpy}}
  \newcommand*{\Grp}{\text{\sffamily Grp}}
  \newcommand*{\Set}{\text{\sffamily Set}}
  \newcommand*{\Ring}{\text{\sffamily Ring}}

  \newcommand{\iso}{\cong}
  \newcommand{\mono}{\rightarrowtail}
  \newcommand{\epi}{\twoheadrightarrow}
#+END_SRC


* Environments
** Default Environments
Change enumerate labels to alphabet instead of numbers
#+BEGIN_SRC latex :noweb-ref default-environments
  \renewcommand{\labelenumi}{(\alph{enumi})}
#+END_SRC

** Custom Environments
#+BEGIN_SRC latex :noweb-ref custom-environments
  %\newcommand{\hidesol}[1]{
  %\ifnum\showsolutions=1
  %#1 \vspace{\baselineskip}
  %\fi
  %\ifnum\showsolutions=0
  %\vspace{2\baselineskip} \hspace{2cm}
  %\fi
  %}
  %\newcommand{\hideproof}[1]{
  %\ifnum\showproofs=1
  %#1 \vspace{\baselineskip}
  %\fi
  %\ifnum\showproofs=0
  %\vspace{2\baselineskip} \hspace{2cm}
  %\fi
  %}
  \usepackage{comment}
  \ifnum\showsolutions=1
    \newenvironment{sol}{\textbf{Solution:}~}{}
  \else
    \excludecomment{sol}
  \fi
  \newcommand{\prerequisite}[1]{Prerequisites: #1}
  \newcommand{\prerequisites}[1]{Prerequisites: #1}
#+END_SRC

** Classic Theorems
#+BEGIN_SRC latex :noweb-ref classic-theorems
  \newtheorem{thm}{Theorem}[subsection]
  % cursive text
  \newtheorem{lem}[thm]{Lemma}
  \newtheorem{prop}{Proposition}[thm]
  \newtheorem{cor}[thm]{Corollary}

  % Non-cursive text
  \theoremstyle{definition} 
  \newtheorem{dfn}[thm]{Definition}
  \newtheorem{rem}[thm]{Remark}
  \newtheorem{note}[thm]{Note}
  \newtheorem{ex}[thm]{Example}
  \newtheorem{xmp}[thm]{Example}
  \newtheorem{exr}{Exercise}[section]
#+END_SRC

** Fancy Boxes
*** Setup
Taken from this [texblog post](https://texblog.org/2015/09/30/fancy-boxes-for-theorem-lemma-and-proof-with-mdframed/)

Setup numbering system and define a function that creates environment.
~\newenvironment~ takes one optional and three necessary arguments.
- The environment name (that which is put inside ~\begin{###}~)
- The Title name which is printed on the page
- The colour of the box. I use colors in the format ~blue!50!green!50~ etc.
- The counter, split into name and command

#+BEGIN_SRC latex :noweb-ref fancy-setup
  \newcounter{theo}[subsection]
  \newcounter{exer}[section]
  \renewcommand{\thetheo}{\arabic{section}.\arabic{subsection}.\arabic{theo}}
  \renewcommand{\theexer}{\arabic{section}.\arabic{exer}}

  \newcommand{\defboxenv}[5]{
    \newenvironment{#1}[1][]{
      \refstepcounter{#4}
      \ifstrempty{##1} { % no Title
        \mdfsetup{
          frametitle={
            \tikz[baseline=(current bounding box.east), outer sep=0pt]
            \node[anchor=east,rectangle,fill=#3]
            {\strut #2~#5};
          }
        }
      }{ % else: with Title
        \mdfsetup{
          frametitle={
            \tikz[baseline=(current bounding box.east),outer sep=0pt]
            \node[anchor=east,rectangle,fill=#3]
            {\strut #2~#5:~##1};
          }
        }
      } % either case
      \mdfsetup{
        linecolor=#3,
        innertopmargin=0pt,
        linewidth=2pt,
        topline=true,
        frametitleaboveskip=\dimexpr-\ht\strutbox\relax,
      }
      \begin{mdframed}[]\relax
      }{ % post env command
      \end{mdframed}
    }
  }
#+END_SRC

*** Box Environments
#+BEGIN_SRC latex :noweb-ref box-environments
  \defboxenv{dfn}{Definition}{orange!50}{theo}{\thetheo}
  \defboxenv{edfn}{(Definition)}{orange!20}{theo}{\thetheo}
  \defboxenv{thm}{Theorem}{blue!40}{theo}{\thetheo}
  \defboxenv{lem}{Lemma}{blue!20}{theo}{\thetheo}
  \defboxenv{prop}{Proposition}{myblue}{theo}{\thetheo}
  \defboxenv{cor}{Corollary}{blue!30}{theo}{\thetheo}
  \defboxenv{xmp}{Example}{green!50!blue!30!white}{theo}{\thetheo}
  \defboxenv{rem}{Remark}{red!20}{theo}{\thetheo}
  \defboxenv{exr}{Exercise}{black!30!white}{exer}{\theexer}
#+END_SRC

* Tangling
Here, I chose which code blocks I export into which header file
#+BEGIN_SRC latex :noweb yes :tangle ./classic-header.tex
  <<font-settings>>
  <<content>>
  <<page-settings>>
  <<math>>
  <<graphics>>
  <<formatting>>
  <<colors>>
  <<symbols>>
  <<delimiters>>
  <<math-operators>>
  <<default-environments>>
  <<custom-environments>>
  <<classic-theorems>>
#+END_SRC

#+BEGIN_SRC latex :noweb yes :tangle ./fancy-header.tex
  <<font-settings>>
  <<content>>
  <<page-settings>>
  <<math>>
  <<graphics>>
  <<formatting>>
  <<colors>>
  <<symbols>>
  <<delimiters>>
  <<math-operators>>
  <<default-environments>>
  <<custom-environments>>
  <<fancy-setup>>
  <<box-environments>>
#+END_SRC
