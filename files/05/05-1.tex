\subsection{Definition and examples}

\begin{dfn}[]
  Let $\alpha,\beta: [0,1] \to X$ be two paths.
\begin{itemize}
  \item $\alpha,\beta$ are \textbf{homotopic \rep{}}, if there exists a homotopy preserving the endpoints. That is, there exists a continuous map $H: [0,1] \times [0,1] \to X$ with
    \begin{align*}
      H(s,0) = \alpha(s) \quad \text{and} \quad H(s,1) = \beta(s) \quad \text{for all} \quad  s \in [0,1]
      \\
      H(0,t) = \alpha(0) \quad \text{and} \quad H(1,t) = \alpha(1) \quad \text{for all} \quad t \in [0,1]
    \end{align*}
    We write $\alpha \sim \beta$ \rep{} (or $\alpha \simeq \beta$) if that this the case\footnote{This clashes with the notation used by Prof.\ Feller, who writes $f \simeq g$ if $f,g$ are homotopic (we use $\sim$) \textbf{and} $\alpha \simeq \beta$ if they are homotopic \rep{}.}.

    In particular, this means $\alpha(0) = \beta(0)$ and $\alpha(1) = \beta(1)$.
  \item $\alpha$ is called a \textbf{loop at $x_0$} (\textbf{Schleife an $x_0$}) if $\alpha(0) = \alpha(1) = x_0$.

    If $\alpha$ and $\beta$ are loops with the same basepoint $x_0$ and are homotopic \rep{}, we say that they are \textbf{homotopic rel.\ $x_0$}.
\end{itemize} 
Homotopy \rep{} defines an equivalence relation. We denote the equivalence class of $\alpha$ by 
\begin{align*}
  [\alpha] := \{\gamma | \gamma\text{ is a path in $X$ and } \alpha \sim \gamma \text{ rel endpoints}\}
\end{align*}
\end{dfn}

\begin{xmp}[]
  Homotopy of loops is a two-dimensional concept. So when looking for examples, it is a good idea to start with subsets of $\R^{2}$.
  Let $X = \C^{\times} = \C \setminus \{0\}$.
  \begin{itemize}
    \item For any $x_0 \in X$, there is the \textbf{constant loop} $\const_{x_0}$ defined by 
      \begin{align*}
        \const_{x_0} = (t \mapsto x_0) : [0,1] \to X
      \end{align*}
    \item For any $k \in \Z$, define the loops
      \begin{align*}
        \alpha_k = (s \mapsto e^{2 \pi i ks}) : [0,1] \to X
      \end{align*}
      One can easily show that they are all homotopic.
      But as we will see later, the different $\alpha_k$ are pairwise not homotopic \rep{} and that they satisfy
      $\alpha_{k} \alpha_{\ell} \sim \alpha_{k + \ell}$ \rep{}.
    \item For any pointed topological space $(X,x_0)$ and $\alpha$ a loop at $x_0$, define the \emph{reversal} of the loop by
      \begin{align*}
        \alpha^{-} = (s \mapsto  \alpha(1-s)): [0,1] \to X
      \end{align*}
      Then $\alpha \alpha^{-}$ is homotopic \rep{} to $\const_{x_0}$.
      Indeed, define
      \begin{align*}
        H(s,t) = \left\{\begin{array}{ll}
          \alpha(st) & t \leq \tfrac{1}{2}\\
          \alpha^{-}(1 - st)& t \geq \tfrac{1}{2}
        \end{array} \right.
      \end{align*}
      Then
      \begin{align*}
        H(s,0) = \alpha(0) = \const_{x_0}(s)
        ,\quad
        H(s,1) = \alpha(s) = 
      \end{align*}
  \end{itemize}
\end{xmp}

\begin{rem}[]
  If $\alpha \sim \alpha'$ \rep{} and $\beta \sim \beta'$ \rep{}, then $\alpha \beta \sim \alpha' \beta'$ \rep{}.
\end{rem}

\begin{dfn}[]
  Let $(X,x_0) \in \Top^{\ast}$.
  The \textbf{fundamental group} of $(X,x_0)$ is 
  \begin{align*}
    \pi_1(X,x_0) := \{[\alpha] \big\vert \alpha \text{\ is a loop at $x_0$}\} = 
    \faktor{\left\{
      \text{Loops at $x_0$}
    \right\}}{\text{Homotopy \rep{}}}
  \end{align*}
\end{dfn}
\begin{lem}\label{lem:fundamental-group}
  The fundamental group $\pi_1(X,x_0)$ is a group with multiplication
  \begin{align*}
    ([\alpha],[\beta]) \mapsto [\alpha \beta]
  \end{align*}
\end{lem}
We have to show that the multiplication is well-defined and that it does indeed define a group structure.
\begin{proof}
  The well-defined-ness of the multiplication is in the Exercise sheet 7, Problem 1.

  The other group axioms are easily proven.
  The identity of the group is the constant map $x_0$ and inverse is the reverse traversal of the loop.
\end{proof}

\begin{xmp}[]
Let $K \subseteq \R^{n}$ be convex and $x_0 \in K$. Then the fundamental group $\pi_1(K,x_0)$ is the trivial group $\{e\}$.
\end{xmp}

\begin{thm}[]\label{thm:fund-group-of-circle}
  \begin{enumerate}
    \item The fundamental group of the sphere is the trivial group $\{e\}$. Moreover, for all $n \geq 2$, $\pi_1(\IS^{n},x_0) = \{e\}$ 
    \item The fundamental group of the circle is $\Z$ and the map 
      \begin{align*}
        \Phi: \Z \to \pi_1(\IS^{1},1)
        , \quad
        k \mapsto \alpha_k = (t \mapsto  e^{2 \pi i kt})
        :[0,1] \to \IS^{1}
      \end{align*}
      is a group isomorphism.
  \end{enumerate}
\end{thm}
\begin{proof}
\begin{enumerate}
  \item Let $n \geq 2$ and $\alpha$ a loop at $x_0$.
    WLOG, we can assume $x_0$ is the north-pole.
    The idea is to take the south-pole $-x_0 \in \IS^{n}$ and ``push'' the points in $\image \alpha$ away from it towards $x_0$, thus contracting it to the loop $\const_{x_0}$.
    Visually this is what would happen if you were to puncture a balloon at the south-pole.
    This is of course only possible is $-x_0 \notin \image\alpha$, so we need to ``nudge'' $\alpha$ away from $-x_0$ everytime it crosses it.

    To fix this issue, we claim that there exists a loop $\beta \sim \alpha$ rel $x_0$ with $-x_0 \notin \image \beta$.
    Let $D^{n} \subseteq \IS^{n}$ be the southern hemisphere with center $-x_0$.
    We construct $\beta$ by keeping $\alpha$ whenever it is on the northern hemisphere and pushing it towards the equator on the southern hemisphere.
    This is possible because $D^{n}$ is contractible, so let ${(I_j)}_{j \in J}$ be the maximal intervals such that $\Image(\alpha |_{I_J}) \subseteq D^{n}$ and set
    \begin{align*}
      \beta(t) = \left\{\begin{array}{ll}
          \alpha(s) & s \notin \bigcup_{j \in J}I_j\\
          \beta_j(s) & s \in I_j
      \end{array} \right.
    \end{align*}
    such that $\beta_j(s): I_j \to \del D^{n}$ satisfies
    \begin{align*}
      \beta_j |_{\del I_j} = \alpha|_{\del I_j}
    \end{align*}
    It follows that $-x_0 \notin \image \beta$. 
    So since $\IS^{n} \setminus \{-x_0\} \iso \R^{n}$ is contractible, it follows that $\alpha \sim \beta \sim \const_{x_0}$.

  \item We only provide an informal proof. We will do it more rigorously later.
    For surjectvitiy of the map $\Phi: \Z \to  \pi_1(\IS^{1},1), k \mapsto  \alpha_k$, let $\alpha$ be a loop with basepoint $1$.

    The idea then is that we use the map 
    \begin{align*}
      \psi: \R \to \IS^{1}, \quad x \mapsto e^{2 \pi i x}
    \end{align*}
    that wraps $\R$ around the circle.
    We then show that there exists a path $\tilde{\alpha}: [0,1] \to  \R$ such that $\alpha = \psi \circ \tilde{\alpha}$.

    So if we set $k = \tilde{\alpha}(1) \in \Z$. then we can show that $\tilde{\alpha} \sim \tilde{a}_k$ rel endpoints.
    Therefore
    \begin{align*}
      \alpha = \pi \circ \tilde{\alpha} \sim \pi \circ \tilde{\alpha}_k = \alpha_k \text{ rel 1}
    \end{align*}
    For injectivity, we prove that $\Phi$ is group homomorphism with kernel $0$.
\end{enumerate}
\end{proof}


\begin{lem}[]
  The fundamental group defines a covariant functor $\pi_1: \Top^{\ast} \to \Grp$ that maps pointed topological spaces $(X,x_0)$ to their fundamental group $\pi_1(X,x_0)$ and that maps a basepoint preserving maps $f: (X,x_0) \to (Y,y_0)$ to group homomorphisms
\begin{align*}
  f_{\ast} := \pi_1(f):  \pi_1(X,x_0) \to \pi_1(Y,y_0)
  , \quad
  [\alpha] \mapsto [f \circ \alpha]
\end{align*}
Which means
\begin{enumerate}
  \item For any topological space $X$
    \begin{align*}
      (\id_X)_{\ast} = \id_{\pi_1(X,x_0)} \quad \text{ for all } x_0 \in X
    \end{align*}
  \item For continuous, basepoint preserving maps $f: (X,x_0) \to (Y,y_0)$, $g:(Y,y_0) \to (Z,z_0)$
    \begin{align*}
      (g \circ f)_{\ast} = g_{\ast} \circ f_{\ast}
    \end{align*}
\end{enumerate}
\end{lem}
\begin{proof}
  It follows directly from the definition of $\pi_1(f)$ that
  \begin{enumerate}
    \item For all $\alpha \in \pi_1(X,x_0)$:
      \begin{align*}
        (\id_X)_{\ast}([\alpha]) = 
        [\id_X \circ \alpha] = [\alpha] = \id_{\pi_1(X,x_0)}([\alpha])
      \end{align*}
    \item For all $\alpha \in \pi_1(X,x_0)$:
      \begin{align*}
        (g \circ f)_{\ast}([\alpha]) =
        [(g \circ f) \circ \alpha]
        = [g \circ (f \circ \alpha)]
        = g_{\ast}([f \circ \alpha])
        = g_{\ast}(f_{\ast}([\alpha]))
        = (g_{\ast} \circ f_{\ast})([\alpha])
      \end{align*}
  \end{enumerate}
\end{proof}

