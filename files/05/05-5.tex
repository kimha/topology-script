\subsection{Computing Fundamental groups}\label{ssec:pi1-examples}
This section contains examples where we can compute the fundamental group using what we've learned in this chapter.
Although marked as examples, you should try to compute the fundamental groups yourself as an exercise before checking the answer.

\subsubsection*{The $n$-sphere}
Let $n \geq 2$. If we set
\begin{align*}
  A = \IS^{n} \setminus \{\text{north pole}\}, \quad B = \IS^{n} \setminus \{\text{south pole}\}
\end{align*}
we see that both $A$ and $B$ are contractible, so $\pi_1(A) = \pi_1(B) = \{e\}$.
By Seifert-van Kampen's theorem it follows
\begin{align*}
  \pi_1(\IS^{n}) = \faktor{\pi_1(A) \ast \pi_1(B)}{N} \subseteq \{e\} \ast \{e\} = \{e\} \quad \text{for all } \quad n \geq 2
\end{align*}

\begin{xmp}[]
  For $X = \C \setminus \{-1,1\}$, the theorem says that $\pi_1(X) = F_2 = \Z \ast \Z$.
\end{xmp}

\begin{xmp}[]
  Recall the wedge product $\IS^{1} \vee\IS^{1} = (\IS^{1} \times \{1\}) \cup (\{1\} \times \IS^{1})$, which in our case looks like $\infty$.

  Setting
  \begin{align*}
    A = (\IS^{1} \times \{1\}) \cup (\{(1,e^{2 \pi i s}) \big\vert s \in (-\epsilon,\epsilon)\}
    , \quad \text{and} \quad
    B = -A
  \end{align*}
  we find that
  \begin{align*}
    A \iso \IS^{1} \iso B, \quad \text{and} \quad A \cap B \iso \{0\}\\
    \implies \pi_1(X) \iso \Z \ast \Z
  \end{align*}
\end{xmp}

\subsubsection*{The dome}

Let $d \in \N$ and 
\begin{align*}
  \phi_d: \IS^{1} \to \C^{\times}, \quad z \mapsto  z^{d}
\end{align*}
Take the space $X$ obtained by glueing $\C^{\times}$ to $\ID^{2}$.
\begin{align*}
  X = \C^{\times} \cup_{\phi_d} \ID^{2}, x_0 = [\frac{1}{2}]  
\end{align*}
For $d=1$, this looks like the we put a dome around the hole in $\C^{\times}$.
We then can show that $\pi_1(X) \iso \Z/d\Z$

To prove this, we partition the space by removing the point of the dome and the dome itself:
\begin{align*}
  A = X \setminus [0], \quad \text{and} \quad B = \{[z] \big\vert z \in \ID^{2} \setminus \IS^{1}\}
\end{align*}
Then we see
\begin{align*}
  B \iso \ID^{2} \setminus \IS^{1} \implies \pi_1(B) = \{e\} \implies \pi_1(A) \ast \pi_1(B) = \pi_1(A)
\end{align*}
For the inclusion map
\begin{align*}
  \iota: \C^{\times} \hookrightarrow X, \quad z \mapsto  [z]
\end{align*}
Note that $\iota(\C^{\ast}) \subseteq A$ is a strong deformation retract, as we can pull the puncutured dome down.

This inclusion gives us an isomorphism
\begin{align*}
  \pi_1(A,[1]) = \{[\alpha_k] \big\vert k \in \Z\}
\end{align*}
where $\alpha_k$ is the path
\begin{align*}
  \alpha_k: [0,1] \to  A, s \mapsto  [e^{2 \pi i k s}]
\end{align*}
Although we chose the wrong basis point, $[1]$ instead of $x_0$, they are path connected, say by a path $\beta$, so
\begin{align*}
  \pi_1(A,x_0) = \{[\beta \alpha_k \beta^{-}] \big\vert k \in \Z\} \iso \Z
\end{align*}
Lastly, looking at the intersection $A \cap B$ we see
\begin{align*}
  A \cap B \iso \ID^{2} \setminus (\IS^{1} \cup \{0\}) \implies \pi_1(A \cap B) = \{[\beta_k] \big\vert k \in \Z\}
\end{align*}
where $\beta_k$ is the path
\begin{align*}
  \beta_k: [0,1] \to  A \cap B, \quad s \mapsto [\frac{1}{2} e^{2 \pi i k s}]
\end{align*}

Now we claim that $i_A: \pi_1(A \cap B) \to \pi_1(A)$ is given by $i_A([\beta_k]) = [\beta \alpha_{dk}\beta^{-}]$, because
\begin{align*}
  i_A = ([\beta_k]) = [\beta {\beta'}_k \beta^{-})]
\end{align*}
where we identified $\ID^{2}$ with the image of the glueing:
\begin{align*}
  {\beta'}_k(s) = [\underbrace{e^{2 \pi i k s}}_{\in \ID^{2}}] [\underbrace{e^{2 \pi i k d s}}_{\in \C^{\times}}]
\end{align*}

Therefore by the theorem, we get
\begin{align*}
  \pi_1(X) \iso \faktor{\pi_1(A) \ast \pi_1(B)}{N} = \pi_1(A)/N
\end{align*}
and since
\begin{align*}
  N 
  &= 
  \scal{\scal{i_A([\beta_k]) \left(
        i_B([\beta_k])
  \right)^{-1}} \big\vert [\beta_k] \in \pi_1(A \cap B) }\\
  &=\{[(\beta \alpha_k \beta^{-})^{d}] \big\vert k \in \Z\} \lhd \pi_1(A) \implies \pi_1(A)/N \iso \Z/d\Z
\end{align*}


Alternatively, with the generators-and-relations notation, we have
\begin{align*}
  \pi_1(A) = \scal{\alpha_1\big\vert \emptyset} 
  , \quad
  \pi_1(B) = \scal{1 \big\vert \emptyset}
  ,\quad
  \pi_1(A \cap B) = \scal{\beta_1\big\vert \emptyset}
\end{align*}
Embedding the loop on the dome $\beta_1$ into $B$ returns the constant path $1$.
But embedding it into $A$ returns $\alpha_1^{d}$, as $\phi_d$ makes it go around $d$-times faster.
Seifert van Kampen's theorem gives us
\begin{align*}
  \pi_1(X) = \scal{\alpha_1 \big\vert i_A(\beta_1) = \alpha_1^{d} = 1 = i_B(\beta_1)} \iso \Z/d\Z
\end{align*}



\subsubsection*{The Real Projective space} %$\RP^{n}$}
{\itshape
I think it is probably the best example to learn from as it makes use of pretty much all aspects of Seifert-van Kampen's theorem that might come up in other examples and shows how we need to keep simplifying the problem before we even try to apply the theorem.
So although this example might take a bit longer to work through, it is well worth it and you should give it a try.
}

For $n \geq 0$, the real projective space $\RP^{n}$ is often defined as the collection of lines through the origin in euclidean space.
Define the equivlance relation on $\R^{n+1} \setminus \{0\}$ by
\begin{align*}
  x \sim \lambda x \quad \text{for} \quad \lambda \in \R \setminus \{0\}
\end{align*}
Then $\RP^{n} := \faktor{\R^{n+1} \setminus \{0\}}{\sim}$ with the quotient topology.

\textbf{$\bm{n = 0}$:} There is only one line in $\R^{1}$, so $\RP^{0} = \{\ast\}$ is one point.
In particular $\pi_1(\RP^{0}) = \{e\}$.

\textbf{$\bm{n \geq 1}$:} Generally, having equivalence classes that consist of many points willl make the problem hard. By restricting the equivalence relation to the $n$-sphere $\IS^{n}$, where we only identify antipodal points:
\begin{align*}
  \RP^{n} \iso \faktor{\IS^{n}}{\sim} \quad \text{with} \quad 
  x \sim -x \quad \text{for} \quad x \in \IS^{n}
\end{align*}
the equivalence classes only consist of two points instead of an entire line. 
But we can do even better. 
Because every class consists of two opposite points now, it suffices to consider one hemisphere of $\IS^{n}$.
We can thus describe $\RP^{n}$ as the quotient space of the very simple $n$-disk $\ID^{n} \subseteq \R^{n}$, where we identify opposite points \emph{on its boundary}
\begin{align*}
  \RP^{n} \iso \faktor{\ID^{n}}{\sim} \quad \text{with} \quad 
  x \sim -x \quad \text{for} \quad x \in \del \ID^{n} = \IS^{n-1}
\end{align*}
\textbf{$\bm{n = 1}$:} The $1$-disk $\ID^{1}$ is the interval $[-1,1]$. Identifying the opposite points on its boundary $\del \ID^{1} = \{-1,1\}$, we obtain the $1$-sphere, so $\RP^{1} \iso \IS^{1}$ and thus $\pi_1(\RP^{1}) = \Z$.

\textbf{$\bm{n \geq 2}$:} In fact, we see that we have attached $\RP^{n-1}$ to the boundary of $\ID^{n}$. So we can write $\RP^{n}$ as the glueing (see Definition~\ref{dfn:glueing})
\begin{align*}
  \RP^{n} \iso \RP^{n-1} \cup_{\phi} \ID^{n}
\end{align*}
where $\phi$, using some isomorphisms and abuse of notation can be described by 
\begin{align*}
  \phi: \del \ID^{n} \iso \IS^{n-1} &\to \faktor{\IS^{n-1}}{\sim} \iso \RP^{n-1}
  \\
  x &\mapsto [x]
\end{align*}
We can now compute $\pi_1(\RP^{n})$ using Seifert-van Kampen's theorem. Before moving on, think about which sets $A,B \subseteq \RP^{n}$ are best suited for use in the theorem.

\vspace{3\baselineskip}

Let $\pi: \ID^{n} \to \faktor{\ID^{n}}{\sim} \iso \RP^{n}$ denote the quotient map and set
\begin{align*}
  A := \pi(\mathbb{B}^{n}) \iso \mathbb{B}^{n}
  \quad \text{and} \quad 
  B := \pi(\ID^{n} \setminus \{0\}) \simeq \RP^{n-1}
\end{align*}
where the $\mathbb{B}^{n} = \ID^{n} \setminus \del \ID^{n}$ is the open $n$-ball. The isomorphism for $A$ comes from the fact the the equivalence relation only affects the points in $\del \ID^{n}$, whereas the homotopy equivalence for $B$ follows because $\del\ID^{n} \subseteq \ID^{n} \setminus \{0\}$ is a deformation retract.

We have that $A \cap B = \mathbb{B}^{n} \setminus \{0\}$ is path-connected for $n \geq 2$ and
\begin{align*}
  \pi_1(A) = \{e\} \quad \text{and} \quad 
  \pi_1(B) = \pi_1(\RP^{n-1})
\end{align*}
\textbf{$\bm{n = 2}$:} As $\pi_1(\RP^{1}) \iso \pi_1(\IS^{1}) \iso \Z$, Seifert-van Kampen's theorem says that
\begin{align*}
  \pi_1(\RP^{2}) \iso 
  \faktor{
    \pi_1(A) \ast \pi_1(B)
  }{
    \underbrace{
      \{i_A(c) {i_B(c)}^{-1} \big\vert c \in \pi_1(A \cap B)\}
    }_{N}
  }
\end{align*}
so all we need to do is find out how loops in $A \cap B$ embed into $\RP^{2}$.
Let $\gamma$ be a loop in $A \cap B \iso \mathbb{B}^{2} \setminus \{0\}$ that goes around the origin once. Note that $[\gamma] =: c$ is a generator of $\pi_1(A \cap B) \iso \Z$, so once we found out $i_A(c)$ and $i_B(c)$, we know $N$.

Embedding $\gamma$ into $A \iso \mathbb{B}^{2}$, which is contractible, we get $i_A(c) = e$, the neutral element in $\pi_1(A)$.

Embedding $\gamma$ into $B \iso \RP^{1}$, we get a loop that goes around the hole \emph{twice}, so $i_B(c) = c^{2}$.

We therefore find that
\begin{align*}
  \pi_1(\RP^{2}) = \faktor{\pi_1(A) \ast \pi_1(B)}{N}
  = \scal{e, c \big\vert c^{2} = e} \iso \faktor{\Z}{2\Z}
\end{align*}
\textbf{$\bm{n \geq 3}$:} Since $A \cap B \iso \mathbb{B}^{n} \setminus \{0\}$ has a deformation retract $\IS^{n-1} \subseteq \mathbb{B}^{n} \setminus \{0\}$, and we know that $\pi_1(\IS^{n-1}) = \{e\}$ for $n \geq 3$, the subgroup $N$ is trivial, so we get
\begin{align*}
  \pi_1(\RP^{n}) = \faktor{\pi_1(A) \ast \pi_1(B)}{\underbrace{N}_{\{e\}}}
  = \underbrace{\pi_1(\mathbb{B}^{n})}_{\{e\}} \ast \pi_1(\RP^{n-1}) \iso \pi_1(\RP^{n-1})
\end{align*}
which by induction is the same as $\pi_1(\RP^{2}) \iso \faktor{\Z}{2\Z}$.


