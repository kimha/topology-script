\subsection{Exercises}
\begin{exr}[]
  What would change in Lemma~\ref{lem:path-lift} if $Y \stackrel{\pi}{\to}X$ wouldn't be a covering, but just a fiber bundle?
\end{exr}
\begin{sol}
If the fibers aren't discrete, then uniqueness of a lift is not guaranteed.
Take for example the fiber bundle
\begin{align*}
  \pi:  {[0,1]}^{2} \to [0,1] 
  ,\quad
  (x,y) \mapsto  x
\end{align*}
For $\alpha = \id_{[0,1]}$ the identity path, we can have multiple lifts of $\alpha$ to $Y$ to the same starting point $y_0 = (0,0)$. Examples are
\begin{align*}
  \tilde{\alpha}: [0,1] \to {[0,1]}^{2}
  ,\quad
  x \mapsto (x,0)
  \quad \text{or} \quad 
  \tilde{\alpha}: [0,1] \to {[0,1]}^{2}
  x \mapsto (x,\sin(x))
\end{align*}
\end{sol}

\begin{exr}[]
  Find an example of a covering $\pi: Y \to X$ with two paths $\alpha,\beta$ in $X$ that are not homotopic \rep, but such that their lifts $\tilde{\alpha},\tilde{\beta}$ with common basepoint are.
\end{exr}
\begin{sol}
    Take the covering $\pi: \R \to \IS^{1}$ with $\alpha$ clockwise and $\beta$ anti-clockwise paths (so that they are not homotopic \rep).
    Since $\R$ is contractible, any two paths in $\R$ (in particular the lifts $\tilde{\alpha},\tilde{\beta}$) are homotopic \rep.
\end{sol}


%\begin{exr}[Connection to Galois Theory]
%  Covering spaces of topological spaces are like algebraic extensions of a field.
%  The universal covering is like the algebraic closure.
%  %The fundamental group is like the Galois group of the algebraic closure.
%  The relation between covers and subgroups of the fundamental group is like the relation between intermediate field extensions and subgroups of the Galois group.
%
%  What does this mean?
%  An element of the deck-transformation group $\Deck(\pi)$ is an automorphism of the covering space keeping the covering map $\pi$ invariant.
%  An element of the Galois group $\text{Gal}(E/k)$ is a field automorphism of $E$ that keeps $k$ invariant.
%
%\end{exr}

\begin{exr}[]
  \hypertarget{exr:leaf-index}{Let} $(Y,y_0) \stackrel{\pi}{\to}(X,x_0)$ be a covering with $Y$ path-connected.
  Show that there is a bijection of sets
  \begin{align*}
    \pi^{-1}(x_0)
    \iso
    \pi_1(X,x_0)/G(\pi)
  \end{align*}
  In particular, $\pi$ is an $n$-leaf covering if and only if the index $[\pi_1(X) : G(\pi)]$ is $n$.

  \textsf{Hint: Look at the covering $\pi: \IS^{1} \to \RP^{1}$ to get intuition behind the construction of the maps.}
\end{exr}
\begin{sol}
  We define inverse maps 
  \begin{center}
  \begin{tikzcd}[ ] 
    \pi^{-1} (x_0)
    \arrow[bend left]{r}{\Phi}
    &
    \pi_1(X,x_0)/G (\pi)
    \arrow[bend left]{l}{\Psi}
  \end{tikzcd}
  \end{center}
  
  \textbf{Definition of $\bm{\Phi}$:} Since $Y$ is path-connected, for every $y \in \pi^{-1}(x_0)$, there exists a path $\gamma: [0,1] \to Y$ from $y_0$ to $y$. Then $\pi \circ \gamma :[0,1] \to X$ is a path in $X$.
  Then define $\Phi(y) := [(\pi \circ \gamma)]G(\pi)$ to be the coset in $\pi_1(X,x_0)$.

  \textbf{Claim: This construction does not depend on $\gamma$.} 
  If $\gamma': [0,1] \to Y$ is another path from $y_0$ to $y$, then $\gamma {\gamma'}^{-}$ is a loop at at $y_0$, so 
  \begin{align*}
    \pi_{\ast}([\gamma {\gamma'}^{-}]) = \pi \circ (\gamma \gamma^{-}) = [\pi \circ \gamma][\pi \circ \gamma]^{-1} \in G(\pi)
  \end{align*}
  so $[(\pi \circ \gamma)]$ and $[(\pi \circ \gamma')]$ belong to the same coset.

  \textbf{Definition of $\bm{\Psi}$:} 
  Define the map $\psi: \pi_1(X,x_0) \to \pi^{-1}(x_0)$ as follows:
  For $\alpha$ a path in $X$, let $\overline{\alpha}$ be \hyperlink{lem:lifting-of-paths}{the lift} of $\alpha$ to $Y$ at $y_0$ and define $\psi([\alpha]) = \overline{\alpha}(1)$ to be the endpoint of the lift.

  $\psi$ is well-defined because if $\alpha \sim \alpha'$ \rep, then by the \hyperlink{cor:monodromy-lemma}{Monodromy Lemma}, $\overline{\alpha}(1) = \overline{\alpha'}(1)$.

  Now define $\Psi([\alpha]G(\pi)) = \psi([\alpha]) = \overline{\alpha}(1)$.

  \textbf{$\bm{\Psi}$ is well-defined:} This part of the proof is left as an exercise to the reader.
  %Let $\alpha_1,\alpha_2:[0,1] \to X$ be loops at $x_0$ and $\overline{\alpha_1}, \overline{\alpha_2}$ be their lifts to $y_0$.
  %If $[\alpha_1]$ and $[\alpha_2]$ belong to the same coset, there exists a loop $\gamma: [0,1] \to Y$ at $y_0$ such that $\pi_{\ast}([\gamma]) = [\pi \circ \gamma] = [\alpha_1]^{-1}[\alpha_2]$.
  %By reparamaterizing $\gamma$, we may also assume that $(\pi \circ \gamma)(\tfrac{1}{2}) = x_0$.

  %Define the loops
  %\begin{align*}
  %  \gamma_1:[0,1] \to Y 
  %  \quad
  %  t \mapsto \gamma(\tfrac{t}{2})
  %  \\
  %  \gamma_2:[0,1] \to Y 
  %  \quad
  %  t \mapsto \gamma(\tfrac{1}{2} + \tfrac{t}{2})
  %\end{align*}
  %Then $\gamma_1^{-}$ is a lift of $\alpha_1^{-}$ and $\gamma_2$ is a lift of 
  %$\overline{\alpha}^{-}\overline{\alpha'}$ is a lift of $\alpha^{-}\alpha'$ at $\alpha(1)$.




  \textbf{$\mathbf{\Psi \circ \Phi = \id}$:} 
  Let $y \in \pi^{-1}(x_0)$, $\gamma$ a path from $y_0$ to $y$ and $\overline{\pi \circ \gamma}$ the lift of $\pi \circ \gamma$.
  Since both $\gamma$ and $\overline{\pi \circ \gamma}$ are lifts of $\pi \circ \gamma$ at $y_0$, they must be identical by \hyperlink{lem:unique-lift-of-path}{the uniqueness} of lifting of paths.
  Thus 
  \begin{align*}
    (\Psi \circ \Phi)(y) = \Psi([\pi \circ \gamma]G(\pi)) = \overline{\pi \circ \gamma}(1) = \gamma(1) = y
  \end{align*}

  \textbf{$\mathbf{\Phi \circ \Psi = \id}$:} 
  Let $\alpha$ be a loop in $X$, $\overline{\alpha}$ its lift at $y_0$.
  Let $\gamma$ be any path from $y_0$ to $\overline{\alpha}(1)$.
  Then
  \begin{align*}
    (\Phi \circ \Psi)([\alpha]G(\pi)) = \Phi(\overline{\alpha}(1)) = [\pi \circ \gamma]G(\pi)
  \end{align*}
  Using pretty much the same reasoning as in the claim above, one shows that $[\alpha]$ and $[\pi \circ \gamma]$ belong to the same coset by concatenating $\overline{\alpha}$ with $\gamma^{-}$.

\end{sol}

\begin{exr}[Winter 2021 Exam (part a)]
Let $X  = \{z \in \C \big\vert 2 \leq \abs{z} \leq 3\}$.
Find, up to \hyperlink{dfn:isomorphic-over-X}{isomorphism of coverings}, all coverings $f: Y \to X$ for which $Y$ is connected.
\end{exr}
\begin{sol}
%  Clearly, $X$ is homotopy equivalent to $\IS^{1}$, which has fundamental group $\pi_1(\IS^{1}) \iso \Z$.
%  $X$ is also sufficiently connect, so by the \hyperlink{thm:existence-theorem}{Existence theorem}, for every subgroup $H \subseteq \Z$, there exists such a covering $f_H: Y \to X$ with $Y$ connected (and locally path connected) such that its characteristic group is $G(f)$ is $H$.
%  
%  Since $X$ is locally path connected, 

  Man sieht einfach, dass $X$ homotopieäquivalent zum Kreis $\mathbb{S}^{1}$ ist, welches Fundamentalgruppe $\pi_1(\mathbb{S}^1) \cong \mathbb{Z}$ hat.
  $X$ ist auch hinreichend zusammenhängend, also gibt es nach dem Existenzsatz für jede Untergruppe $H < \pi_1(X) \cong \mathbb{Z}$ eine Überlagerung $f_H: Y \to X$ mit $Y$ zusammenhängend und charakteristischer Untergruppe $G(f_H) = H$.
  (Dieser Satz ist für diese Aufgabe nicht notwendig, aber hilft als Startpunkt bzw.\ als Indikator falls wir eine vergessen haben).

  Die Untergruppen von $\Z$ sind gerade $n\Z$ für $n = 0,1,2\ldots$.
  Zugehörige Überlagerungen sind z.B.
  \begin{align*}
    \text{für }n \geq 1 \quad
    &f_n: X \to X, 
    r \cdot e^{i \theta} 
    \mapsto r \cdot e^{i n \theta}, 
    \\
    &f_0: [2,3] \times \R \to X,
    (r,s) 
    \mapsto r \cdot e^{i \theta s}
  \end{align*}

  Wir möchten nun den Eindeutigkeitssatz benutzen, um andere Überlagerungen auszuschliessen. 
  Sei $f: Y \to  X$ eine zusammenhängende Überlagerung.
  Da $X$ lokal wegzusammenhängend ist, muss auch der Überlagerungsraum lokal wegusammenhängend sein [1]. Somit ist $Y$ auch wegzusammenhängend [2].
  Nach dem Eindeutigkeitssatz ist $f$ zu jenem $f_n$ isomorph, deren Charakteristische Untergruppen $G(f), G(f_n)$ sich gleichen.

  [1] wurde in der Vorlesung bewiesen, [2] glaub auch (könnte eine Serie gewesen sein).

  [1]: Für $y \in Y$, sei $U$ eine Umgebung von $f(y)$, sodass $f|_{f^{-1}(U)}: f^{-1}(U) \to U$ eine triviale Faser ist.
  Sei also $F$ diskret sodass $\pi^{-1}(U) \stackrel{\phi}{\cong} U \times F$. Da $X$ lokal wegusammenhängend ist, gibt es eine wegzusammenhängende Umgebung $V \subseteq U$ von $f(y)$, dann ist die Komponente von $\phi^{-1}(V \times F)$ die $y$ enthält eine wegzusammenhängende Umgebung von $y$.

  [2]: Sei $Z$ zusammenhängend und lokal wegzusammenhängend und $z \in Z$. Sei $C$ die Wegzusammenhangskomponente von $z$. Wir zeigen dass $C$ offen und abgeschlossen ist.

  $C$ ist offen: Sei $u \in C$


  $C$ offen + abgeschlossen + $Z$ zusammenhängend $\implies$ $C = Z$.
\end{sol}

\begin{exr}[]
Find all 2-leaf coverings $\pi: Y \to \IS^{1} \vee \IS^{1}$ up to \hyperlink{dfn:isomorphic-over-X}{isomorphism over} $\IS^{1} \vee \IS^{1}$ for which $Y$ is path-connected.
\end{exr}
\begin{sol}
  By the \hyperlink{exr:leaf-index}{previous Exercise}, if $\pi: Y \to X$ is a $2$-leaf covering, its characteristic subgroup $G(\pi) = \{\pi_{\ast}(c) \big\vert c \in \pi_1(Y)\}$ must have index $[\pi_1(X) : G(\pi)] = 2$.

  Any such subgroup $G(\pi)$ induces group homomorphisms
  \begin{align*}
    0 \to G(\pi) \stackrel{\iota}{\hookrightarrow} \pi_1(X) \stackrel{\phi}{\epi} \faktor{\pi_1(X)}{G(\pi)}\iso \Z/2\Z \to 0
  \end{align*}
  where $\iota$ is the inclusion map and $\phi$ the quotient map and such that $\image \iota = \Ker \phi$.
  
  Since $\pi_1(X) = \pi_1(\IS^{1}) \ast \pi_1(\IS^{1}) = <a,b>$ is the free group on two generators,
  we want to find all surjective group homomorphisms $\phi: <a,b> \to \Z/2\Z$.
  By the universeal property of the free group, this is equivalent to finding non-zero maps $f: \{a,b\} \to \Z/2\Z = \{0,1\}$.
  There are $3$ such maps:
  \begin{align*}
    f_L: \{a,b\} \to \{0,1\}, f_L(a) = 1, f_L(b) = 0\\
    f_R: \{a,b\} \to \{0,1\}, f_R(a) = 0, f_R(b) = 1\\
    f_B: \{a,b\} \to \{0,1\}, f_R(a) = 1, f_R(b) = 1
  \end{align*}
  these induce group homomorphisms
  \begin{align*}
    \phi_L: \scal{a,b} \to \Z/2\Z,
    &\quad
    a^{n_1}b^{n_1}\cdot a^{n_k}b^{n_k} 
    \mapsto \sum_{i=1}^{k}n_i \mod 2
    \\
    \phi_R: \scal{a,b} \to \Z/2\Z,
    &\quad
    a^{n_1}b^{n_1}\cdot a^{n_k}b^{n_k} 
    \mapsto \sum_{i=1}^{k}m_i \mod 2
    \\
    \phi_{LR}: \scal{a,b} \to \Z/2\Z,
    &\quad
    a^{n_1}b^{n_1}\cdot a^{n_k}b^{n_k} 
    \mapsto \sum_{i=1}^{k}n_i + m_i \mod 2
  \end{align*}
  Since $X$ is sufficiently connected, we can use the \hyperlink{thm:existence-theorem}{existence theorem} to get coverings 
  \begin{align*}
    \begin{array}{lll}
    \pi_L: Y_L \to X 
    & &
    G(\pi_L) = \scal{a^{2},b}
    \\
    \pi_{R}: Y_R \to X
    & \text{with}
    & G(\pi_R) = \scal{a,b^{2}}
    \\
    \pi_{LR}: Y_{LR} \to X
    & &
    G(\pi_{LR}) = \{a^{n_1}b^{n_1}\cdots a^{n_k}b^{n_k} \in \scal{a,b} \big\vert
    \sum_{i=1}^{k}n_i + m_i \in 2\Z
    \}
    \end{array}
  \end{align*}
  note that the existence theorem doesn't tell us what the coverings look like.
  If you want to see pictures, see Exercise 5b from the HS21 Topology Exam (in German)\url{https://exams.vmp.ethz.ch/exams/jepy4rdn.pdf}.

  All subgroups $G(\pi_L),G(\pi_{R}),G(\pi_{LR})$ are different, so they are not isomorphic by the \hyperlink{thm:uniqueness-theorem}{uniqueness theorem}.

  Thus all $2$-leaf coverings of $\IS^{1} \wedge \IS^{1}$ are isomorphic to exactly one of $\pi_L,\pi_R,\pi_{LR}$



  

\end{sol}

\begin{exr}[]
Let $X$ be path connected and locally path connected and assume a universal cover $\pi:\tilde{X} \to X$ exists.
Show that
\begin{align*}
  \Deck(\pi) \iso \pi_1(X)
\end{align*}
\end{exr}
\begin{sol}
  By the deck transformation theorem, we have an isomorphism
  \begin{align*}
    \faktor{N_G}{G} 
    \iso \Deck(\pi)
  \end{align*}
  where $N_G = \{c \in \pi_1(X,x_0) \big\vert gc = cg \forall g \in G(\pi)\}$ is the normalizer.
  Since $Y \stackrel{\pi}{\to}X$ is a universal covering, $Y$ is simply connected, so $G = \{e\} < \pi_1(X,x_0)$ is trivial.
  In particular, every element of $\pi_1(X,x_0)$ commutes with $\{e\}$, so $N_G = \pi_1(X,x_0)$.
  Thus
  \begin{align*}
    \Deck(\pi) \iso \faktor{N_G}{G} = \faktor{\pi_1(X,x_0)}{\{e\}} \iso \pi_1(X,x_0)
  \end{align*}
\end{sol}


\begin{exr}[FS 21 Exam]
  For which $n \in \N$ does there exist an $n$-th root from $\C$ to $\C$?

  \emph{Note: A map $f_n: \C \to \C$ is called an $n$-th root if $(f_n(z))^{n} = z$ for all $z \in \C$.}
\end{exr}
\begin{sol}
  Consider the $n$-leaf covering
  \begin{align*}
    \pi: \C^{\ast} \to \C^{\ast}, \quad z \mapsto z^{n}
  \end{align*}
  If an $n$-th root $f_n: \C \to \C$ exists, then we can restrict it to obtain a lift of the identity $\id: \C^{\ast} \to \C^{\ast}$ via $\pi$.
  \begin{center}
  \begin{tikzcd}[ ] 
    & \C^{\times}
    \arrow[]{d}{\pi}
    \\
    \C^{\times}
    \arrow[dotted]{ur}{f_n|_{\C^{\times}}}
    \arrow[]{r}{\id}
    &
    \C^{\times}
  \end{tikzcd}
  \end{center}
But by the \hyperlink{thm:lifting-criterion}{lifting criterion}, such a lift exists if and only if
  \begin{align*}
    \id_{\ast}(\pi_1(\C^{\ast})) \subseteq G(\pi) = \pi_{\ast}(\pi_1(\C))
  \end{align*}
  Clearly, the left hand side $\id_{\ast}(\pi_1(\C^{\ast}))$ is just $\pi_{1}(\C^{\ast}) = \scal{a} \iso \Z$.
\end{sol}


\begin{exr}[Summer 2022 Exam --- Exercise 5]
Let $X:= \R^2 \setminus \{(0,0,0)\}$ and $Y := X /\sim$, where $v \sim w$ if either $v = w$ or $v = -w$.

\begin{enumerate}
  \item Compute the isomorphism class of the fundamental group of $Y$.
  \item Let $q: X \to Y$ be the quotient map and let
    \begin{align*}
      f: S^1 \to Y, \quad e^{2 \pi it} \mapsto q((\sin(\pi t), \cos(\pi t), 0))
    \end{align*}
    Does there exist a continuous function $g: S^1 \to X$ with $q \circ g = f$?
\end{enumerate}
\end{exr}
\begin{sol}

{\color{red}TODO:~Translate to English}.
\begin{enumerate}
  \item 
    % We use the deck transformation group theorem for the quotient map $q: X \to Y$.
Wir verwenden den Satz über Deckbewegungen für die Quotientenabbildung $q: X \to Y$.

\textbf{$\bm{q}$ ist eine Überlagerung:} 
Einen lokalen Homemorphismus um $[v]$ erhält man, indem man z.B. die Umgebung $\{[x] \big\vert x \in X : \|x-v\| < \|v\|\}$ wählt.
Die Faser von $[v] \in Y$ ist $\{v,-v\}$ (und insbesondere diskret), also ist $q$ eine Überlagerung.


\textbf{Deckbewegungsgruppensatz ist anwendbar:}
$X$ ist offensichtlich wegzusammenhängend und lokal wegzusammenhängend.
Da die Quotientenabbildung $q$ stetig und offen ist, ist ihr Bild $Y$ auch wegzusammenhängend und lokal wegzusammenhängend.
Da $X$ einfach zusammenhängend ist, ist die Charakteristische gruppe $G(q) = \pi_1(q)$ trivial und nach dem Decktransformationssatz ist $\text{Deck}(q) \cong N_{G(q)}/{G(q)} = \pi_1(Y)$.

\textbf{$\Deck(q) \cong \Z/2\Z$:}
Wir zeigen nun, dass $\text{Deck}(q) = \{\id,(x \mapsto  -x)\}$ ist.
Angenommen, $\phi: X \to X$ ist ein Homeomorphismus mit $q \circ \phi = q$.


\item 
  % By the \hyperlink{thm:lifting-criterion}{lifting criterion}, such a function $g: S^{1} \to X$ exists if and only if $f_{\ast}(\pi_1(S^{1})) \subseteq G(q)$.
    Nach dem Hochhebungskriterium gibt es eine solche Funktion $g: S^{1} \to X$ genau dann, wenn $f_{\ast}(\pi_1(S^{1})) \subseteq G(q)$.
    
\end{enumerate}
\end{sol}


\begin{exr}[]
Let $\pi: Y \to X$ be a $2:1$ covering with $X$ path-connected.
Show that
\begin{enumerate}
  \item $Y$ is path-connected $\iff$ there exists a loop in $X$ that lifts to a non-closed path in $Y$.
  \item $Y$ has at most two path-connected components.
    When it has two components, $Y = Y_1 \sqcup Y_2$, then $\pi|_{Y_i}: Y_i \to X$ is a homeomorphism (for $i =1,2$).
\end{enumerate}
\end{exr}
