\subsection{Orbit spaces}
%For a topological space $X$, the set of automorphisms
%\begin{align*}
%  \Aut(X) = \{\Phi: X \to X \text{ isomorphism}\}
%\end{align*}
%form a group under composition with identity $\id_X$.
%Elements of this group 

%We can think of groups as symmetries of spaces. Are there any special properties of such spaces?

%We of course want our group actions to be continous.

\begin{dfn}[]
Let $G$ be a topological group and $X$ a topological
space.

An \textbf{operation}/\textbf{continuous action} (stetige Gruppenwirkung) of $G$ on $X$ is a continuous group action of $G$ on $X$, i.e.\ a continuous map $G \times X \to X$ (written $(g,x) \mapsto gx$) such that
\begin{enumerate}
  \item $1 x = x$ for all $x \in X$
  \item $g_1(g_2 x) = (g_1 g_2) x$ for all $g_1,g_2 \in G, x \in X$
\end{enumerate}
We call such a topological space $X$ a \textbf{$G$-space}.
\end{dfn}

\begin{xmp}[]
  For the dihedral group with $8$ elements (and the discrete topology)
  \begin{align*}
    G = D_{2 \cdot 4} = \scal{R,T \big\vert T^{2} = 1, R^{4} = 1, RT = TR^{-1}}
  \end{align*}
  and $X = \C^{\times}$, we can define the continuous group action
  \begin{align*}
    T z = \overline{z}
    ,\quad
    Rz = z \cdot e^{\frac{2 \pi i}{4}}
  \end{align*}

  For the subgroup $\IS^{1} < \C^{\times}$, we can define the $\IS^{1}$-action on $\C^{\times}$ by multiplication.
\end{xmp}

\begin{xmp}[]
  Every topological group $G$ has a continuous $G$-action on itself given by its own group multiplication. 
  Moreover, for $H < G$ a subgroup, $G$ has a group action on the homogeneous space $G/H$ given by
  \begin{align*}
    G \times G/H \to G/H
    (g, xH) \mapsto (gx)H
  \end{align*}

\end{xmp}

\begin{dfn}[]
Let $X$ be a $G$-set and $x \in X$. The \textbf{orbit} of $x$ is the set
\begin{align*}
  G_x := \{gx \big\vert g \in G\}
\end{align*}
These orbits are equivalence classes of an equivalence relation $\sim$ on $X$ given by
\begin{align*}
  x \sim y \iff \exists g \in G: y = gx
\end{align*}
and as such, we call $X/G := X/\sim$ the \textbf{orbit space} of the group action.
\end{dfn}

\begin{xmp}[]
\begin{itemize}
  \item For $SO(n)_v = \{Av \big\vert A \in SO(n)\} = \{w \in \R^{n} \big\vert \abs{w} = \abs{n}\}$.
    We can visualize the orbits as $n-1$ spheres of any radius. 
    It is easy to see how the orbit space $\R^{n}/SO(n)$ is isomorphic to $[0,\infty)$.

  \item For $G = \R^{\times} = (\R \setminus \{0\}, \cdot)$ and $X = \R^{n+1} \setminus \{0\}$, the map
    \begin{align*}
      \R^{\times} \times X \to X, \quad (\lambda,x) \mapsto  \lambda x
    \end{align*}
    is a continuous action and the orbit space is called the $n$-dimensional \textbf{real projective space} $\R\IP^{n} = X/\R^{\ast}$.
    We can view this as consisting of straight lines through the origin of $\R^{n+1}$.
    One can also find an isomorphism $\faktor{\IS^{n}}{\{\pm 1\}} \iso \R\IP^{n}$.
\end{itemize}
\end{xmp}

\begin{dfn}[]
Let $X$ be a $G$-space, $x \in X$. 
We call $G_x := \text{Stab}_G(x) = \{g \in G \big\vert gx = x\} \subseteq G$ the \textbf{stabilizer} of $x$.
\end{dfn}
Clearly, the stabilizer is a subgroup.

\begin{xmp}[]
  Consider the group action of $SO(n)$ with the point $x = e_1 \in \mathbb{S}^{n-1}$.

  Its stabilizer $\text{Stab}_{SO(n)}(x)$ is isomorphic to $SO(n-1)$, as a matrix $A \in SO(n)$ satisfying $Ae_1 = e_1$ must be of the form
  \begin{align*}
    A = \begin{pmatrix}
    1 & 0\\
    0 & B
    \end{pmatrix}
    , \quad \text{where} \quad A^{T}A = 1 \implies B^{T}B = 1 \implies B \in SO(n-1)
  \end{align*}
  It is also a homoemorphism as $S^{n-1}$ is Hausdorff and $SO(n)/\text{Stab}_G(x)$ is compact (closed and bounded).
\end{xmp}

A simple, yet powerful theorem is the \textbf{topological orbit theorem}.
\begin{thm}[Topological Orbit theorem]
Let $X$ be a $G$ space and $x \in X$.
Then the map
\begin{align*}
  F: \faktor{G}{\text{Stab}_{G}(x)} \to \mathcal{O}_G(x), \quad g \cdot \text{Stab}_G(x) \mapsto gx
\end{align*}
is a continuous bijection.
\end{thm}
\begin{proof}
  This mapping is indeed well defined, as
  \begin{align*}
    a \cdot \text{Stab}_G(x) = b \cdot \text{Stab}_G(x) \implies \exists h \in \text{Stab}_G(x) \text{ with } a = bh \implies ax = (bh)x = b(hx) = bx
  \end{align*}
  By definition of the orbit, it's clearly surjective.
  For injectivity, we have
  \begin{align*}
    ax = bx \implies a^{-1}(bx) = a^{-1}(ax) = x \implies a^{-1}b \in \text{Stab}_G(x)
  \end{align*}
  and for continuity, we see that the mapping
  \begin{align*}
    f \circ \pi: G \to \mathcal{O}x, \quad g \mapsto  gx
  \end{align*}
  is continuous and we can use Lemma~\ref{lem:unipropquot} to show continuity of $f$. 
\end{proof}


