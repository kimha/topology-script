\subsection{Application and properties of $\pi_1$}

\begin{cor}[]
  If $f: X \to  Y$ is a homeomorphism, then $f_{\ast}: \pi_1(X,x_0) \to  \pi_1(Y,y_0)$ is a group isomorphism.
\end{cor}
\begin{proof}
  This follows directly from Lemma \ref{lem:func-iso} because homeomorphisms are the isomorphisms in $\Top^{\ast}$ and group isommorphisms are the isomorphisms in $\Grp$.
\end{proof}

Two applications of the functoriality of the fundamental group are the Brower fixpoint theorem, aswell as the fundamental theorem of Algebra.


\begin{thm}[Brower Fixpoint theorem]
  Let $n \in \N$ and $f: \ID^{n} \to \ID^{n}$ continuous. 
  Then $f$ has a fixpoint, so $\exists x \in \ID^{n}$ with $f(x) = x$.
\end{thm}
We will only prove it for $n \leq 2$ by using the fundamental group. For higher $n$, we will have to use another invariant. 
\begin{proof}
  Assume that $f$ has no fixpoint.
  We then can show that there exists a retraction $\rho: \ID^{n} \to  \del \ID^{n} = \IS^{n-1}$.

  The reason this is true is because if $f(x) \neq x$, then there exists a unique line from $x$ to $f(x)$.
  We then define $\rho(x)$ to be the intersection of that line with $\del \ID^{n}$, so
  \begin{align*}
    \{\rho(x)\} = \del \IS^{n-1} \cap \{f(x) + t\left(
        x - f(x)
    \right) \big\vert t \in \R_{> 0}\}
  \end{align*}
  this function is continuous, because in a neighborhood $U$ of $x$, $f(U)$ is a neighborhood of $f(x)$, and geometrically $\rho(U)$ is a neighborhood of $\rho(x)$.

  Now, we show that there cannot exist such a retraction $\rho: \ID^{n} \to \IS^{n}-1$.

  We already saw this for $n = 1$, because $\ID^{1}$ is connected and $\{0,1\}$ isn't.

  For $n = 2$ assume there would exist such a retraction $\rho: \ID^{2} \to \IS^{1}$.
  This means
  \begin{align*}
   \id_{\IS^{1}} = \rho \circ \iota, \quad \text{where} \quad \iota: \IS^{1} \to \ID^{2}, s \mapsto s
  \end{align*}
  but by functoriality of the fundamental group we would have
  \begin{align*}
    \id_{\pi_1(\IS^{1},1)} = \left(
      \id_{\IS^{1}}
    \right)_\ast 
    =
    (\rho \circ \iota)_{\ast} = \rho_{\ast} \circ \iota_{\ast}
  \end{align*}
  where
  \begin{align*}
    \rho_{\ast}: \pi_1(\ID^{2},1) \to  \pi_1(\IS^{1},1) \quad \text{and} \quad \iota_{\ast}: \pi_1(\IS^{1},1) \to  \pi_1(\ID^{2},1)
  \end{align*}
  But this contradicty our previous calculation of their fundamental groups, where we found 
  $\pi_1(\ID^{2},1) = \{e\}$ and $\pi_{1}(\IS^{1},1) = \Z$.

  In a diagram, the argument looks like this
  \begin{center}
  \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
    \IS^{1} \arrow[]{r}{\iota} 
    & \ID^{2} \arrow[]{r}{\rho}
    & \IS^{1} \arrow[bend left]{d}{\pi_1}\\
    \Z \arrow[]{r}{\iota_{\ast}} 
    & \{e\} \arrow[]{r}{\rho_{\ast}}
    & \Z
  \end{tikzcd}
  \end{center}
\end{proof}
In the proof, we showed that if $\rho: X \to A$ is a retraction and $\iota: A \to  X$ is the embedding, then $\rho_{\ast}$ is surjective and $\iota_{\ast}$ is injective.

Moreover, if $\rho$ is a strong deformation retract, then $\rho_{\ast}$ and $\iota_{\ast}$ are inverse group homomorphisms.




\begin{thm}[Fundamental theorem of Algebra]
  Let $p(z) = z^{d} + a_{d-1}z^{d-1} + \ldots + a_0 \in \C[z]$ be a non-constant polynomial.
  Then $\exists x_0 \in \C$ with $p(z_0) = 0$
\end{thm}
\begin{proof}
  Assume $p(z) \neq 0 \forall z \in \C$. 
  Then chose a radius $r$ large enough, (for example $r > \sum_{j=0}^{d-1}\abs{a_j}$) and construct he path
  \begin{align*}
    \alpha: [0,1] \to \C^{\ast}, \quad s \mapsto  \frac{p(re^{2 \pi i s})}{p(s)}
  \end{align*}
  Then, $\alpha$ is homotopic to the constant map $1$ with the homotopy
  \begin{align*}
    h(s,t) = \frac{p(tre^{2 \pi i s})}{p(tr)}
  \end{align*}
  If we move along the path $d$-times, we get the path $\alpha^{d}$, which is again homotopic to $\alpha$ by
  \begin{align*}
    h(s,t) = \frac{tp(re^{2 \pi i s}) + (1-t)(re^{2 \pi i s})^{d}}{tp(r) + (1-t) r^{d}} \in \C^{\ast}
  \end{align*}
  which is well-defined because
  \begin{align*}
    \abs{
      tp(re^{2 \pi i s}) + (1-t)(re^{2 \pi i s})^{d}
    }
    &\geq r^{d} - t \sum_{j=0}^{d -1}\abs{a_j}r^{j} \\
    &=
    r^{d-1}\left(r - t \sum_{j=0}^{d-1}\abs{a_j} r^{j - dt} \right)\\
    &\geq
    r^{d-1}(r - t \sum_{j=0}^{d-1}\abs{a_j}) \geq r^{d-1}\left(r - \sum_{j=0}^{d-1}\abs{a_j}\right) > 0
  \end{align*}
  but by the previous example with $\pi_1(\R^{2} \setminus \{0\}$ we would have
  \begin{align*}
    1 = [\alpha] = [\alpha_d] \neq 1 \quad \text{ in } \pi_1(\C^{\ast},1) \lightning
  \end{align*}
\end{proof}


The fundamental group by definition depends on the choice of the basepoint, but how much?
Can we predict when a change of the basepoint changes the fundamental group?


\begin{lem}[]
  The following are easy to prove
\begin{enumerate}
  \item Let $\beta$ be a path from $x_0$ to $x_1$ in $X$. Then the mapping
    \begin{align*}
      \Psi_{\beta}: \pi_1(X,x_1) \to  \pi_1(X,x_0),  \quad [\alpha] \mapsto [\beta \alpha \beta^{-}]
    \end{align*}
    is an isomorphism (in $\Grp$).
  \item The fundamental group of the product is the product of the fundamental groups. So for $(X,x_0),(Y,y_0)$ in $\Top^{\ast}$, there exists an isomorphism
    \begin{align*}
      \pi_1(X,x_0) \times \pi_1(Y,y_0) \stackrel{\sim}{\to} \pi_1(X \times Y, (x_0,y_0))
    \end{align*}
\end{enumerate}
In other words, for a fixed space $(X,x_0)$ in $\Top^{\ast}$, the following ``diagram'' commutes
\begin{center}
  \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
    \Top^{\ast} 
    \arrow[]{r}{\pi_1}  
    \arrow[swap]{d}{(X,x_0) \times -}
  & \Grp
  \arrow[]{d}{\pi_1(X,x_0) \times -}
  \\
  \Top^{\ast}
  \arrow[]{r}{\pi_1}
  & \Grp
  \end{tikzcd}
\end{center}
\end{lem}

From now on, we will write $\pi_1(X) := \pi_1(X,x_0)$ if $X$ is path connected.

\begin{cor}[]
$\R^{2} \iso \R^{n} \implies n = 2$
\end{cor}
\begin{proof}
  Because a homeomorphism $\phi: \R^{2} \setminus \{0\}$ induces a homeomorhpism
  \begin{align*}
    \tilde{\phi}: \R^{2} \setminus \{0\} \to  \R^{n} \{0\}
  \end{align*}
  it follows directly from the computation done in Example \ref{ex:homotopy-of-spheres}.
\end{proof}


Now we can finally define what it means for a topological space to have no point-holes.
\begin{dfn}[]
  A path connected space $X$ is called \textbf{simply connected}, if $\pi_1(X) = \{e\}$
\end{dfn}
For example, $\IS^{n}$ is simply connected for $n \geq 2$.


\begin{thm}[]
  Let $f: (X,x_0) \to (Y,y_0)$ be a homotopy equivalence.
  Then the induced mapping $f_{\ast}: \pi_1(X,x_0) \to  \pi_1(Y,y_0)$ is a group isomorphism.
\end{thm}
This gives us one of the most useful tools for computing fundamental groups.
\begin{cor}[]
If $X \stackrel{\rho}{\to}A$ is a deformation retract, then then the induced maps $\rho_{\ast}: \pi_1(X) \to \pi_1(A)$ and $\iota_{\ast}: \pi_1(A) \to  \pi_1(X)$ are group isomorphisms.
\end{cor}
\begin{proof}[Proof Corollary]
  Follows immediately from the theorem and Lemma~\ref{lem:deformation-retract-homotopy}
\end{proof}

\begin{xmp}
\begin{itemize}
  \item $\IS^{n} \subseteq \R^{n+1} \setminus \{0\}$ is a deformation retract, so from Theorem~\ref{thm:pi1-of-sphere}, we get
\begin{align*}
  \pi_1(\R^{n+1} \setminus \{0\}) = \left\{\begin{array}{ll}
    \Z & n = 1\\
    \{e\} & n \geq 2
  \end{array} \right.
\end{align*}
  \item For $M$ the Möbius strip, the mid-line circle $\IS^{1} \subseteq M$ is a deformation retract so $\pi_1(M) = \Z$.
    However, its boundary $\del M$ (where $\del$ is with respect to $\R^{3}$) is also homeomorphic to $\IS^{1}$, but it is \emph{not} a deformation retract of $M$.
    Let $[\beta]$ be a generator of $\pi_1(M)$.
    Then a generator $[\alpha]$ of $\pi_1(\del M)$ goes around the strip twice, so the inclusion map $\iota: \del M \to M$ induces a non-surjective group homomorphism
    \begin{align*}
      \iota_{\ast}: \pi_1(\del M) \to \pi_1(M), \quad
      [\alpha] \mapsto \iota_{\ast}([\alpha]) = [\iota \circ \alpha] = 2[\beta]
    \end{align*}
    So here we see that it is not enough to check that both $A$ and $X$ have the same fundamental group, the homotopy inverses (here $\iota$ and $\rho$) must also induce group isomorphisms!
  \item $\IS^{2} \subseteq B_1(0) \setminus \{0\}$ is a deformation retract, so both have trivial fundamental group $\{e\}$.
\end{itemize}
\end{xmp}
To prove the theorem, we will use the following lemma
\begin{lem}[]
  Let $f_0 \sim f_1: X \to Y$ be homotopic via $h$, $x_0 \in X$ and $\beta$ a path from $f_0(x_0) \to f_1(x_0)$.
  Then $(f_0)_{\ast} = \Psi_{\beta} \circ (f_1)_{\ast}$.
  The corresponding diagram is
  \begin{center}
  \begin{tikzcd}[row sep=0.8em] %\arrow[bend right,swap]{dr}{F}
   & \pi_1(Y,f_1(x_0)) \arrow[]{dd}{\Psi_{\beta}}
   \\
   \pi_1(X,x_0)
   \arrow[]{ur}{(f_1)_{\ast}}
   \arrow[]{dr}{(f_0)_{\ast}}\\
   & \pi_1(Y,f_0(x_0))
  \end{tikzcd}
  \end{center}
\end{lem}
\begin{proof}[Proof Lemma]
  Set $\beta_t:[0,1] \to Y$ as $\beta_{t}(s) = \beta(st)$.
  Then for any loop $\alpha$ at $x_0$ we get
  \begin{align*}
    f_0 \circ \alpha \sim \beta (f_1 \circ \alpha_)\beta^{-} \text{ rel }x_0
  \end{align*}
  via the homotopy
  \begin{align*}
    H(s,t) = \beta_t(h_t \circ \alpha)\beta_t^{-}
  \end{align*}
  and as such, the group homomorhpisms are equal:
  \begin{align*}
    (f_0)_{\ast}[\alpha] = [f_0 \circ \alpha] = [\beta(f_1 \circ \alpha)\beta^{-})] = \Psi_{\beta}([f_1 \circ \alpha]) = (\Psi_{\beta} \circ (f_1)_{\ast})[\alpha]
  \end{align*}
\end{proof}

\begin{proof}[Proof theorem]
Let $f: X \to Y$ be a homotopy equivalence and $g: Y \to  X$ its homotopy inverse. So let $h,k$ be the corresponding homotopies
\begin{align*}
  g \circ f \sim_h \id_X \quad \text{and} \quad f \circ g \sim_k \id_Y
\end{align*}
Let $x_0 \in X$ and consider the diagram
\begin{center}
\begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
  \pi_1(X,x_0) 
  \arrow[]{r}{f_{\ast}}
  &
  \pi_1(Y,f(x_0)) 
  \arrow[]{r}{g_{\ast}}
  &
  \pi_1(X,(g \circ f)(x_0))
  \arrow[]{r}{f_{\ast}}
  &
  \pi_1(X,(f \circ g \circ f)(x_0))
\end{tikzcd}
\end{center}
We then take the path $\beta(s) := h(x_0,s)$ that connects $(g \circ f)(x_0)$ with $x_0$, we can use the previous lemma to get
\begin{align*}
  g_{\ast} \circ f_{\ast} = (g \circ f)_{\ast} = \Psi_{\beta} \circ (\id_X)_{\ast} = \Psi_{\beta}
\end{align*}
which shows that $f_{\ast}$ is injective.
For surjectivity, we take the path $\beta'(s) = k(f(x_0),s)$ connecting $(f \circ g \circ f)(x_0)$ with $f(x_0)$ and get similarly
\begin{align*}
 f_{\ast}\circ g_{\ast} = \Psi_{\beta'} 
\end{align*}
which shows injectivity of $g_{\ast}$, so since $g_{\ast} \circ f_{\ast}$ is bijective, $f_{\ast}$ is also surjective and thus an isomorphism.
\end{proof}



\begin{xmp}[]
If $X$ is contractible, then $X$ is simply connected because the fundamental group of the singleton space is $\{e\}$.
\end{xmp}

We have called the fundamental group functor $\pi_{\bm{1}}$. But if there is a $\pi_1$, is there a $\pi_2$?

Because a loop $\alpha: [0,1]$ is the same as continuous function $\alpha \in \Hom_{\Top^{\ast}}(\IS^{\bm{1}},(X,x_0))$ we can generalize the notion of the fundamental group.

\begin{dfn}[]
  For $(X,x_0) \in \Top^{\ast}$ and $n \in \N$ we define the \textbf{higher homotopy groups}
  \begin{align*}
    \pi_n(X,x_0) := \{[\alpha] \big\vert \alpha: \IS^{n} \to  X \text{ with } \alpha(e_1) = x_0\} = \Hom_{\Htpy^{\ast}}(\IS^{n},(X,x_0))
  \end{align*}
\end{dfn}
They are actually only a group for $n \geq 1$, but for $n \geq 2$ they are all abelian.

To give an intuition what the homotopy groups mean, let's first note that
\begin{align*}
  \pi_0(X,x_0) = \{[\alpha] \big\vert \alpha: \{-1,1\} \to  X, \alpha(1) = x_0\} \iso \{[x] \big\vert x \in X\}
\end{align*}
so:
\begin{itemize}
  \item $\pi_0(X,x_0)$ measures the number of path connected components of $X$ (irrespective of the choice of $x_0$). In $\R^{n}$ this corresponds to $n-1$-dimensional holes.
  \item In $\R^{n}$, $\pi_1(X,x_0)$ tells us how many $(n-1)$-dimensional ``holes'' there are.
\end{itemize}

\begin{rem}[]
  There is a theorem that says that the functor $\pi_1$ is ``surjective''. 
  That is, for every group $G$ there exists a topological space $X$ such that $\pi_1(X) = G$.
\end{rem}


