\subsection{Continuous map from and into the quotient space}
When you are given a topological space $Z$, it is often useful to consider a more familiar space $X$ and define an equivalence relation $\sim$ on $X$ such that $Z \iso \faktor{X}{\sim}$.
One can then try to understand the topology of $Z$ by looking at continuous maps from and into $X$ instead.
This section will be very relevant for much of the remaining material.

\begin{lem}[Continuous maps \emph{from} the quotient space]\label{lem:unipropquot}
Let $X,Y$ be topological spaces and $\sim$ an equivalence relation on $X$ and $\pi: X \to \faktor{X}{\sim}$ the quotient map.
Then a function $f: \faktor{X}{\sim} \to Y$ is continuous if and only if $f \circ \pi$ is continous.

This can be visualized in the following diagram
\begin{center}
\begin{tikzcd}[] %\arrow[bend right,swap]{dr}{F}
  X \arrow[swap]{d}{\pi} \arrow[]{dr}{f \circ \pi}\\
  \faktor{X}{\sim} \arrow[]{r}{f} & Y
\end{tikzcd}
\end{center}
\end{lem}
\begin{proof}
Well, if $f$ is continous, then $f \circ \pi$ is the composition of continuous maps.

On the other hand if $f \circ \pi$ is continous, let $V \subseteq Y$ be open. Then
\begin{align*}
  (f \circ \pi)^{-1}(V) = \pi^{-1}(f^{-1}(V))\subseteq X \text{ is open }
\end{align*}
but by definition of the quotient topology this just says that $f^{-1}(V)$ is open.
\end{proof}

\begin{xmp}[]
  The Lemma lets us identify periodic continuous functions $\R \to \R^{n}$ with continuous functions $\IS^{1} \to \R^{n}$.
  For this it suffices to to show that the map
  \begin{align*}
    \Phi: \faktor{[0,L]}{\sim} \to \IS^{1}, \quad [t] \mapsto e^{\frac{2 \pi i t}{L}}
  \end{align*}
  is a homeomorphism,
  where $\faktor{[0,L]}{\sim}$ is obtained by glueing together its endpoints.

  Since $\Phi$ is continuous and bijection, $\faktor{[0,1]}{\sim}$ is compact and $\IS^{1}$ is Hausdorff, $\Phi$ is a homeomorphism (by Theorem~\ref{thm:compact-hausdorff-iso}).
\end{xmp}

\begin{rem}[]\hypertarget{rem:quotient-induced-map}
  The Lemma says that every continuous function $f: X \to Y$ that satisfies $x \sim y \implies f(x) = f(y)$ for all $x,y \in X$ induces a continuous map
  \begin{align*}
    \overline{f}: \faktor{X}{\sim} \to Y, \quad [x] \mapsto f(x)
  \end{align*}
\end{rem}

\begin{xmp}[]
For $\ID^{2} = \{v \in \R^{2} \big\vert \abs{v} \leq 1\}$ we set $\sim$ on $\ID^{2}$ to
\begin{align*}
  v \sim w \iff v = w \text{ or } \abs{v} = \abs{w} = 1
\end{align*}
Then resulting space is homeomorphic to the sphere $\IS^{2}$

To prove this we give a mapping
\begin{align*}
  g: \ID^{2}\to \IS^{2}, \quad v \mapsto \left\{\begin{array}{ll}
      (0,0,-1) & \text{ if } v = (0,0)\\
      \left(
      \frac{\sqrt{1 - (2 \abs{v} - 1)^{2}}}{\abs{v}}v,
      2 \abs{v} - 1
      \right)& 
      \text{ otherwise}
  \end{array} \right.
\end{align*}
which is continuous. 
From the previous lemma, it follows that the \hyperlink{rem:quotient-induced-map}{induced map} $f: \faktor{\ID^{2}}{\sim} \to  \IS^{2}$.
And since it is also bijective and $\faktor{\ID^{2}}{\sim}$ is compact, $\IS^{2}$ is Hausdorff, $f$ is a homeomorphism.
\end{xmp}


\begin{lem}[Continuous maps \emph{into} the quotient space]
  Let $X$ and $Y$ be topological spaces, $\sigma$ an equivalence relation on $X$ and $\phi: Y \to \faktor{X}{\sim}$ a map.

  If there exists a continuous map $\Phi: Y \to X$ such that $\phi = \pi \circ \Phi$, then $\phi$ is continuous.

\begin{center}
\begin{tikzcd}[] %\arrow[bend right,swap]{dr}{F}
  & X \arrow[]{d}{\pi}\\
  Y \arrow[dashed]{ur}{\Phi} \arrow[]{r}{\phi} & \faktor{X}{\sim}
\end{tikzcd}
\end{center}
\end{lem}
The proof is obvious since the composition of continous maps is continuous.
The Lemma doesn't tell us when such a continuous map $\Phi$ exists. 
In Chapter~\ref{sec:lifts}, we will find some results that do precisely that when $X$ and $\faktor{X}{\sim}$ share enough properties.

\begin{xmp}[]
Let $n \in \N, n \geq 2$. Set $X = \R^{n}$ with the equivalence relation
\begin{align*}
  v \sim w \iff v_i = w_i \quad \forall i \leq n -1
\end{align*}
The quotient map can be thought of compressing the $n$-th dimension on $\R^{n}$ onto the remaining $n-1$ ones. 
From what we just showed, the mapping
\begin{align*}
  \phi: \R^{n-1} \to \R^{n}/\sim, \quad u \mapsto [(u,0)]
\end{align*}
is continuous and is a homeomorphism where the inverse map is given by
\begin{align*}
  \phi^{-1}: \R^{n}/\sim \to \R^{n-1}, \quad [v] \mapsto  (v_1,\ldots,v_{n-1})
\end{align*}
\end{xmp}

