\subsection{Homotopy of maps}
For points $x,y$ in a topological space $X$, we can think of a path $\gamma:[0,1] \to X$ as something that continually deforms $x$ into $y$.
Homotopy of maps is the analogue for functions, instead of points. 
\begin{dfn}[]
Let $f,g: X \to Y$ be continuous maps.
\begin{itemize}
  \item A continuous map $H: X \times [0,1] \to  Y$ such that for all $x \in X$:
    \begin{align*}
      H(x,0) = f(x) \quad \text{ and } \quad H(x,1) = g(x)
    \end{align*}
    is called a \textbf{homotopy} from $f$ to $g$ and we write $f \sim_H g$. 
  \item We say that $f$ is \textbf{homotopic} to $g$, if such a homotopy exists and we write $f \sim g$.
\end{itemize}
\end{dfn}
\textbf{Notation:} For $t \in [0,1]$ fixed, and $f \sim_H g$, we write
\begin{align*}
  H_t = H(-,t) = (x \mapsto H(x,t)): X \to Y
\end{align*}
We sometimes draw little diagrams like this 
$\xymatrix@1{ % the @1 makes this an in-line diagram
    % arrows are of the form \ar @OPTION1 @OPTION2 ... [DIRECTION]POSITION{LABEL}
    % where options are @/BEND/, @{ARROWHEAD} etc.
    % direction is u (up), d (down), l (left), r (right)
    % position is ^ (above), _ (below, | (middle with break)
    X 
    \ar@/^2ex/[r]^f
    \ar@/_2ex/[r]_g
    \ar@{}[r]|{\Downarrow H}
    & 
    Y
  }$
to show the relationship, which is to be read as ``$H$ is a homotopy from $f$ to $g$''.
\begin{xmp}[]
  \begin{itemize}
    \item 
      A homotopy $\xymatrix@1{
        X
        \ar@/^2ex/[r]^f
        \ar@{}[r]|{\Downarrow H}
        \ar@/_2ex/[r]_g
        &
        Y
      }$
      defines a collection of paths $H_x = (t \mapsto H(x,t))$ from $f(x)$ to $g(x)$ for all $x \in X$.
      In particular, for $X = \{p\}$ one point, a homotopy corresponds to a path in $Y$.
      This is because $\{p\} \times [0,1]$ is homeomorphic to $[0,1]$.
    \item Let $X = [0,1]$ and $Y = \R^{2}$ (with the euclidean topology)
      and consider the functions
      \begin{align*}
        &f: [0,1] \to \R^{2}, \quad s \mapsto (s,s)
        \\
        &g: [0,1] \to \R^{2}, \quad s \mapsto (s+1,s)
      \end{align*}
      Then a homotopy from $f$ to $g$ is given by
      \begin{align*}
        H: [0,1] \times [0,1] \to Y, \quad
        (s,t) \mapsto (s+t,s)
      \end{align*}
  \end{itemize}
\end{xmp}
From the examples above, one should be able to see some sort of symmetry between the maps $f$ and $g$.

\begin{lem}[]
  Homotopy of maps forms an equivalence relation $\sim$ on $\Hom_{\Top}(X,Y)$.
\end{lem}
\begin{proof}
  \textbf{Reflexivity:} 
      \begin{align*}
        f \sim f \text{ via } H: X \times [0,1] \to Y, \quad (x,t) \mapsto f(x)
      \end{align*}
      We will denote this homotopy with the symbol $\id_f$ and draw $\xymatrix@1{
        X
        \ar@/^2ex/[r]^f
        \ar@{}[r]|{\Downarrow \id_f}
        \ar@/_2ex/[r]_f
        &
        Y
      }$

  \textbf{Symmetry:}
    We reverse the homotpy. If $f \sim_H g$ for some $H: X \times[0,1] \to Y$, then we get that for
      \begin{align*}
        H^{-}: X \times [0,1] \to  Y, \quad H(x,1-t)
      \end{align*}
      we have $g \sim_{H^{-}}f$, because for all $x \in X$:
      \begin{align*}
        H^{-}(x,0) = H(x,1) = g(x) \quad \text{and} \quad H^{-}(x,1) = H(x,0) = f(x) 
      \end{align*}
      The corresponding drawing is
      \begin{align*}
        \xymatrix{
          X
          \ar@/^2ex/[r]^f
          \ar@{}[r]|{\Downarrow H}
          \ar@/_2ex/[r]_g
          &
          Y
        }
        \quad
        \xymatrix{
          X
          \ar@/^2ex/[r]^f
          \ar@{}[r]|{\Uparrow H^{-}}
          \ar@/_2ex/[r]_g
          &
          Y
        }
      \end{align*}

    \textbf{Transitivity:}
      If $f \sim_K g$ and $g \sim_L h$, then define the \textbf{vertical composition} of $L$ and $K$ by
      \begin{align*}
        H: X \times [0,1] \to Y, \quad H(x,t) := \left\{\begin{array}{ll}
            K(x,2t) & t \leq \tfrac{1}{2}\\
            L(x,2t -1)& t \geq \tfrac{1}{2}
        \end{array} \right.
      \end{align*}
      this map is well defined as for $t = \tfrac{1}{2}$ we have
      $K(x,1) = g(x) = L(x,0)$
      for continuity we can use the homeomorphism
      \begin{align*}
        \Phi: X \times [0,1] \to X \times[0,1] \cup_{\phi} X \times [1,2] =: Q
      \end{align*}
      given by
      \begin{align*}
        \phi: X \times \{1\} \to  X \{1\}, \quad
        (x,1) \mapsto (x,1) \quad \text{and} \quad \Phi: (x,t) \mapsto [(x,t)]
      \end{align*}
      and use the continuous map
      \begin{align*}
        r: X \times [0,1] \sqcup X \times [1,2] \to Y, r = h(x,t) \sqcup k(x,t-1)
      \end{align*}
      to define $H$ in another way. $H = R \circ \Phi$ and use Lemma~\ref{lem:unipropquot} for
      \begin{align*}
        R: Q \to  Y, \quad [(x,t)] \mapsto r(x,t)
      \end{align*}

      The reason we call this vertical composition becomes apparent in the following diagram
      \begin{align*}
        \xymatrix{
          X
          \ar@/^4ex/[r]^f
          \ar@{{}{ }{}} @/^2ex/ [r]|{\Downarrow K}
          \ar[r]|g
          \ar@{{}{ }{}} @/_2ex/ [r]|{\Downarrow L}
          \ar@/_4ex/[r]_h
          &
          Y
        }
        \quad \text{becomes} \quad 
        \xymatrix{
          X
          \ar@/^4ex/[r]^f
          \ar@{}[r]|{\Downarrow H}
          \ar@/_4ex/[r]_h
          &
          Y
        }
      \end{align*}
      The following is not notation covered in the lecture\footnote{So don't assume the person grading your exam will know it}, but we will write $L \cdot K$ for the vertical composition.
\end{proof}

\textbf{Notation:} As we will often consider homotopies of maps $f,g: X \to Y$ with $X = [0,1]$ quite frequently, it can be confusing to remember which $[0,1]$ is which for homotopies $H: [0,1] \times [0,1] \to Y$.
We will chose the letters $s,t$ in this case and write $H(s,t)$ such that the \emph{time} variable $t$ corresponds to the continuous deformation of $f$ into $g$.

\begin{dfn}[]\label{dfn:homotopy-equivalence-class}
Let $X,Y$ be topological spaces and $f: X \to Y$ continuous.
Write $[f]$ for the equivalence class of $f$ under homotopy and denote the set of equivalence classes with respect to homotopy of maps as 
\begin{align*}
  [X,Y] := \faktor{\Hom_\Top(X,Y)}{\text{homotopy}} 
  := \left\{
    [f] \big\vert f: X \to Y \text{\ continuous}
  \right\}
\end{align*}
I will somtimes write $\Hom_{\Htpy}(X,Y)$ or $\Htpy(X,Y)$ instead.
\end{dfn}

\begin{xmp}[]\label{xmp:euclidean-is-special}
  Homotopy gives us another way of showing that certain special spaces are special.
  The space $\R^{n}$ (with the euclidean topology) is special in that for any space $X$, $[X,\R^{n}]$ has exactly one element.
  In other words, every two continuous maps $f,g: X \to \R^{n}$ are homotopic, as a homotopy $f \sim_h g$ can be given by
  \begin{align*}
    h: X \times [0,1] \to \R^{n}, \quad (x,t) \mapsto  (1-t)f(x) + tg(x)
  \end{align*}
  The singleton space is special in that two maps $f,g: \{\ast\} \to Y$ are homotopic if and only if their images $f(\ast),g(\ast)$ are path connected in $Y$.
\end{xmp}


\begin{lem}[]\label{lem:properties-homotopy}
  Homotopy not only defines an equivalence class structure inside a given $\Hom_{\Top}(X,Y)$, but that structure is also compatible with certain operations such as composition and taking products.
  \begin{enumerate}
    \item Homotopies can be \textbf{composed horizontally}.
      If $f\sim_K f'$ in $\Hom_{\Top}(X,Y)$ and $g \sim_{L}g'$ in $\Hom_{\Top}(Y,Z)$, then $g \circ f$ and $g' \circ f'$ are homotopic in $\Hom_{\Top}(X,Z)$.
      The name is explained by the following diagram:
      %\begin{align*}
      %  \xymatrix{
      %    X
      %    \ar@/^2ex/[r]^f
      %    \ar@{}[r]|{\Downarrow K}
      %    \ar@/_2ex/[r]_{f'}
      %    &
      %    Y
      %    \ar@/^2ex/[r]^g
      %    \ar@{}[r]|{\Downarrow L}
      %    \ar@/_2ex/[r]_{g'}
      %    &
      %    Z
      %  }
      %\end{align*}
    \item Homotopies of maps can be extended to products.
      If $f_i \sim_{H_i}g_i$ in $\Hom_{\Top}(X_i,Y_i)$ for $i \in I$, then the maps
      \begin{align*}
        &\prod_{i \in I}f_i = \left(
          {(x_i)}_{i \in I}
          \mapsto {(f(x_i))}_{i \in I}
        \right)
        :
        \prod_{i \in I} X_i \to \prod_{i \in I} Y_i
        \\
        \quad \text{and} \quad 
        &\prod_{i \in I}g_i = \left(
          {(x_i)}_{i \in I}
          \mapsto {(g(x_i))}_{i \in I}
        \right)
        :
        \prod_{i \in I} X_i \to \prod_{i \in I} Y_i
      \end{align*}
      are homotopic.
  \end{enumerate}
\end{lem}
\begin{proof}
  \begin{enumerate}
    \item We define the homotopy $H$ by
      \begin{align*}
        H: X \times [0,1] \to Z
        ,\quad 
        (x,t) \mapsto  L(K(x,t),t) 
      \end{align*}
      Then for all $x \in X$:
      \begin{align*}
        H(x,0) &= L(K(x,0),0) = L(f(x),0) = g(f(x))
        \\
        \text{and} \quad
        H(x,1) &= L(K(x,1),1) = L(f'(x),1) = g'(f'(x))
      \end{align*} 
      \item The homotopies can be traversed simultaneously. Define
      \begin{align*}
        H\left(
          {(x_i)}_{i \in I}
          , t
        \right)
        =
        {\left(
          H_i(x_i,t)
        \right)}_{i \in I}
      \end{align*}
      this yields
      \begin{align*}
        H\left(
          {(x_i)}_{i \in I}, 0
        \right)
        =
        {\left(
          H_i(x_i,0)
        \right)}_{i \in I}
        = \left(
          f_i(x_i)
        \right)_{i \in I}
        = \prod_{i \in I}f_i \left(
          x_i
        \right)_{i \in I}
      \end{align*}
      the argument for $H((x_i)_{i \in I},1)$ is practically the same.
  \end{enumerate}
\end{proof}

%\begin{xmp}[Whiskering]
%  With horizontal composition, homotopies and functions can be mixed.
%  Given the following diagram
%  \begin{align*}
%    \xymatrix{
%      W
%      \ar[r]^f
%      &
%      X
%      \ar@/^2ex/[r]^g
%      \ar@{}[r]|{\Downarrow H}
%      \ar@/_2ex/[r]_{g'}
%      &
%      Y
%      \ar[r]^\ell
%      &
%      Z
%    }
%  \end{align*}
%  we can obtain a new homotopy by horizontal composition of $H$ with the identity homotopies $\id_f, \id_h$:
%  \begin{align*}
%    \xymatrix{
%      W
%      \ar@/^2ex/[r]^f
%      \ar@{}[r]|{\Downarrow \id_f}
%      \ar@/_2ex/[r]_{f}
%      &
%      X
%      \ar@/^2ex/[r]^g
%      \ar@{}[r]|{\Downarrow H}
%      \ar@/_2ex/[r]_{g'}
%      &
%      Y
%      \ar@/^2ex/[r]^\ell
%      \ar@{}[r]|{\Downarrow \id_\ell}
%      \ar@/_2ex/[r]_{\ell}
%      &
%      Z
%    }
%    =
%    \xymatrix{
%      W
%      \ar@/^2ex/[r]^{\ell \circ g \circ f}
%      \ar@{}[r]|{\Downarrow \ell H f}
%      \ar@/_2ex/[r]_{\ell \circ g \circ f'}
%      &
%      Z
%    }
%  \end{align*}
%  where $\ell H f$ should be interpreted as the horizontal composition $\id_\ell H \id_f$ of homotopies.
%\end{xmp}



\vfill

You should now \ldots
\begin{itemize}
  \item be able to define homotopy and prove that it defines an equivalence relation.
  \item behaves under glueing, composition and for products.
\end{itemize}
