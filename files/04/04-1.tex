\subsection{The Notion of a Category}

\begin{dfn}[]
  A \textbf{category} $\textsf{C}$ consists of the following data: 
  \begin{itemize}
    \item A \emph{class} $\text{Obj}(\textsf{C})$ of objects. (Usually denoted with symbols $X,Y,Z, \ldots$)
    \item For every pair of objects $X,Y \in \text{Obj}(\textsf{C})$, there is a set $\Hom(X,Y)$ called the \textbf{Hom-Set}. Sometimes we write $\textsf{C}(X,Y)$ or $\Hom_{\textsf{C}}(X,Y)$ if unclear from the context.
      Elements of $\Hom(X,Y)$ are called \textbf{morphisms from $X$ to $Y$} and are usually denoted with symbols $f,g,h,i,j,k, \ldots$ and we write $X \stackrel{f}{\to}Y$ (or $f: X \to Y$).
      We call $X$ the \textbf{domain} and $Y$ the \textbf{codomain} of $f$.

      In order for every morphism to have a well-defined domain and codomain, we additionally require that $\Hom(X,Y) \cap \Hom(X',Y') = \emptyset$ for $(X,Y) \neq (X',Y')$.
  \end{itemize}
  For the data to from a valid category they must fulfill the following requirements:
  \begin{itemize}
    \item Morphisms can be composed: If $X \stackrel{f}{\to} Y$ and $Y \stackrel{g}{\to} Z$ are morphisms, then there exists a morphism $X \stackrel{g \circ f}{\to} Z$.
      More formally, for every triple $(X,Y,Z)$ of objects in $\textsf{C}$ there exists a map
      \begin{align*}
        \Hom(X,Y) \times \Hom(Y,Z) \to \Hom(X,Z), \quad (f,g) \mapsto g \circ f
      \end{align*}
    \item Composition is associative: For morphisms $X \stackrel{f}{\to } Y \stackrel{g}{\to} Z \stackrel{h}{\to} W$ we have the equality $(f \circ g) \circ h = f \circ (g \circ h)$, so we write $f \circ g \circ h$.
    \item For every object $X$ in $\textsf{C}$, there exists an \textbf{identity morphism} $X \stackrel{\id_X}{\to} X$ that satisfies the left and right unit laws: For any object $Y \in \text{Obj}(\textsf{C})$
      %\begin{align*}
      %  \forall f \in \Hom(X,Y): f \circ \id_X = f 
      %  \quad &\text{and} \quad 
      %  \forall g \in \Hom(Y,X): \id_X \circ g = g
      %  \\
      %  \xymatrix{
      %    X 
      %    \ar@(dl,ul)[]^{\id_X}
      %    \ar[r]^f
      %    &
      %    Y
      %  }
      %  \quad &\text{and} \quad 
      %  \xymatrix{
      %    Y 
      %    \ar[r]^g
      %    &
      %    X
      %    \ar@(ur,dr)[]^{\id_X}
      %  }
      %\end{align*}
      You will often see the notation $1_X$ or $\bm{1}_X$ or $\mathds{1}_X$ for the identity morphism.
  \end{itemize}
  If $\text{Obj}(\textsf{C})$ is a set (and not a proper class) we say that $\textsf{C}$ is a \textbf{small} category.
\end{dfn}

Let's take a look at some categories. 
When we say that something is a category, we usually specify what the objects are and what the morphisms (and composition) look like.

\begin{xmp}[]
  Make sure to verify that these do indeed form categories by checking the properties above.
  \begin{itemize}
    \item The category of topological spaces, written $\Top$ where the objects are topological spaces (with their topology) and where the morphisms are \emph{continuous} functions.
    \item The category of sets, denoted $\Set$, where the objects are sets and morphisms are functions.
    \item The category $\Grp$, where the objects are groups and morphisms are group homomorphisms.
    \item The category $\Top^{\ast}$, with pointed topological spaces with a basepoint $(X,\tau,x_0)$ as objects and where the morphisms are continous, base-point preserving functions.
    \item The category $\Ring$ with Rings as objects and ring homomorphisms as morphisms.
    \item \textsf{Graph} has graphs as objects and graph homomorphisms as morphisms.
    \item For $k$ a field, the category $\textsf{k-Vec}$ has $k$-vector spaces as objects with linear maps as morphisms.
    \item The category of fields with field homomomorphisms. 
  \end{itemize}
  Some of these categories will be of special interest later. In particular $\Set$, $\Top$, $\Top^{\ast}$ and $\Grp$, so try to translate any definition/result in a general category for these four.
\end{xmp}

It is traditional to name categories after their objects and imply what the morphisms are.
But just like how topological spaces should always be considered with their topology, we have to think of the objects and morphisms together when taking about categories.


\begin{xmp}[]\label{xmp:more-categories}
  The examples above are categories were objects were \emph{sets with structure} and the morphisms were \emph{structure preserving} maps.
  But the objects need not be sets, and morphisms need not be functions!
\begin{itemize}
  \item The trivial category, which consists of a single object $\ast$ and its identity morphism $\ast \stackrel{\id}{\to} \ast$.
  \item The empty category, without any objects or morphisms.
  \item A group (or monoid) $(G,\cdot,e)$ can be viewed as a category with a single object $\ast$, where the morphisms $\ast \stackrel{g}{\to} \ast$ are the elements $g \in G$ and where composition of morphisms is defined as the multiplication in $G$. For $g,h \in \Hom(\ast,\ast)$ there is a morphism $g \circ h := g \cdot h: \ast \to \ast$.
  \item A poset $(P,\leq)$ (such as $\N$) defines a category where the objects are the elements of $P$ and there exist unique morphisms $x \to y$ if and only if $x \leq y$.
  \item \textsf{Htpy} has the same objects as $\Top$, but morphisms are homotopy classes of continuous maps.
    So $\Hom(X,Y) = [X,Y]$ as in Definition~\ref{dfn:homotopy-equivalence-class}.
  \item In the context of the $\textsf{C++}$ programming language, consider the collection of types such as \texttt{int}, \texttt{bool}, \texttt{std::vector<llnode*>} etc.\  that form the objects of this category, where a morphism $T \stackrel{f}{\to} T'$ is a \emph{terminating} function with input argument of type $T$ that returns a $T'$.
    The condition that $f$ must terminate is necessary to let composition be well-defined.


  \item Given a category $\textsf{C}$, we can talk about its \textbf{arrow category}, where the objects are morphisms $X \stackrel{f}{\to} Y$ from $\textsf{C}$ and a morphism between objects $(X_0 \stackrel{f}{\to} Y_0), (X_1 \stackrel{g}{\to}Y_1)$ is a pair $(\alpha,\beta)$ of morphisms $X_0 \stackrel{\alpha}{\to} X_1$ and $Y_0 \stackrel{\beta}{\to} Y_1$ such that the following diagram commutes:
    \begin{center}
    \begin{tikzcd}[ ] %\arrow[bend right,swap]{dr}{F}
      X_0 \arrow[]{r}{\alpha} \arrow[swap]{d}{f} & X_1 \arrow[]{d}{g}\\
      Y_0 \arrow[]{r}{\beta} & Y_1
    \end{tikzcd}
    \end{center}
    i.e.\ such that $g \circ \alpha = \beta \circ f$.
    In this new category, the corresponding diagram would be
    $f \stackrel{(\alpha,\beta)}{\to} g$.
\end{itemize}
\end{xmp}

The language of category theory lets us unify similar ideas from different contexts:
\begin{dfn}[]\label{dfn:cat-iso}
  In any category, an \textbf{isomorphism} (or \textbf{iso}, for short) is a morphism with a two-sided inverse.
That is, a morphism $f: X \to Y$ is iso, if there exists a morphism $g: Y \to X$ such that $f \circ g = \id_Y$ and $g \circ f = \id_X$.
\end{dfn}
In $\Set$, the isomorphisms are bijective functions. In $\Grp$, they are group isomorphisms. In $\Top$ they are homeomorphisms. In $\Htpy$ an isomorphism is a homotopy equivalence (up to homotopy of course).

The possibility to use the same definition and applying it to different contexts to recover ``classical'' definitions is quite common and also what makes category theory so nice to use.


\vfill
After this section, you should \ldots
\begin{itemize}
  \item know the definition of a category and be able to give many different examples of categories.
\end{itemize}
