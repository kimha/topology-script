\subsection{Topological spaces over $X$}
When we were looking at the fundamental group, we were fixing the space $\IS^{1}$ and tried to understand other spaces $Y$ by studying continuous functions from $\IS^{1}$ to $Y$.

In this section, we fix a topological space $X$ and consider other spaces $Y$ with surjective morphisms $\pi: Y \to  X$, and try to understand them by studying such morphisms.

We can turn this into a category, where the objects are topological spaces $Y$ with continuous surjective maps $\pi: Y \to X$, and a morphisms between objects $(Y, \pi)$ and $(\tilde{Y}, \tilde{\pi})$ is a continous map $f: Y \to  \tilde{Y}$ such that the following diagram commutes
\begin{center}
\begin{tikzcd}[column sep=0.8em] %\arrow[bend right,swap]{dr}{F}
  Y \arrow[swap]{dr}{\pi} \arrow[]{rr}{f}& & \tilde{Y} \arrow[]{dl}{\tilde{\pi}}\\
    & X
\end{tikzcd}
\end{center}

\begin{dfn}[]
  If two objects in this category are isomorphic (as in Definition~\ref{dfn:cat-iso}),
  we say that they are \hypertarget{dfn:isomorphic-over-X}{\textbf{isomorphic over $X$}}.

  That is, two objects $Y \stackrel{\pi}{\to}X$, $\tilde{Y} \stackrel{\tilde{\pi}}{\to}X$ are isomorphic, if there exist homeomorphisms $\Phi:Y \to \tilde{Y}$,$\Psi: \tilde{Y} \to Y$, such that $\pi \circ \Psi = \tilde{\pi}$ and $\tilde{\pi} \circ \Phi = \pi$.
\end{dfn}

\begin{xmp}[]
  For $X = \IS^{1}$, the cylinder $C = \IS^{1} \times [0,1]$ and the Möbius strip $M= [-1,1] \times [0,1]/\sim$, with $(s,0) \sim (-s,1)$
  are objects in this category and their surjective morphisms are 
  \begin{align*}
    \pi_C:C \to &\IS^{1}, \quad (x,t) \mapsto x
    \\
    \pi_M:M \to &\IS^{1}, \quad [s,t] \mapsto  e^{2 \pi i t}
  \end{align*}
\end{xmp}

\begin{dfn}[]
  Let $\pi: Y \to X$ be a continuous surjective map
  \begin{itemize}
    \item $(Y,\pi)$ is called a \textbf{trivial fiber} (or trivial fiber bundle) (\textbf{triviale Faserung} oder triviales Faserbündel), if there exists a topological space $F$ such that $(Y,\pi)$ is \hyperlink{dfn:isomorphic-over-X}{isomorphic over $X$} to
      \begin{align*}
        \tilde{\pi}: X \times F \to  X, \quad (x,s) \mapsto x
      \end{align*}
      We call the space $F$ the \textbf{fiber} of the map.
    \item $\pi$ is a \textbf{fiber bundle} (\textbf{lokal triviale Faserung}/Faserbündel), if for all $x \in X$ there exists a neighborhood $U$ of $X$ such that
      \begin{align*}
        \pi|_{\pi^{-1}(U)} : \pi^{-1}(U) \to U
      \end{align*}
      is a trival fiber. For $x \in X$, we call $\pi^{-1}(x)$ the \textbf{fiber of $(Y,\pi)$ over $x$}
  \end{itemize}
\end{dfn}

\begin{xmp}
Intuitively, a fiber bundle $(Y, \pi)$ is a space that is obtained by glueing topological spaces $F_x$ to every point of $X$ in a continuous way.
If this is done without ``twisting'' or ``moving'' the space, we call this a trivial fiber bundle.

\begin{itemize}
  \item 
    The cylinder $(C,\pi_C)$ is a trivial fiber.
  \item 
    The Möbius strip $(M, \pi_M)$ is not a trivial fiber, but a fiber bundle.
  \item
    For $X = [-1,0) \sqcup (0,1]$, we can take the space obtained by glueing $\R$ to every point on the left half, and $[0,1]$ to the right half.
    \begin{align*}
      Y = 
      [-1,0) \times \R \sqcup (0,1] \times [0,1]
      \subseteq \R^{2}
    \end{align*}
    with $\pi: Y \to X$ the projection onto the first coordinate.
    This is a fiber bundle, even though at the points $x = -1$ and $x = 1$, the fibers $\pi^{-1}(-1)$ and $\pi^{-1}(1)$ are not isomorphic.
  \item 
    Here's an example for Physicists. A vector field (such as $\vec{E}, \vec{B}$ etc.) is a (smooth) function $f: \R^{3} \to \R^{3}$. 
    But it is often nicer to visualize it as something that to every point in $\R^{3}$ attaches a vector $f(\vec{x})$.
    The set of all vector fields can then be described as the space $Y$, obtained by glueing a copy of $\R^{3}$ to every point in $\R^{3}$.
    This defines a trivial fiber bundle $Y \stackrel{\pi}{\to}X$.
    A vector field is then something that for every $x \in X$ picks out an element of fits fiber $\pi^{-1}(x)$.
    This is equivalent to saying that the following diagram commutes
    \begin{align*}
      \xymatrix{
        X
        \ar[r]^{f}
        \ar@/^4ex/[rr]^{\id_X}
        &
        Y
        \ar[r]^{\pi}
        &
        X
      }
    \end{align*}
    because if $\pi \circ f = \id_X$, this means the chosen vector $f(\vec{x})$ projects back to $x$.

    If we want our vector field to be continuous/smooth, we then just require that $f:X \to Y$ be a continuous/smooth right-inverse to $\pi$.
\item 
  For $X \subseteq \R^{n}$ a $d$-dimensional smooth manifold and $x \in X$, let $F_x$ be the tangent space of $X$ at $x$.
  This then defines a fiber bundle on $X$.
  Take for example the sphere $X = \IS^{2} \subseteq \R^{3}$. The tangent space at $x \in X$ is then a two-dimensional plane. Moving around $x$ a little, changes the orientation of the plane slightly.

\end{itemize}
\end{xmp}


\vfill
Before going into the next section, make sure you understand the definition of a fiber bundle.
Given a picture/definition of a map $Y \stackrel{\pi}{\to}X$, you should be able to tell if it is a (trivial) fiber bundle or not by ``looking at it''.
