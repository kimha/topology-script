\subsection{Products and coproducts}\label{ssec:product-coproduct}

For $X,Y$ sets, recall that their cartesian product is the set 
\begin{align*}
  X \times Y = \left\{
    (x,y) \big\vert x \in X, y \in Y
  \right\}
\end{align*}
which is usually constructed as ordered tuples of elements from $X$ and $Y$, (such as $(x,y) = \{x,\{x,y\}\}$).
This set comes with two natural maps called the \emph{projection maps}
\begin{align*}
  \pi_X: X \times Y \to X, \quad (x,y) \mapsto x\\
  \pi_Y: X \times Y \to Y, \quad (x,y) \mapsto y
\end{align*}
\begin{center}
\begin{tikzcd}[column sep=0.8em] 
  & X \times Y
  \arrow[swap]{dl}{\pi_X}
  \arrow[]{dr}{\pi_Y}
  \\
  X 
  &&
  Y
\end{tikzcd}
\end{center}
Now let $X$ and $Y$ be (possibly not disjoint) sets.
We construct their \textbf{disjoint union} as
\begin{align*}
  X \sqcup Y = X \times \{0\} \cup Y \times \{1\} = \left\{
    (x,0) \big\vert x \in X
  \right\}
  \cup 
  \{(y,1) \big\vert y \in Y\} 
\end{align*}
So for example, if $X = \{0,1,2\}$ and $Y = \{1,2,3\}$, then $X \sqcup Y$ has six elements.

The disjoint union comes with two natural inclusion maps
\begin{align*}
  \iota_X:X \to X \sqcup Y, \quad x \mapsto (x,0)\\
  \iota_Y:Y \to X \sqcup Y, \quad y \mapsto (y,1)
\end{align*}
\begin{center}
\begin{tikzcd}[column sep=0.8em] 
  & X \sqcup Y
  \\
  X 
  \arrow[]{ur}{\iota_X}
  &&
  Y \arrow[swap]{ul}{\iota_Y}
\end{tikzcd}
\end{center}
Note how this diagram looks like the one for the cartesian product, except the arrows go in the opposite direction.

Often, when defining a map from $X \sqcup Y$ to another set $Z$, we do so by defining two functions $X \stackrel{f}{\to} Z$ and $Y \stackrel{g}{\to} Z$.
This uniquely defines a map
\begin{align*}
  f \sqcup g: X \sqcup Y \to Z,\quad 
  \left\{\begin{array}{l}
   x \mapsto f(x) \\
   y \mapsto g(y)
  \end{array} \right.
\end{align*}

\begin{dfn}[]
  Let $(X,\tau_X)$ and $(Y,\tau_Y)$ be two topological spaces. 
  \begin{itemize}
    \item Their \textbf{disjoint union} or \textbf{coproduct space}\index{coproduct!space} (\textbf{disjunkte Summe}) is the topological space $(X \sqcup Y, \tau_{X \sqcup Y})$, where
      \begin{align*}
        \tau_{X \sqcup Y} := \{U \sqcup V \big\vert U \in \tau_X, V \in \tau_Y\}
      \end{align*}
    \item Their (cartesian) \textbf{product}\index{product!space} is the topological space $(X \times Y, \tau_{X \times Y})$, where
      \begin{align*}
        \tau_{X \times Y} := \{W \subseteq X \times Y \big\vert \forall (x,y) \in W \exists U \in \tau_X, \exists V \in \tau_Y: (x,y) \in U \times V \subseteq W \}
      \end{align*}
      The topology $\tau_{X \times Y}$, is called the \textbf{product topology}\index{product!topology}\index{topology!product} on $X \times Y$.
  \end{itemize}
\end{dfn}

\begin{xmp}[]
  Let $\Z_{\text{five}}$ be the space of integers, where $U \subseteq \Z_{\text{five}}$ is open if and only if $5 \in U$ or $U = \emptyset$ and let $\Z_{\text{disc.}}$ have the discrete topology.

We now characterize the open sets of $\Z_{\text{five}} \times \Z_{\text{disc.}}$ in the product topology.

Let $W \subseteq \Z_{\text{five}} \times \Z_{\text{disc.}}$ be open.
If $W$ contains some point $(p,q) \in \Z_{\text{five}} \times \Z_{\text{disc.}}$, we must be able to find  to find open subsets $U \subseteq \Z_{\text{five}}, V \subseteq Z_{\text{disc.}}$ such that $(p,q) \in U \times V \subseteq W$.

If $p = 5$, then we can take $U = \{5\}, V = \{q\}$, which are open in $\Z_{\text{five}}$ or $\Z_{\text{disc.}}$, respectively.
So any point lying on the $p=5$ row doesn't put any restrictions on what $W$ has to look like.

Now if $p \neq 5$, the smallest open subset of $\Z_{\text{five}}$ containing $p$ ist the set $\{5,p\}$.
Thus $W$ must contain the set $\{5,p\} \times \{q\}$.

We can see that the open sets of $W \subseteq \Z_{\text{five}} \times \Z_{\text{disc.}}$ can be characterized as follows
\begin{align*}
  W \text{\ is open } \iff
  (p,q) \in W \implies (5,q) \in W
\end{align*}
\end{xmp}

%\begin{xmp}[]
%Let $\Z_{\text{cofin.}}$ have the cofinite topology. 
%Describe the open sets of $\Z_{\text{cofin.}} \times \Z_{\text{cofin.}}$.
%\end{xmp}

\begin{rem}[]
For the coproduct, the inclusions $\iota_X: X \to X \sqcup Y$ and $\iota_Y: Y \to X \sqcup Y$ are continuous.

For the product, the projection map $\pi_X = ((x,y) \mapsto x): X \times Y \to X$, and $\pi_Y$ are continuous with respect to the product topology.

In fact, there is another equivalent way to define the product topology.

The product topology is the coarsest topology on $X \times Y$, such that the projection mappings $\pi_X: X \times Y \to X$, $\pi_Y: X \times Y \to Y$ are continuous. 
\end{rem}


\begin{dfn}[]
  Let $X,Y$ be topological spaces. The \textbf{box topology}\index{topology!box} on $X \times Y$ is defined as
  \begin{align*}
    \tau_{\text{box}} :=
    \left\{
      U \times V \subseteq X \times Y
      \big\vert U \in \tau_X, V \in \tau_Y
    \right\}
  \end{align*}
\end{dfn}

\begin{rem}[]
  Let $f: X \to Y$ be a continuous function.
  \begin{itemize}
    \item If $g: X \to Z$ is another continuous function, then both $f$ and $g$ are continuous if and only if the product map $(f,g) = (x \mapsto (f(x),g(x))): X \to Y \times Z$ is continuous.
  \end{itemize}
\end{rem}


\vfill
You should now \ldots
\begin{itemize}
  \item know the definition of the disjoint union and product topology.
  \item be able to tell if a subset $W \subseteq X \times Y$ is open or not.
\end{itemize}

